{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Solvers.Herbie where

import Control.Applicative (liftA2)
import Control.Monad (join, when)
import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IntMap
import Solvers.HerbrandNew

data Term
  = HConst String
  | HVar Int
  | HFunction Term Term
  deriving (Eq)

instance Show Term where
  show (HConst "") = "End"
  show (HConst s) = s
  show (HVar s) = "v" ++ show (s `mod` 10000)
  show (HFunction (HConst s) (HFunction v1@(HVar _) (HConst ""))) = s ++ "(" ++ show v1 ++ ")"
  show (HFunction (HConst s) (HFunction v1@(HVar _) (HFunction v2@(HVar _) (HConst "")))) =
    show v1 ++ " " ++ s ++ " "++ show v2 -- special treatment for binary operators
  show (HFunction term1 term2) = show term1 ++ " -> " ++ show term2

data VarType = Special | Normal deriving (Show, Eq)

-- Skolemisation of individual variables is stored in varTable
-- Unification information is stored in varTable
-- Var number is stored in heap

type VarCount = Int

data VarDetail = VarDetail {varType :: VarType, boundTo :: Maybe Term} deriving Show

data Flags = Flags { noRewindHeap :: Bool, noOccursCheck:: Bool } deriving Show

type Store = (VarCount, IntMap VarDetail, Flags)

debugHerbrandMode = False 

debugHerbrand :: String -> Herbrand Store ()
debugHerbrand message = when debugHerbrandMode $ doIO $ putStrLn message

varCount :: Herbrand Store VarCount
varCount = do
  HerbState (varCount, varTable, _) n <- retState
  return varCount

varTable :: Herbrand Store (IntMap VarDetail)
varTable = do
  HerbState (varCount, varTable, _) n <- retState
  return varTable

_newStore :: IO Store
_newStore = return (0, IntMap.empty, Flags False False)

_deleteStore :: Store -> IO ()
_deleteStore _ = return ()

_occuredIn :: Term -> Term -> Herbrand Store Bool
_occuredIn t t' = do
  debugHerbrand $ "Debug[Herbrand]: Occur check on  " ++ show t ++ " and " ++ show t'
  HerbState (_, _, flags) n <- retState
  if noOccursCheck flags
    then return False
    else do
      term1 <- _deref t
      term2 <- _deref t'
      _occuredIn' term1 term2
  where
    _occuredIn' :: Term -> Term -> Herbrand Store Bool
    _occuredIn' (HVar _) (HConst _) = return False
    _occuredIn' (HVar v1) (HVar v2) = return (v1 == v2)
    _occuredIn' var@(HVar _) (HFunction t1 t2) = liftA2 (||) (_occuredIn var t1) (_occuredIn var t2)

-- Variable Operations
_var :: Herbrand Store Term
_var = do
  HerbState (varCount, varTable, flags) n <- retState
  setState $ HerbState (varCount + 1, varTable, flags) n
  return $ HVar ((n * 10000) + varCount)

_varBoundTo :: Term -> Herbrand Store (Maybe Term)
_varBoundTo (HVar v) = do
  vtable <- varTable
  case IntMap.lookup v vtable of
    Just (VarDetail _ justterm) -> return justterm
    _ -> return Nothing

_varType :: Term -> Herbrand Store VarType
_varType (HVar v) = do
  vtable <- varTable
  case IntMap.lookup v vtable of
    Just (VarDetail Special _) -> return Special
    _ -> return Normal

_isVar :: Term -> Herbrand Store Bool
_isVar (HVar v) = do
  vtable <- varTable
  case IntMap.lookup v vtable of
    Nothing -> return True
    Just (VarDetail Normal Nothing) -> return True
    Just (VarDetail Normal (Just term)) -> _isVar term
    Just (VarDetail Special _) -> return False
_isVar _ = return False

_wasVar :: Term -> Herbrand Store Bool
_wasVar (HVar _) = return True
_wasVar _ = return False

_skolemise :: Term -> Herbrand Store ()
_skolemise (HVar v) = do
  HerbState (vcount, vtable, flags) n <- retState
  let bounds = join $ fmap boundTo (IntMap.lookup v vtable)
  setState $
    HerbState
      ( vcount,
        IntMap.insert v (VarDetail Special bounds) vtable,
        flags
      )
      n
_skolemise _ = error "Cannot skolemise non-var term"

_rename :: Term -> Herbrand Store Term
_rename (HVar v) = do
  vtable <- varTable
  let bounds = join $ fmap boundTo (IntMap.lookup v vtable)
  case bounds of
    Just term -> return term -- var is already renamed
    Nothing -> do
      newVar <- _var -- issue a new var
      HerbState (vcount, vtable', flags) n <- retState
      setState $
        HerbState
          ( vcount,
            IntMap.insert v (VarDetail Special (Just newVar)) vtable',
            flags
          )
          n
      return newVar
_rename (HConst c) = _cnst c
_rename (HFunction t1 t2) = do
  t1' <- _rename t1
  t2' <- _rename t2
  return $HFunction t1' t2'

_deref :: Term -> Herbrand Store Term
_deref var@(HVar v) = do
  vtable <- varTable
  case IntMap.lookup v vtable of
    Nothing -> return var
    Just (VarDetail Special _) -> return var
    Just (VarDetail Normal Nothing) -> return var
    Just (VarDetail Normal (Just term)) -> _deref term
_deref t = return t

-- Const Operations
_cnst :: String -> Herbrand Store Term
_cnst s = return $ HConst s

new_const = _cnst

_isCnst :: Term -> Herbrand Store Bool
_isCnst term = do
  dereffed <- _deref term
  go dereffed
  where go (HConst _) = return True
        go _ = return False


_cnstName :: Term -> Herbrand Store String
_cnstName (HConst c) = return c

_printTerm :: Term -> Herbrand Store ()
_printTerm (HVar v) = do
  HerbState (_, varTable, _) _ <- retState
  case IntMap.lookup v varTable of
    Nothing -> do
      doIO $ putStrLn $ show (HVar v)
    Just (VarDetail vtype vbound) -> do
      doIO $ putStrLn $ show (HVar v) ++ " | typeof: " ++ show vtype ++ " | bound to: " ++ show vbound
_printTerm (HConst c) = doIO $ putStrLn $ show (HConst c)
_printTerm f@(HFunction t1 t2) = doIO $ putStrLn $ show f

_createChoicePoint :: Herbrand Store Store
_createChoicePoint = getStore

_rewind :: Store -> Herbrand Store ()
_rewind (oldVarCount, oldVarTable, _) = do
  HerbState (varCount, _, flags@(Flags nwh _)) n <- retState
  if nwh
    then setState $ HerbState (varCount, oldVarTable, flags) n
    else setState $ HerbState (oldVarCount, oldVarTable, flags)  n

_setFlag :: HerbrandFlag -> FlagState -> Herbrand Store ()
_setFlag NoRewindHeap On = do
  HerbState (varCount, vtable, (Flags _ noc)) n <- retState
  setState $ HerbState (varCount, vtable, (Flags True noc)) n
_setFlag NoRewindHeap Off = do
  HerbState (varCount, vtable, (Flags _ noc)) n <- retState
  setState $ HerbState (varCount, vtable, (Flags False noc)) n
_setFlag NoOccursCheck On = do
  HerbState (varCount, vtable, (Flags nwh _)) n <- retState
  setState $ HerbState (varCount, vtable, (Flags nwh True)) n
_setFlag NoOccursCheck Off = do
  HerbState (varCount, vtable, (Flags nwh _)) n <- retState
  setState $ HerbState (varCount, vtable, (Flags nwh False)) n


-- Unification
_unify :: Term -> Term -> Herbrand Store UnifyStatus
_unify t1 t2 = do
  t1' <- _deref t1
  t2' <- _deref t2
  res <- _unify' t1' t2'
  debugHerbrand $ "Debug[Herbrand]: Unifying " ++ show t1 ++ " and " ++ show t2 ++ " | Result: " ++ show res
  return res


_unify' :: Term -> Term -> Herbrand Store UnifyStatus
-- const =:= const
_unify' (HConst a) (HConst b) = do
  return $ if a == b then Success else MismatchFail

-- var =:= const
_unify' var@(HVar v) cnst@(HConst _) = do
  vtable <- varTable
  case IntMap.lookup v vtable of
    Nothing -> do
      HerbState (varCount, _, flags) n <- retState
      setState $
        HerbState
          ( varCount,
            IntMap.insert v (VarDetail Normal (Just cnst)) vtable,
            flags
          )
          n
      return Success
    Just (VarDetail Normal Nothing) -> do
      HerbState (varCount, _, flags) n <- retState
      setState $
        HerbState
          ( varCount,
            IntMap.insert v (VarDetail Normal (Just cnst)) vtable,
            flags
          )
          n
      return Success
    Just (VarDetail Normal (Just term)) -> _unify' term cnst

-- const =:= var
_unify' cnst@(HConst _) var@(HVar _) = _unify' var cnst
-- var =:= var
_unify' var1@(HVar v1) var2@(HVar v2) = do
  if v1 == v2
    then return Success
    else do
      vtable <- varTable
      case (IntMap.lookup v1 vtable, IntMap.lookup v2 vtable) of
        (Just (VarDetail Normal (Just t1)), Just (VarDetail Normal (Just t2))) -> _unify' t1 t2
        (_, Just (VarDetail Normal (Just term))) -> _unify' var1 term
        (Just (VarDetail Normal (Just term)), _) -> _unify' term var2
        (_, _) -> do
          HerbState (varCount, _, flags) n <- retState
          setState $
            HerbState
              ( varCount,
                IntMap.insert v1 (VarDetail Normal (Just var2)) vtable,
                flags
              )
              n
          return Success

-- fun =:= const
_unify' f@(HFunction _ _) c@(HConst _) = do
  return MismatchFail

-- const =:= fun
_unify' c@(HConst _) f@(HFunction _ _) = do
  return MismatchFail

-- var =:= fun
_unify' var@(HVar v) fun@(HFunction _ _) = do
  occured <- var `_occuredIn` fun
  if occured
    then return OccursFail
    else do
      bindTo <- _varBoundTo var
      case bindTo of
        Nothing -> do
          HerbState (varCount, vtable, flags) n <- retState
          setState $
            HerbState
              ( varCount,
                IntMap.insert v (VarDetail Normal (Just fun)) vtable,
                flags
              )
              n
          return Success
        Just term -> _unify' term fun

-- fun =:= var
_unify' fun@(HFunction _ _) var@(HVar _) = _unify' var fun

-- fun =:= fun
_unify' fun1@(HFunction term11 term12) fun2@(HFunction term21 term22) = do
  result1 <- _unify term11 term21
  result2 <- _unify term12 term22
  case (result1, result2) of
    (Success, a) -> return a
    (a, _) -> return a

_eqeq :: Term -> Term -> Herbrand Store UnifyStatus
_eqeq t1 t2 = do
  t1' <- _deref t1
  t2' <- _deref t2
  res <- _eqeq' t1' t2'
  debugHerbrand $ "Debug[Herbrand]: Entail " ++ show t1 ++ " and " ++ show t2 ++ " | Result: " ++ show res
  return res


_eqeq' :: Term -> Term -> Herbrand Store UnifyStatus
-- const =:= const
_eqeq' (HConst a) (HConst b) = do
  return $ if a == b then Success else MismatchFail

-- var =:= const
_eqeq' var@(HVar _) cnst@(HConst _) = do
  return MismatchFail

-- const =:= var
_eqeq' cnst@(HConst _) var@(HVar _) = _eqeq' var cnst

-- var =:= var
_eqeq' var1@(HVar v1) var2@(HVar v2) = do
  v1Type <- _varType var1
  v2Type <- _varType var2
  case (v1Type, v2Type) of
    (Special, Special) -> do
      v1Bound <- _varBoundTo var1
      v2Bound <- _varBoundTo var2
      case (v1Bound, v2Bound) of
        (Just t1, Just t2) -> _eqeq' t1 t2
        _ -> return MismatchFail
    (Special, Normal) -> do
      v1Bound <- _varBoundTo var1
      case v1Bound of
        (Just t) -> _eqeq' t var2
    (Normal, Special) -> do
      v2Bound <- _varBoundTo var2
      case v2Bound of
        (Just t) -> _eqeq' t var1
    (Normal, Normal) -> return $ if v1 == v2 then Success else MismatchFail

-- fun =:= const
_eqeq' f@(HFunction _ _) c@(HConst _) = do
  return MismatchFail

-- const =:= fun
_eqeq' c@(HConst _) f@(HFunction _ _) = do
  return MismatchFail

-- var =:= fun
_eqeq' var@(HVar v) fun@(HFunction _ _) = do
  return MismatchFail

-- fun =:= var
_eqeq' fun@(HFunction _ _) var@(HVar _) = _eqeq' var fun

-- fun =:= fun
_eqeq' fun1@(HFunction term11 term12) fun2@(HFunction term21 term22) = do
  result1 <- _eqeq' term11 term12
  result2 <- _eqeq' term21 term22
  case (result1, result2) of
    (Success, a) -> return a
    (a, _) -> return a

-- Matching
_match :: Term -> Term -> Herbrand Store UnifyStatus
_match t1 t2 = do
  t1derefed <- _deref t1
  t2derefed <- _deref t2
  res <- _match' t1derefed t2derefed
  debugHerbrand $ "Debug[Herbrand]: Match " ++ show t1 ++ " and " ++ show t2 ++ " | Result: " ++ show res
  return res


_match' :: Term -> Term -> Herbrand Store UnifyStatus
-- const =:= const
_match' constant1@(HConst a) constant2@(HConst b) = do
  return $ if a == b then Success else MismatchFail

-- var =:= const This does not bind variable
_match' variable@(HVar v) constant@(HConst _) = do
  return MismatchFail

-- const =:= var This should bind variable
_match' constant@(HConst _) variable@(HVar v) = do
  HerbState (varCount, vtable, flags) n <- retState
  setState $ HerbState (varCount, IntMap.insert v (VarDetail Special (Just constant)) vtable, flags) n --set to special var
  return Success

-- var =:= var what happen if match x y, match 
_match' variable1@(HVar v1) variable2@(HVar v2) = do
  case v1 == v2 of
    True -> return Success
    False -> do
      v2type <- _varType variable2
      case v2type of 
        Normal -> do
          HerbState (varCount, vtable, flags) n <- retState
          setState $ HerbState (varCount, IntMap.insert v2 (VarDetail Special (Just variable1)) vtable, flags) n --set to special var
          return Success
        Special -> do
          v2BoundTo <- _varBoundTo variable2
          case v2BoundTo of
            Nothing -> return MismatchFail
            Just t -> _eqeq t variable1

-- var =:= fun always fail is var is not bind to function
_match' variable@(HVar v) fun@(HFunction _ _) = do
  return MismatchFail

-- fun =:= var will bind var to function
_match' fun@(HFunction _ _) variable@(HVar v) = do
  occured <- variable `_occuredIn` fun
  case occured of
    True -> return OccursFail
    False -> do
      HerbState (varCount, vtable, flags) n <- retState
      setState $ HerbState (varCount, IntMap.insert v (VarDetail Special (Just fun)) vtable, flags) n --set to special var
      return Success

-- fun =:= var always fail
_match' fun@(HFunction _ _) constant@(HConst _) = do
  return MismatchFail

-- var =:= fun always fail
_match' constant@(HConst _) fun@(HFunction _ _) = do
  return MismatchFail

-- fun =:= fun
_match' fun1@(HFunction a b) fun2@(HFunction a' b') = do
  resultA <- _match' a a'
  resultB <- _match' b b'
  case (resultA, resultB) of
    (Success, Success) -> return Success
    (Success, a) -> return a
    (a, _) -> return a

-- Variant
_variant :: Term -> Term -> Herbrand Store UnifyStatus
_variant _ _ = return Success

-- Utilities for herbrand operations
-- TODO: Make this a total function.
_isApp :: Term -> Herbrand Store Bool
_isApp term = do
  derefed <- _deref term
  go derefed
  where go (HFunction _ _) = return True
        go _ = return False

_appArg :: (Num a, Eq a, Show a) => Term -> a -> Herbrand Store Term
_appArg (HFunction arg _) 0 = return arg
_appArg (HFunction _ arg) 1 = return arg
_appArg t n = do
  doIO $ print t
  doIO $ print n
  undefined

_arg :: Term -> Int -> Herbrand Store Term
_arg term argNo = do
  if argNo == 0
    then _appArg term 0
    else do
      tailTerm <- _appArg term 1
      _arg tailTerm (argNo - 1)

_countArgs :: Term -> Herbrand Store Int
_countArgs term = do
  atom <- _isCnst term
  if atom
    then return 0
    else do
      newTerm <- _appArg term 1
      len0 <- _countArgs newTerm
      return (1 + len0)

_makeArgs :: [Term] -> Herbrand Store Term
_makeArgs [] = return $ HConst ""
_makeArgs (arg : args) = do
  nargs <- _makeArgs args
  return $ HFunction arg nargs

_functor :: Term -> Herbrand Store (Term, Int)
_functor term = do
  term_derefed <- _deref term
  atom <- _isCnst term_derefed
  if atom
    then do
      return (term_derefed, 0)
    else do
      f <- _appArg term_derefed 0
      f_derefed <- _deref f
      args <- _appArg term_derefed 1
      noargs <- _countArgs args
      return (f_derefed, noargs)

_construct :: Term -> [Term] -> Herbrand Store Term
_construct f args = do
  nargs <- _makeArgs args
  return $ HFunction f nargs

_share :: Term -> Term -> Herbrand Store Bool
_share term1 term2 = do
  derefedTerm1 <- _deref term1
  derefedTerm2 <- _deref term2
  go derefedTerm1 derefedTerm2
  where
    go (HConst _) _ = return False
    go _ (HConst _) = return False
    go (HVar v1) (HVar v2) = return $ v1 == v2
    go (HFunction t1 t2) t = do
      r1 <- _share t t1
      r2 <- _share t t2
      return $ r1 || r2
    go t (HFunction t1 t2) = do
      r1 <- _share t t1
      r2 <- _share t t2
      return $ r1 || r2

_ground :: Term -> Herbrand Store Bool
_ground term = do
  t <- _deref term
  go t
  where
    go (HConst _) = return True
    go (HVar _) = return False
    go (HFunction t1 t2) = do
      r1 <- _ground t1
      r2 <- _ground t2
      return $ r1 && r2

-- Herbie
instance Herb Store Term where
  newStore = _newStore
  deleteStore = _deleteStore
  unify = _unify
  match = _match
  eqeq = _eqeq
  variant = _variant -- not matter
  skolemise = _skolemise -- not matter
  cnst = _cnst
  app t1 t2 = return $ HFunction t1 t2
  cnstName = _cnstName
  appArg = _appArg
  isVar = _isVar
  wasVar = _wasVar
  isCnst = _isCnst
  isApp = _isApp
  deref = _deref
  ground = _ground
  share = _share
  rename = _rename
  construct = _construct
  functor = _functor
  arg = _arg
  printTerm = _printTerm
  var = _var
  setFlag = _setFlag
  createCP = _createChoicePoint
  rewind = _rewind

test1 = do
  mystore <- _newStore
  exeHerbrand testcase (HerbState mystore 10)
  return ()
  where
    testcase :: Herbrand Store ()
    testcase = do
      cp <- _createChoicePoint
      v1 <- _var
      v2 <- _var
      v3 <- _rename v2
      v4 <- _rename v2
      _skolemise v1
      -- _rewindKeepHeap cp
      v5 <- _var
      _printTerm v1
      _printTerm v2
      _printTerm v3
      _printTerm v4
      _printTerm v5

test2 = do
  mystore <- _newStore
  exeHerbrand testcase (HerbState mystore 10)
  return ()
  where
    testcase :: Herbrand Store ()
    testcase = do
      v <- _var
      a <- _cnst "a"
      b <- _cnst "b"

      _unify' v a
      r <- _unify' v b
      _printTerm v
      doIO $ print r

testWasVar = do
  mystore <- _newStore
  _ <- exeHerbrand testcase (HerbState mystore 10)
  return ()
  where
    testcase :: Herbrand Store ()
    testcase = do
      v1 <- _var
      v2 <- _var
      _unify' v1 v2
      _unify' v2 (HConst "A")
      _printTerm v1
      _printTerm v2
      v1Const <- _isCnst v1
      v2Const <- _isCnst v2
      doIO $ print v1Const
      doIO $ print v2Const

-- main = testWasVar