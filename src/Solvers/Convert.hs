--------------------------------------------------------------------------------
--
-- Copyright (C) 2004 The Chameleon Team
--
-- This program is free software; you can redistribute it and/or modify it
-- under the terms of the GNU General Public License as published by the Free
-- Software Foundation; either version 2 of the License, or (at your option)
-- any later version. This program is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU General Public License for more details.
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
--
--------------------------------------------------------------------------------
-- Implementor: J. Wazny
-- Maintainer : J. Wazny
--------------------------------------------------------------------------------
--
-- Converts types, constraints, CHR rules from the format known to the rest of
-- the system (the external format) to the format understood by this solver
-- (the `internal' format.) We call this "internalisation."
--
-- Also does the reverse process, aka "externalisation."
--
-- NOTE: Throughout this module we limit ourselves to Herbie terms, when, to
--         be more general we should allow any terms which are in the Herbrand
--         class.
--
-- NOTE: I've tried using a HashTable for VarMaps, but the performance is
--         far, far worse than for FiniteMaps (by a factor of 10-1000.) This is
--         pretty weird.
--
-- NOTE: Before switching to FiniteMap, we used a list, and it turned out that
--         for anything but tiny programs, `intType' was the bottleneck.
--         With VarMaps as lists, the running time seemed exponential in the
--         program size. (double the size -> 100 times longer). Running time is
--         now about linear. (This is for extremely simple programs without
--         recursion, or deep function or callgraph nesting.)
--
--------------------------------------------------------------------------------

-- NOTE: tighten up this export list a bit.
module Solvers.Convert
  ( module Solvers.Convert,
    module I,
    module Solvers.HerbrandNew,
    module Solvers.Herbie,
    module Solvers.Misc,
  )
where

import Control.Monad
-- import qualified Data.HashTable as HT

-- import qualified Solvers.CHR as I

import qualified Core.CHR as E
import Core.Constraint
import Core.Justify
import Core.Name
import Core.Types
import qualified Data.Map.Strict as Map
-- import Foreign.C
import Misc
import qualified Solvers.CHRRule as I
import qualified Solvers.CHRState as I
import qualified Solvers.CHRState as S
import Solvers.HerbrandNew
import Solvers.Herbie
import Solvers.Misc

--------------------------------------------------------------------------------

-- a `justified' term
type JTerm c = (c, Just)

----------------------------------------

-- A mapping from external variables to Herbie terms
-- NOTE: in general these should be any terms which are members of Herbrand
type VarMap = Map.Map Var Term

emptyVarMap :: VarMap
emptyVarMap = Map.empty

varMapToList :: VarMap -> [(Var, Term)]
varMapToList = Map.toList

varMapTerms :: VarMap -> [Term]
varMapTerms = map snd . Map.toList

listToVarMap :: [(Var, Term)] -> VarMap
listToVarMap = Map.fromList

elemVarMap :: Var -> VarMap -> Bool
elemVarMap = Map.member

lookupVarMap :: Var -> VarMap -> Maybe Term
lookupVarMap v vm = Map.lookup v vm

----------------------------------------

-- Given a variable v, finds the variable mapped to v by the VarMap
-- NOTE: This is too inefficient to use often.
originalVar :: VarMap -> Var -> Var
originalVar vm v0 = lookup (varMapToList vm)
  where
    v = varNameStr v0
    lookup [] = v0
    lookup ((v1, v2) : vvs)
      | show v2 == v = v1
      | otherwise = lookup vvs

----------------------------------------
-- internalise types

-- NOTE: all information in type variables is lost (e.g. source location)

intType :: VarMap -> Type -> Herbrand Store (VarMap, Term)
intType vm (TVar v) = case Map.lookup v vm of
  Nothing -> do
    tm <- var
    let vm' = Map.insert v tm vm
    return (vm', tm)
  Just tm -> return (vm, tm)
intType vm (TCon c) = do
  -- cstr <- doIO $ newCString (nameStr c)
  ct <- cnst (nameStr c)
  return (vm, ct)
intType vm (TApp t1 t2) = do
  (vm1, tm1) <- intType vm t1
  (vm2, tm2) <- intType vm1 t2
  tm3 <- app tm1 tm2
  return (vm2, tm3)

----------------------------------------
-- Internalise a variable, retaining all source information.
-- We use the same trick to embed variable information as we do for
-- justifications. (Namely, to represent it as a string and turn it into a term
-- constructor.)

-- We want to do this for polymorphic variables in an implication, so that we
-- can get back the necessary source information if there's a type error.
intVar :: VarMap -> Var -> Herbrand Store (VarMap, Term)
intVar vm v = do
  (vm', t) <- intType vm (TVar v)
  c <- cnst "Var"
  i <- cnst (show v)
  tm <- construct c [t, i]
  return (vm', tm)

----------------------------------------
-- internalise constraints
--

intUCons :: VarMap -> UCons -> Herbrand Store (VarMap, (Term, Just))
intUCons vm (UC nm ts j) = do
  -- cstr <- doIO $ newCString (nameStr nm)
  c <- cnst (nameStr nm)
  (vm', ts') <- thread intType vm ts
  tm <- construct c ts'
  return (vm', (tm, j))

-- Note: we end up recreating this same "=" string for each constraint
intBCons :: VarMap -> BCons -> Herbrand Store (VarMap, (Term, Just))
intBCons vm bc@(Eq t1 t2 j) = do
  eq <- cnst "="
  (vm1, tm1) <- intType vm t1
  (vm2, tm2) <- intType vm1 t2
  tm <- construct eq [tm1, tm2]
  return (vm2, (tm, j))

pairToBCons :: (Term, Term) -> Herbrand Store Term
pairToBCons (tm1, tm2) = do
  eq <- cnst "="
  construct eq [tm1, tm2]

-- We create a term (which looks like a user constraint to Herbie) with the
-- following information embedded:
--  - universally quantified/polymorphic variables
--  - LHS constraints (justified)
--  - RHS constraints (justified)
intICons :: VarMap -> ICons -> Herbrand Store (VarMap, (Term, Just))
intICons vm ic@(IC vs (lucs, lbcs) (rucs, rbcs, rics) j) = do
  (vm1, lutj) <- thread intUCons vm lucs
  (vm2, lbtj) <- thread intBCons vm1 lbcs
  (vm3, rutj) <- thread intUCons vm2 rucs
  (vm4, rbtj) <- thread intBCons vm3 rbcs
  (vm5, ritj) <- thread intICons vm4 rics
  (vm6, vs') <- thread intVar vm5 vs

  -- Simply add the implications in with the user constraints; the solver
  -- will recognise and separate them all out eventually anyway.
  rutj <- return (rutj ++ ritj)

  lucs' <- mapM mkJustTerm lutj
  lbcs' <- mapM mkJustTerm lbtj
  rucs' <- mapM mkJustTerm rutj
  rbcs' <- mapM mkJustTerm rbtj

  -- implication var. constructor
  varsC <- cnst "VS!"
  vars <- construct varsC vs'
  -- implication lhs constructor
  lhsC <- cnst "LHS!"
  lhs <- construct lhsC (lucs' ++ lbcs')
  -- implication lhs constructor
  rhsC <- cnst "RHS!"
  rhs <- construct rhsC (rucs' ++ rbcs')
  -- implication constructor
  impC <- cnst "IMP!"
  imp <- construct impC [vars, lhs, rhs]

  return (vm5, (imp, j))
  where
    -- builds a Term with a justification; result is of form (term @ j)
    -- NOTE: we encode justifications as constants whose string name is the
    --             justification (we use show/read to encode and decode this)
    mkJustTerm :: (Term, Just) -> Herbrand Store Term
    mkJustTerm (t, j) = do
      jC <- cnst (show j)
      app t jC

-- NOTE: the terms appear in the list in the same order as in the original
-- constraint; (including ucs before bcs)
intConstraint :: VarMap -> Constraint -> Herbrand Store (VarMap, [(Term, Just)])
intConstraint vm c = do
  (vm1, tm_ucs) <- thread intUCons vm (cUCons c)
  (vm2, tm_bcs) <- thread intBCons vm1 (cBCons c)
  (vm3, tm_ics) <- thread intICons vm2 (cICons c)
  return (vm3, tm_ics ++ tm_ucs ++ tm_bcs)

----------------------------------------
-- internalise CHR rules

intCHR :: VarMap -> E.CHR -> Herbrand Store (I.Rule Term)
intCHR vm r = case r of
  E.SimpRule _ ucs c -> int I.SimpRule (ucs, c)
  E.PropRule _ ucs c -> int I.PropRule (ucs, c)
  where
    int mkRule (ucs, c) = do
      (vm1, tmj_ucs) <- thread intUCons vm ucs
      (vm2, tmj_cs) <- intConstraint vm1 c
      let (tm_ucs, _) = unzip tmj_ucs
          (tm_cs, js) = unzip tmj_cs
      return (mkRule tm_ucs tm_cs js)

--------------------------------------------------------------------------------
-- externalisation

-- Externalises a term representing a type. Will optionally dereference
-- variables if flag dr is True.
-- NOTE: This should only be called for terms which actually represent types.
--       (which can only appear within constraints.)
extTypeConf :: Herb s c => Bool -> c -> Herbrand s Type
extTypeConf dr t0 = do
  t <- if dr then deref t0 else return t0
  isV <- if dr then isVar t else wasVar t
  if isV
    then return (TVar (mkVar (show t)))
    else do
      isC <- isCnst t
      if isC
        then do
          cstr <- cnstName t
          return (TCon (mkFreeName cstr))
        else do
          -- Admiral Ackbar sez: It's an app!
          tm_l <- appArg t 0
          tm_r <- appArg t 1
          l <- ext tm_l
          r <- ext tm_r
          return (TApp l r)
  where
    ext = extTypeConf dr

extType :: Herb s c => c -> Herbrand s Type
extType = extTypeConf True

extOrigType :: Herb s c => c -> Herbrand s Type
extOrigType = extTypeConf False

----------------------------------------

-- Externalise a Var with embedded source information.
-- The first return value is the name of the type as it appears in the store,
-- the second is the original Var.
extVar :: Herb s c => c -> Herbrand s (Type, Var)
extVar t = do
  a1 <- arg t 1
  a2 <- arg t 2
  t <- extType a1
  cstr <- cnstName a2
  return (t, read cstr)

-- pulls apart a term representing an embedded (with info), and returns the var
-- term and info components
extVarInfo :: Herb s c => c -> Herbrand s (c, Var)
extVarInfo t = do
  a1 <- arg t 1
  a2 <- arg t 2
  cstr <- cnstName a2
  return (a1, read cstr)

-- NOTE: Only call this for terms which actually represent user constraints!
--       (which by construction can only appear within the UStore)
extUCons :: Term -> Just -> Herbrand Store UCons
extUCons tm j = do
  (func, arity) <- functor tm
  isC <- isCnst func
  when (not isC) (bug "extUCons: functor is not a constant!")
  cstr <- cnstName func
  ts <- extArgs arity []
  let nm = mkFreeName cstr
      uc = UC nm ts j
  return uc
  where
    extArgs 0 ts = return ts
    extArgs n ts = do
      a <- arg tm n
      t <- extType a
      extArgs (n -1) (t : ts)

-- Converts a term into a herbrand constraint.
-- NOTE: does not perform any checking of the term, hence may crash if the term
--         really doesn't represent a herbrand constraint!
extBCons :: Herb s c => c -> Just -> Herbrand s BCons
extBCons tm j = do
  tm1 <- arg tm 1
  tm2 <- arg tm 2
  t1 <- extType tm1
  t2 <- extType tm2
  return (Eq t1 t2 j)

-- As above, but does not dereference variables when externalising types.
extOrigBCons :: Herb s c => c -> Just -> Herbrand s BCons
extOrigBCons tm j = do
  tm1 <- arg tm 1
  tm2 <- arg tm 2
  t1 <- extOrigType tm1
  t2 <- extOrigType tm2
  return (Eq t1 t2 j)

-- Converts a term representing an implication into a real implication.
-- NOTE: does not perform any checking of the term, hence may crash if the term
--         really doesn't represent an implication!
extICons :: Term -> Just -> Herbrand Store ICons
extICons tm j = do
  (vs, lucs, lbcs, rucs, rbcs) <- disectICons tm
  tvs <- mapM extVar vs
  lucs' <- mapM (uncurry extUCons) lucs
  lbcs' <- mapM (uncurry extBCons) lbcs
  rucs' <- mapM (uncurry extUCons) rucs
  rbcs' <- mapM (uncurry extBCons) rbcs
  let vs' = map snd tvs
  return (IC vs' (lucs', lbcs') (rucs', rbcs', []) j)

-- Takes a term representing an implication, and breaks it into its five
-- components (in order):
--  1. universal variables
--  2. lhs user constraints
--  3. lhs herbrand constraints
--  4. rhs user constraints
--  5. rhs herbrand constraints
-- NOTE: Must pass in a well-formed implication term!
disectICons ::
  Herb s c =>
  c ->
  Herbrand s ([c], [JTerm c], [JTerm c], [JTerm c], [JTerm c])
disectICons tm = do
  tm1 <- arg tm 1
  tm2 <- arg tm 2
  tm3 <- arg tm 3
  vs <- toList tm1
  lhs <- toList tm2
  rhs <- toList tm3
  (lucs, lbcs) <- split lhs
  (rucs, rbcs) <- split rhs
  lucs' <- mkJTerms lucs
  lbcs' <- mkJTerms lbcs
  rucs' <- mkJTerms rucs
  rbcs' <- mkJTerms rbcs
  return (vs, lucs', lbcs', rucs', rbcs')
  where
    mkJTerms = mapM mkJTerm

    -- turns a single justified term into a JTerm
    mkJTerm :: Herb s c => c -> Herbrand s (JTerm c)
    mkJTerm tj = do
      t <- appArg tj 0
      j <- appArg tj 1
      cstr <- cnstName j
      -- str <- doIO (peekCString cstr)
      return (t, read cstr)

    -- splits terms into user and built-in constraints
    -- NOTE: split is processing `justified' terms
    split :: Herb s c => [c] -> Herbrand s ([c], [c])
    split [] = return ([], [])
    split (tj : tjs) = do
      (us, bs) <- split tjs
      t <- arg tj 0
      isB <- isBCons t
      return $
        if isB
          then (us, tj : bs)
          else (tj : us, bs)

    toList :: Herb s c => c -> Herbrand s [c]
    toList tm = do
      (ftr, arity) <- functor tm
      getArgs arity []
      where
        getArgs 0 ts = return ts
        getArgs n ts = do
          t <- arg tm n
          getArgs (n -1) (t : ts)

--------------------------------------------------------------------------------

isFailedState :: I.State c -> Bool
-- isFailedState NorthKorea = True
isFailedState (I.State {}) = False
isFailedState _ = True

mkFailedState :: I.State c -> I.State c
mkFailedState (I.State _ _ _ bstore _ _ _) = I.FailedBS bstore
mkFailedState st = st

-- Given the CHR state, extracts all user and implication constraints from the
-- user constraint store.
-- getCons :: I.State Term -> Herbrand Store ([UCons],[ICons])
getCons state = do
  (uts, its) <- splitUConsICons state
  let uts' = map unwrap uts
      its' = map unwrap its
  ucs <- mapM (uncurry extUCons) uts'
  ics <- mapM (uncurry extICons) its'
  return (ucs, ics)
  where
    unwrap = \(I.Num (I.UCons _ _ t j) _) -> (t, j)

-- splits all the terms in the user store of the state into user and
-- implication constraint terms
splitUConsICons ::
  Herb s c =>
  I.State c ->
  Herbrand s ([I.UStoreCons c], [I.UStoreCons c])
splitUConsICons st = case st of
  I.State _ ts _ _ _ _ _ -> do
    es <- mapM split ts
    return (splitEither es)
  _ -> return ([], [])
  where
    split c@(I.Num (I.UCons func arity term j) _) = do
      isImp <- isICons term
      return $
        if isImp
          then Right c
          else Left c

getBStore :: I.State c -> [BCons]
getBStore I.Failed = []
getBStore I.FailedImp = []
getBStore I.FailedUCUnmatched {} = []
getBStore (I.FailedBS bstore) = bstore
getBStore (I.FailedUniv bstore _ _) = bstore
getBStore (I.FailedUnivEsc bstore _ _) = bstore
getBStore (I.State _ _ _ bstore _ _ _) = bstore

setBStore :: [BCons] -> I.State c -> I.State c
setBStore bstore (I.FailedBS _) = (I.FailedBS bstore)
setBStore bstore (I.FailedUniv _ v vs) = (I.FailedUniv bstore v vs)
setBStore bstore (I.FailedUnivEsc _ t vs) = (I.FailedUnivEsc bstore t vs)
setBStore bstore (I.State s u d _ h i vs) = (I.State s u d bstore h i vs)
setBStore _ s = s

addToBStore :: [BCons] -> I.State c -> I.State c
addToBStore bcs (I.FailedBS bstore) = I.FailedBS (bcs ++ bstore)
addToBStore bcs (I.FailedUniv bstore v vs) = I.FailedUniv (bcs ++ bstore) v vs
addToBStore bcs (I.FailedUnivEsc bstore v vs) =
  I.FailedUnivEsc (bcs ++ bstore) v vs
addToBStore bcs (I.State s u d bstore h i v) = I.State s u d (bcs ++ bstore) h i v
addToBStore _ st = st

--------------------------------------------------------------------------------

ustoreToStack :: Herb s c => S.UStore c -> S.Stack c
ustoreToStack = map (S.UserCons . (\(S.Num uc _) -> uc))

stackToUStore :: Herb s c => S.Stack c -> S.UStore c
stackToUStore ss =
  let ucs =
        concatMap
          ( \s -> case s of
              S.UserCons c -> [c]
              _ -> []
          )
          ss
   in map (flip S.Num (-99)) ucs
