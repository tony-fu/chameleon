--------------------------------------------------------------------------------
--
-- Copyright (C) 2005 The Chameleon Team
--
-- This program is free software; you can redistribute it and/or modify it
-- under the terms of the GNU General Public License as published by the Free
-- Software Foundation; either version 2 of the License, or (at your option)
-- any later version. This program is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU General Public License for more details.
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
--
--------------------------------------------------------------------------------
--
-- Implements the standard Chameleon pipeline.
-- The pipeline processes a single module from parsing to backend output.
--
--------------------------------------------------------------------------------

module System.Pipeline where

-- import System

-- modules

-- parsing the source file

-- external AST checks

-- XHaskell translation

-- desugaring

-- module export processing

-- internal AST checks

-- internal AST analysis
import AST.CallGraph
import AST.ChParser.Parser
import AST.Common (Id, anonId, idStr)
import AST.Desugarer
import AST.Exports
-- import AST.ExtChecks
import AST.IntChecks
import AST.Internal (Dec)
import TypeError.ErrorDetail
-- import AST.RETrans
-- CHR generation

-- type inference

-- kind validation Stuff

-- back-ends
-- import AST.Reorder
import AST.SrcInfo
-- import Backends.Cleanup
-- import qualified Backends.Scheme.GenerateLazy as SchemeLazy
-- import qualified Backends.Scheme.GenerateStrict as Scheme
import Config.Global as Conf
import Control.Monad
import Core.BuiltIn
import Core.CHRGenerator
import Core.InfGoal
import Core.InfResult
import Core.Inference
import qualified Core.Kinds as Kinds
import Data.List
import Data.Maybe
import Misc
import Misc.Error
-- import Misc.ErrorMsg
-- import Misc.Files
-- import Misc.Output.ANSI
-- import Misc.Print
import Misc.Result
import System.Control
import System.IO
import System.Modules
import TypeError.ErrorMsg
    ( generateTypeErrorMsg, reportAllTypeErrors )

import Core.Constraint
import Core.Types
-- import TypeError.ErrorDescription
import Core.Name
-- import qualified Backends.Scheme.Generate as Scheme
import Core.ConsOps
import Data.Aeson.Text
import Data.Text.Lazy (unpack)
--------------------------------------------------------------------------------

-- process all module batches
processBatches :: Sys ()
processBatches = do
  stSet modules' []
  bs <- stGet batches
  process bs
  where
    process [] = stSet batches' []
    process bbs@(b : bs) = do
      stSet batches' bbs
      case b of
        [m] -> processModule m
        ms -> processCycle b
      process bs

----------------------------------------

-- process a singleton batch
processModule :: ModuleName -> Sys ()
processModule m = do
  let fn = addChSuffix m
  verb1 ("Processing module in `" ++ fn ++ "'")
  stSet srcFilename' fn
  getSourceText m
  parseSourceText
  checkExternalAST
  desugarExternalAST
  analyseInternalAST
  impted <- getCurrentImported
  validateKinds impted
  validateTypes

----------------------------------------
-- process a cycle of modules
processCycle :: Batch -> Sys ()
processCycle = bug "Support for cyclical modules not implemented yet!"

--------------------------------------------------------------------------------
-- pipeline stages
--------------------------------------------------------------------------------

-- read the source file
getSourceText :: ModuleName -> Sys ()
getSourceText m = do
  ps <- getConfig modulePaths
  esrc <- doIO (tryToReadModule ps (anonId m))
  case esrc of
    Failure es b -> causeFailure es
    Success es src -> do
      doIO (reportErrors es)
      stSet srcProg' src

--------------------------------------------------------------------------------

-- parse source, generating external AST
parseSourceText :: Sys ()
parseSourceText = do
  verb1 "Parsing source code"
  src <- stGet srcProg
  fn <- stGet srcFilename
  n <- freshInt
  let eres = parseProg fn n src
  case eres of
    Failure es b -> causeFailure es
    Success es (rest, scs, n, ast, toks) -> do
      doIO (reportErrors es)
      stSet extProg' ast
      stSet srcTokens' toks
      verb 3 [show ast]
      -- stSet srcCommands' scs

--------------------------------------------------------------------------------

-- check external AST
checkExternalAST :: Sys ()
checkExternalAST = do
  verb1 "Checking external AST"
  return ()

--------------------------------------------------------------------------------

-- XHaskell translation
-- xhaskellTranslate :: Sys ()
-- xhaskellTranslate = do
--   xh <- getConfig xhaskellExt
--   when xh
--     $ do
--       verb1 "XHaskell Translation"
--       ext_ast <- stGet extProg
--       let ext_ast' = transProg ext_ast
--       stSet extProg' ext_ast'

--------------------------------------------------------------------------------

-- desugar the external AST, converting it to an internal AST
desugarExternalAST :: Sys ()
desugarExternalAST = do
  verb1 "Desugaring AST"
  ext_ast <- stGet extProg
  opts <- stGet config
  let (int_ast, info) = desugarProg opts ext_ast
  verb 3 [show int_ast]
  stSet intProg' int_ast
  stSet srcInfos' info

--------------------------------------------------------------------------------

-- analyse internal AST:
--  - perform various syntactic checks
--  - find all names/declarations in scope
--  - fully qualify all module names
--  - find all actual exports (top-level things)
--  - identify all let-bound vars.
--  - generate call graphs
--  - discover recursive functions
analyseInternalAST :: Sys ()
analyseInternalAST = do
  verb1 "Analysing internal AST"
  int_ast <- stGet intProg

  verb1 "Checking internal AST"
  srcT <- getSrcTextData
  -- Delayed to the end of analyseInternalAST
  -- doIO (internalASTChecks int_ast srcT)

  -- this module's name, and filename
  id <- currentModuleName
  fn <- stGet srcFilename

  -- calculate and qualify local (to the module), top-level declarations
  -- (these aren't necessarily exported)
  let exs = progExportable int_ast
      qexs = map (qualifyExported id) exs
  verb 3 ["Exports: " ++ show exs]
  -- doIO $ putStrLn "Exported:"
  -- mapM_ (doIO . print) exs



  moduleInfos <- stGet modules
  -- qualify all names -- this is the part that causes all the trouble
  -- doIO $ putStrLn "Adding imports and fully qualifying all names"
  let moduleExports = (id, qexs) : [(id, exs) | ModuleInfo id _ exs _ _ _ <- moduleInfos]
      (int_ast', ims, imxs) = qualifyNamesProg moduleExports int_ast
  -- re-calculate exports (it's cheap) now that they're name qualified
  --------------------------------------------------------
  stSet intProg' int_ast'
  let exs = progExports int_ast' -- qexs

  --------------------------------------------------------

  -- add module info
  let mi = ModuleInfo id fn exs imxs ims []
  replaceModuleInfo mi

  -- call graph stuff
  cg <- doIO (progCallGraph int_ast')
  ncg <- doIO (normaliseCallGraph cg)
  ids <- doIO (callGraphVars cg)
  rids <- doIO (recursiveVars ncg)
  stSet callGraph' cg
  stSet normCallGraph' ncg
  stSet letIds' ids
  stSet recursiveIds' rids

  -- Retriving imported declarations

  -- intChk <- getConfig checkIntSyntax
  -- when intChk
  --   $ do
  --     impted <- getCurrentImported
  --     let (imptTypes, imptVals, imptDecs) = buildImportedEnv impted
  --     let imptTypes' = filterPrimitives imptTypes
  --         imptVals' = filterPrimitives imptVals
  --     doIO $ putStrLn "Imported Types:"
  --     doIO $ mapM (putStrLn . show) imptTypes
  --     doIO $ putStrLn "Imported Values:"
  --     doIO $ mapM (putStrLn . show) imptVals
  --     doIO $ putStrLn "Imported Declaratios:"
  --     doIO $ mapM (putStrLn . show) imptDecs

  --     doIO $ putStrLn "Imported Types (Primitive):"
  --     doIO $ mapM (putStrLn . show) imptTypes'
  --     doIO $ putStrLn "Imported Values (Primitive):"
  --     doIO $ mapM (putStrLn . show) imptVals'

  --     doIO (internalASTChecks imptTypes' imptVals' imptDecs int_ast' srcT)

--------------------------------------------------------------------------------

validateKinds :: [Imported] -> Sys ()
validateKinds impted = do
  chk <- getConfig checkKinds
  when
    chk
    ( do
        --id       <- currentModuleName
        --Just mod <- getModuleInfoById id
        --depMods  <- getAllDepMods (modulesImported mod) id
        let impdecs = filterImports impted
        --doIO (putStr ("\n"++(pretty mod)++"\n"))
        verb1 "Validating kinds"
        int_ast <- stGet intProg
        options <- stGet config
        srcT <- getSrcTextData
        dec <- doIO (Kinds.pile int_ast)
        let fulldec = dec ++ impdecs
        (verbMsg, es) <- doIO (Kinds.kindValidate srcT fulldec (mimicGHC options))
        verb1 verbMsg
        unless (null es) $ do
          causeFailure es
        verb1 "Kind validation successful"
    )

getCurrentImported :: Sys [Imported]
getCurrentImported = do
  id <- currentModuleName
  Just mod <- getModuleInfoById id
  depMods <- getAllDepMods (modulesImported mod) id
  getImportedDecs depMods

getAllDepMods :: [IdStr] -> IdStr -> Sys [IdStr]
getAllDepMods (id : ids) ident = do
  ids' <- getAllDepMods ids ident
  if id == ident 
    then do return ids'
    else do
      Just mod <- getModuleInfoById id
      return (nub (modulesImported mod ++ ids'))
getAllDepMods [] _ = return []

getImportedDecs :: [IdStr] -> Sys [Imported]
getImportedDecs (id : ids) = do
  Just (ModuleInfo _ _ xDecs iDecs imps _) <- getModuleInfoById id
  --let xDecs' = filterImports xDecs
  xDecs' <- getImportedDecs ids
  return (xDecs ++ xDecs')
getImportedDecs [] = return []

buildImportedEnv :: [Imported] -> ([Id], [Id], [Dec])
buildImportedEnv (imp : imps) =
  let (typeIds, valIds, classDecs) = buildImportedEnv imps
   in case imp of
        XVal id -> (typeIds, id : valIds, classDecs)
        XType id _ -> (id : typeIds, valIds, classDecs)
        XClass id dec -> case isPrimitive (idStr id) of
          False -> (typeIds, valIds, dec : classDecs)
          True -> (typeIds, valIds, classDecs)
          where
            isPrimitive str =
              case str of
                'C' : 'h' : 'a' : 'm' : 'e' : 'l' : 'e' : 'o' : 'n' : '.' : _ -> True
                _ -> False
        _ -> (typeIds, valIds, classDecs)
buildImportedEnv [] = ([], [], [])

-- Why do we need to do this ?
filterPrimitives :: [Id] -> [Id]
filterPrimitives (id : ids) =
  case (idStr id) of
    'C' : 'h' : 'a' : 'm' : 'e' : 'l' : 'e' : 'o' : 'n' : '.' : _ -> filterPrimitives ids
    _ -> id : (filterPrimitives ids)
filterPrimitives [] = []

filterImports :: [Imported] -> [Dec]
filterImports (imp : imps) = case imp of
  XType _ d -> d : (filterImports imps)
  XClass _ d -> d : (filterImports imps)
  _ -> filterImports imps
filterImports [] = []

--------------------------------------------------------------------------------

validateTypes :: Sys ()
validateTypes = do
  generateCHRsGoals
  inferAllTypes

-- generates all the CHRs and inference goals
-- also simplify the CHRs
generateCHRsGoals :: Sys ()
generateCHRsGoals = do
  verb1 "Generating CHR rules and goals"
  int_ast <- stGet intProg
  recs <- stGet recursiveIds
  id <- currentModuleName
  Just (ModuleInfo _ _ _ imxs ims _) <- getModuleInfoById id
  jmis <- mapM getModuleInfoById ims
  let imstrs = map (idStr . impId) imxs
      mis = map (fromJustMsg "X") jmis
      res =
        [ (f, (t, c)) | ModuleInfo mod_id _ _ _ _ irs <- mis, ir <- irs, isInfSuccess ir, let f = idStr (resId ir)
                                                                                              t = head (resTLV ir)
                                                                                              c = resCons ir, f `elem` imstrs
        ]

  -- retrieve all the imported class and type declarations and add them
  let impdecs = mapMaybe impDec imxs
  verb 3 ["Imported Non-Val. Decs. ", show impdecs]

  ecgs <- doIO (progCHRsGoals int_ast res impdecs recs)
  case ecgs of
    Failure es _ -> causeFailure es
    Success es (chrs, igs) -> do
      doIO (reportErrors es)
      let chrs' = builtInCHRs ++ chrs
      stSet originalCHRs' chrs'
      stSet infGoals' igs
      -- if at verb. level 2 or higher, show the CHRs
      -- doIO (
      --   writeFile 
      --   ("./intermediate/" ++ id ++ ".txt") 
      --   ("CHRs:" ++ concatMap (('\n':) . pretty) chrs ++ "\n\nInference Goals:" ++ concatMap (('\n':) . pretty)  igs)
      --   )
      verb 2 ["CHRs:"]
      verb 2 (map pretty chrs)
      verb 2 ["Inference Goals:"]
      verb 2 (map pretty igs) 

----------------------------------------

inferAllTypes :: Sys ()
inferAllTypes = do
  verb1 "Inferring all types"
  igs <- stGet infGoals
  -- just do top-level variables
  let igs' = [ig | ig <- igs, infTop ig]

  -- we don't generate any simplified CHRs yet
  chrs <- stGet originalCHRs
  res <- doIO (runInfGoals chrs igs')
  stSet infResults' res

  id <- currentModuleName
  -- updateModuleInfoById fn (moduleInfResults' (const res))
  Just (ModuleInfo d f x i m _) <- getModuleInfoById id
  replaceModuleInfo (ModuleInfo d f x i m res)

  -- Find out what the user-specified last stage is supposed to be. If it's
  -- ValidateTypes, and we're on the last module batch, we should print the
  -- type information, then stop.
  bs <- stGet batches
  ls <- getConfig lastStage
  let stop = singleton bs && ls == ValidateTypes

  -- if at verb level 2 or higher, or we're about to stop, report the types
  vb <- asVerboseAs 2
  -- when (vb || stop) (reportTypes stderr)
  -- handleTypeErrors
  reportTypeDiagnosis
  -- doIO makeErrorDetail

  when
    stop
    ( let err = Error NonFatal anonSrcInfo "Reached final stage"
       in causeFailure [err]
    )
  where
    -- deal with the type errors
    -- handleTypeErrors

    reportTypes :: Handle -> Sys ()
    reportTypes h = do
      res <- stGet infResults
      let out = unlines (map formatInfResult res)
      hPuts h out

    reportTypeDiagnosis :: Sys ()
    reportTypeDiagnosis = do 
      res <- stGet infResults
      ast <- stGet extProg
      sd <- getSrcTextData
      fn <- stGet srcFilename
      doIO $ do
      --   details <- handleResults ast sd fn res
      --   let nonEmptyDetails = catMaybes details
      --   when (notNull nonEmptyDetails) $ 
      --     putStrLn $ unpack $ encodeToLazyText (catMaybes details)
      -- where handleResults ast sd fn rs = mapM (getSatSubset ast sd fn) rs
        details <- getChameleonResult res sd fn
        when (filepath details /= "Primitive.hs") $ do
          putStrLn $ unpack $ encodeToLazyText details
          return ()

    handleTypeErrors :: Sys ()
    handleTypeErrors = do
      res <- stGet infResults
      let res' = filter Core.InfResult.isFailure res
      unless (null res') $ do
        sd <- getSrcTextData
        json <- getConfig jsonOutput
        if json
          then do
            doIO (mapM (generateTypeErrorMsg json sd) res')
            return ()
          else do
            es <- doIO (mapM (generateTypeErrorMsg json sd) res')
            causeFailure es
            return ()

--------------------------------------------------------------------------------

-- produces target code for current module
-- backEnd :: Sys ()
-- backEnd = do
--   back <- getConfig backend
--   prog0 <- stGet intProg
--   let prog = cleanupAST prog0
--   case back of
--     NoBackEnd -> return ()
--     SchemeBackEnd -> do
--       verb1 "Generating Strict Scheme Code"
--       scheme Scheme.generate prog
--     SchemeLazyBackEnd -> do
--       verb1 "Generating Lazy Scheme Code"
--       scheme SchemeLazy.generate prog
--   where
--     scheme :: (Prog -> String) -> Prog -> Sys ()
--     scheme trans prog = do
--       -- first place binding groups in dependency order
--       ncg <- stGet normCallGraph
--       prog' <- doIO (reorderBindingGroups ncg prog)
--       fn <- stGet srcFilename
--       let fn' = chToSchemeSuffix fn
--           out = trans prog'
--       res <- doIO (writeFileContents fn' out)
--       checkE res

--------------------------------------------------------------------------------

{-
test :: Sys ()
test = do
    let a = TVar (mkVar "a")
        b = TVar (mkVar "b")
        k = TVar . mkVar . ('K':) . show
        star  = TCon (mkFreeName "*")
        arrow = TCon (mkFreeName "->")
        arr t1 t2 = TApp (TApp arrow t1) t2

        eqs = [  k 8 =:= (k 6 `arr` (k 7 `arr` star))
                ,k 6 =:= k 10
                ,k 7 =:= k 11
                ,k 10 =:= (k 11 `arr` k 9)
                ,k 9  =:= star
                ,k 8  =:= (k 1 `arr` (k 2 `arr` star))
                ,k 14 =:= (k 12 `arr` (k 13 `arr` star))
                ,k 17 =:= (k 1 `arr` (k 2 `arr` star))
                ,k 12 =:= k 18
                ,k 17 =:= (k 18 `arr` k 16)
                ,k 13 =:= k 19
                ,k 16 =:= (k 19 `arr` k 15)
                ,k 12 =:= k 20
                ,k 15 =:= star
                ,k 20 =:= star
                ,(k 14 =+= (k 4 `arr` (k 5 `arr` star))) (J [14]) ]

        goal = InfGoal (anonId "test") True [] (toConstraint eqs)
    puts ("\ngoal:\n" ++ pretty goal)
    res <- doIO (runInfGoals [] [goal])
    puts ("\n\nres :\n" ++ pretty res ++ "\n\n")
-}
