module AST.NativeParser where

import qualified AST.External as Ext
import AST.SrcCommand (SrcCommand)
import AST.SrcInfo (Many (Many), SrcInfo (..), anonSrcInfo)
import qualified AST.Token as AST
import Data.Maybe (fromMaybe)
import Data.Ratio (denominator, numerator)
import GHC
import GHC.Paths (libdir)
import GhcPlugins
import Misc (UniqueNum)
import Misc.Result (Result (..))
import Bag

debugParser :: String -> Ghc ()
debugParser = liftIO . putStrLn

unlocate :: Located x -> x
unlocate (L _ x) = x

locate :: Located x -> SrcSpan
locate (L x _) = x

parseProg ::
  String ->
  UniqueNum ->
  String ->
  Result () (String, [SrcCommand], UniqueNum, Ext.Prog, [AST.Token])
parseProg fileName n src = undefined

testParser :: String -> Ghc Ext.Prog
testParser fileName = do
  setSessionDynFlags =<< getSessionDynFlags
  setTargets
    [ Target (TargetFile fileName Nothing) False Nothing
    ]
  mdGraph <- depanal [] False
  let mdSummaries = mgModSummaries mdGraph
  mds <- parseModuleSummaries mdSummaries
  return $ Ext.Prog mds

parseModuleSummaries :: [ModSummary] -> Ghc [Ext.Module]
parseModuleSummaries [] = return []
parseModuleSummaries (m : ms) = do
  md <- parseModuleSummary m
  mds <- parseModuleSummaries ms
  return $ md : mds

parseModuleSummary :: ModSummary -> Ghc Ext.Module
parseModuleSummary mdlSummary = do
  pasedMdl <- parseModule mdlSummary
  let mdl = ms_mod mdlSummary
      mdlName = moduleName mdl
      mdlNameString = moduleNameString mdlName
      mdlLocation = ms_location mdlSummary
      mdlHsFilePath = fromMaybe "<no file>" (ml_hs_file mdlLocation)
      parsedSrc = getParsedSource pasedMdl
      hsMod = unlocate parsedSrc
      decls = hsmodDecls hsMod
  debugParser $ "Module File Path: " ++ mdlHsFilePath
  debugParser $ "Module Name: " ++ mdlNameString
  decs <- parseDecls decls
  let id = Ext.mkId (SrcInfo 1 mdlHsFilePath 1 1 1) mdlNameString
  return $ Ext.Module id Ext.ExAll [] decs

getParsedSource :: ParsedModule -> ParsedSource
getParsedSource (ParsedModule _ x _ _) = x

parseDecls :: [LHsDecl GhcPs] -> Ghc [Ext.Dec]
parseDecls [] = return []
parseDecls (d : ds) = do
  dec <- parseDecl (unlocate d)
  decs <- parseDecls ds
  return $ dec:decs

parseDecl :: HsDecl GhcPs -> Ghc Ext.Dec
parseDecl (ValD _ binding) = parseBinding binding

parseDecl (TyClD a b) = error "Unknown Parser: Parse Decl (Type declaration)"
parseDecl (InstD a b) = error "Unknown Parser: Parse Decl (Instance declaration)"
parseDecl (DerivD a b) = error "Unknown Parser: Parse Decl (Deriving declaration)"
parseDecl (SigD a b) = error "Unknown Parser: Parse Decl (Signature declaration)"
parseDecl (DefD a b) = error "Unknown Parser: Parse Decl (Default declaration)"
parseDecl (ForD a b) = error "Unknown Parser: Parse Decl (Foreign declaration)"
parseDecl (WarningD a b) = error "Unknown Parser: Parse Decl (Warning declaration)"
parseDecl (AnnD a b) = error "Unknown Parser: Parse Decl (Annotation declaration)"
parseDecl (RuleD a b) = error "Unknown Parser: Parse Decl (Rule declaration)"
parseDecl (SpliceD a b) = error "Unknown Parser: Parse Decl (Splice declaration)"
parseDecl (DocD a b) = error "Unknown Parser: Parse Decl (Doc declaration)"
parseDecl (RoleAnnotD a b) = error "Unknown Parser: Parse Decl (Role declaration)"
parseDecl (XHsDecl a) = error "Unknown Parser: Parse Decl (Unknown declaration)"


parseLocalBinding :: LHsLocalBinds GhcPs -> Ghc [Ext.Dec]
parseLocalBinding bindings' = do
  let binding = unlocate bindings'
  case binding of 
    HsValBinds _ varbinds  -> do
      case varbinds of
        ValBinds _ baggedBinds sigs -> do
          let binds = bagToList baggedBinds
          mapM (parseBinding . unlocate) binds
    EmptyLocalBinds  _  -> return []
    HsIPBinds _ _ -> error "HsIPBinds"
    XHsLocalBindsLR  _  -> error "XHsLocalBindsLR"


parseBinding :: HsBind GhcPs -> Ghc Ext.Dec
parseBinding (FunBind _ fId mgroup _ _) = do
  let (L srcSpan rdrname) = fId
      srcInfo = srcSpanToSrcInfo srcSpan
  case mgroup of
    MG _ alts origin -> do
      defs <- parseMatches (unlocate alts)
      return $ Ext.ValDec srcInfo defs
    _ -> error "Unkonw match group"
-- parseBinding  (VarBind ext vid vrhs vinline) = do
--     liftIO $ print vinline
-- parseBinding  (PatBind _ _ _ _ ) = debugParser "Pattern Binding"
-- parseBinding  (AbsBinds _ _ _ _ _ _ _ ) = debugParser "Abstraction Binding"
-- parseBinding  (PatSynBind _ _ ) = debugParser "PatSynBind Binding"
-- parseBinding  (XHsBindsLR _ ) = debugParser "XHsBindsLR Binding"
parseBinding _ = error "Unknown binding"

parseMatches :: [LMatch GhcPs (LHsExpr GhcPs)] -> Ghc [Ext.Def]
parseMatches [] = return []
parseMatches (m : ms) = do
  case unlocate m of
    Match _ cont pats grhss -> do
      case cont of
        FunRhs matching_fun _ _ -> do
          let rdrName = unlocate matching_fun
              srcSpan = locate matching_fun
              id = rdrNameToId rdrName srcSpan
          pats <- parsePatterns pats
          let (GRHSs _ rhss wheres) = grhss
          rhs <- parseRHSs rhss
          rest <- (parseMatches ms)
          whrs <- parseLocalBinding wheres
          return $ Ext.Def id pats rhs whrs : rest
        _ -> error "Unknown matching context"
    _ -> error "Unknown mathcing"

parsePatterns :: [LPat GhcPs] -> Ghc [Ext.Pat]
parsePatterns [] = return []
parsePatterns (p : ps) = do
  pat' <- parsePattern p
  pats' <- parsePatterns ps
  return $ pat' : pats'

parsePattern :: LPat GhcPs -> Ghc Ext.Pat
parsePattern (VarPat _ x) = do
  let rdrName = unlocate x
      srcSpan = locate x
  return $ Ext.VarPat (rdrNameToId rdrName srcSpan)
parsePattern (LitPat _ x) = do
  lit <- parseLiteral x
  return $ Ext.LitPat lit
parsePattern (AsPat _ x pat) = do
  let rdrName = unlocate x
      srcSpan = locate x
      srcInfo = srcSpanToSrcInfo srcSpan
  pat' <- parsePattern pat
  return $ Ext.AsPat srcInfo (rdrNameToId rdrName srcSpan) pat'
parsePattern (ListPat _ x) = do
  ps <- parsePatterns x
  return $ Ext.ListPat anonSrcInfo ps
parsePattern (TuplePat _ x boxed) = do
  ps <- parsePatterns x
  return $ Ext.TupPat anonSrcInfo ps
parsePattern (ConPatIn locatedId conPatDetails) = do
  let rdrName = unlocate locatedId
      srcSpan = locate locatedId
      id = rdrNameToId rdrName srcSpan
  pats <- parseConPatDetails conPatDetails
  return $ Ext.ConPat anonSrcInfo id pats
parsePattern (WildPat _) = error "Unimplemnted: WildPat"
parsePattern (LazyPat _ _) = error "Unimplemnted: LazyPat"
parsePattern (BangPat _ _) = error "Unimplemnted: BangPat"
parsePattern (SumPat _ _ _ _) = error "Unimplemnted: SumPat"
parsePattern (ConPatOut _ _ _ _ _ _ _) = error "Unimplemnted: ConPatOut"
parsePattern (ViewPat _ _ _) = error "Unimplemnted: ViewPat"
parsePattern (SplicePat _ _) = error "Unimplemnted: SplicePat"
parsePattern (NPat _ _ _ _) = error "Unimplemnted: NPat"
parsePattern (NPlusKPat _ _ _ _ _ _) = error "Unimplemnted: NPlusKPat"
parsePattern (SigPat _ _ _) = error "Unimplemnted: SigPat"
parsePattern (CoPat _ _ _ _) = error "Unimplemnted: CoPat "
parsePattern (XPat a) = parsePattern (unlocate a) -- I don't know why but: all parttens are wrapped under (XPat Located pat)
parsePattern _ = error "Unknown pattern"

parseConPatDetails :: HsConPatDetails GhcPs -> Ghc [Ext.Pat]
parseConPatDetails (PrefixCon args) = parsePatterns args
parseConPatDetails (InfixCon arg1 arg2) = parsePatterns [arg1, arg2]
parseConPatDetails _ = error "Unknown constructor pattern"

-- TODO: Record fields

parseRHSs :: [LGRHS GhcPs (LHsExpr GhcPs)] -> Ghc Ext.RHS
parseRHSs lgrhss = parseGuardedRHSs (map unlocate lgrhss)

parseGuardedRHSs :: [GRHS GhcPs (Located (HsExpr GhcPs))] -> Ghc Ext.RHS
parseGuardedRHSs [(GRHS _ [] body)] = do
  exp <- parseExpression (unlocate body)
  return $ Ext.RHS1 exp

parseGuardedRHSs rhs = do
  guardedExpressions <- go rhs
  return $ Ext.RHSG guardedExpressions
  where go :: [GRHS GhcPs (Located (HsExpr GhcPs))] -> Ghc [Ext.GdExp]
        go [] = return []
        go ((GRHS _ locatedStatements body):gs) = do
          exp <- parseExpression (unlocate body)
          guards <- parseStatements (map unlocate locatedStatements)
          rest <- go gs 
          return $ (Ext.GdExp anonSrcInfo (head guards) exp):rest


parseStatements :: [StmtLR GhcPs GhcPs (LHsExpr GhcPs)] -> Ghc [Ext.Exp]
parseStatements [] = return []
parseStatements (s:ss) = do
  stmt <- parseStatement s
  stmts <- parseStatements ss
  return (stmt:stmts)

parseStatement:: StmtLR GhcPs GhcPs (LHsExpr GhcPs) -> Ghc Ext.Exp
parseStatement (LastStmt _ _ _ _) = error "Not implemented: LastStmt"
parseStatement (BindStmt _ _ _ _ _) = error "Not implemented: BindStmt"
parseStatement (ApplicativeStmt _ _ _) = error "Not implemented: ApplicativeStmt"
parseStatement (BodyStmt _ x _ _) = parseExpression (unlocate x)
parseStatement (LetStmt _ _) = error "Not implemented: LetStmt"
parseStatement (ParStmt _ _ _ _) = error "Not implemented: ParStmt"
parseStatement (TransStmt {}) = error "Not implemented: TransStmt"
parseStatement (RecStmt {}) = error "Not implemented: RecStmt"
parseStatement (XStmtLR _) = error "Not implemented: XStmtLR"


parseExpression :: HsExpr GhcPs -> Ghc Ext.Exp
parseExpression (HsLit a b) = do
  lit <- parseLiteral b
  return $ Ext.LitExp lit
parseExpression (HsVar a b) = do
  let rdrName = unlocate b
      srcSpan = locate b
      id = rdrNameToId rdrName srcSpan
  return $ Ext.VarExp id
parseExpression (HsUnboundVar a b) = do
  debugParser "Unbound Var"
  undefined
parseExpression (HsConLikeOut a b) = do
  debugParser "Constructor like thing"
  undefined
parseExpression (HsRecFld a b) = do
  debugParser "Record Field"
  undefined
parseExpression (HsOverLabel a b c) = debugParser "Loverload Label" >> undefined
parseExpression (HsIPVar a b) = debugParser "HsIPVar" >> undefined
parseExpression (HsOverLit a b) = do
  case b of
    OverLit _ val witness -> do
      case val of
        HsIntegral x -> Ext.LitExp <$> parseIntegerLiteral x
        HsFractional x -> Ext.LitExp <$> parseFractionalLit x
        HsIsString _ x -> Ext.LitExp <$> parseStringLit x
    _ -> error "Unknown overloaded literal"
parseExpression (HsLam _ matchgroup) = do
  debugParser "Lambda Abstraction"
  case matchgroup of
    MG _ alts origin -> do
      let matches = unlocate alts
      debugParser $ "Number of matches "
      case fmap unlocate matches of
        [Match _ cont pats grhss] -> do
          pats <- parsePatterns pats
          let (GRHSs _ rhss wheres) = grhss
          case ( unlocate <$> rhss) of
            [(GRHS _ [] body)] -> do
              exp <- parseExpression (unlocate body)
              return $ Ext.AbsExp anonSrcInfo pats exp
      
parseExpression (HsLamCase a b) = debugParser "HsLamCase" >> undefined
parseExpression (HsApp a b c) = do
  x1 <- parseExpression (unlocate b)
  x2 <- parseExpression (unlocate c)
  return $ Ext.AppExp anonSrcInfo x1 x2
parseExpression (OpApp a b c d) = do
  x1 <- parseExpression (unlocate b)
  x2 <- parseExpression (unlocate c)
  x3 <- parseExpression (unlocate d)
  return $ Ext.IfxAppExp anonSrcInfo x1 x2 x3
parseExpression (HsPar _ exp) = parseExpression (unlocate exp)
parseExpression (HsLet _ binding' exp') = do
  exp <- parseExpression (unlocate exp')
  decs <- parseLocalBinding binding'
  return $ Ext.LetExp anonSrcInfo decs exp

parseExpression (HsAppType a b c) = debugParser "HsAppType" >> undefined
parseExpression (NegApp  a b c) = debugParser "NegType" >> undefined
parseExpression (HsBracket a b) = debugParser "HsBracket " >> undefined

parseLiteral :: HsLit x -> Ghc Ext.Lit
parseLiteral (HsInt _ x) = parseIntegerLiteral x
parseLiteral (HsString _ x) = parseStringLit x
parseLiteral (HsChar _ x) = parseCharLit x
parseLiteral (HsRat a b c) = parseFractionalLit b
parseLiteral _ = error "Unknown Literal Type"

parseIntegerLiteral :: IntegralLit -> Ghc Ext.Lit
parseIntegerLiteral (IL txt neg v) =
  return $ Ext.IntegerLit anonSrcInfo v (extractSourceText txt)

parseFractionalLit :: FractionalLit -> Ghc Ext.Lit
parseFractionalLit (FL txt neg v) =
  return $
    Ext.FloatLit
      anonSrcInfo
      (fromRational v)
      (numerator v, denominator v)
      (extractSourceText txt)

parseStringLit :: FastString -> Ghc Ext.Lit
parseStringLit fs =
  return $
    Ext.StrLit
      anonSrcInfo
      (unpackFS fs)

parseCharLit :: Char -> Ghc Ext.Lit
parseCharLit c =
  return $
    Ext.CharLit
      anonSrcInfo
      c

-- Some one to one translation from GHC type to Chameleon type
extractSourceText :: SourceText -> String
extractSourceText (SourceText x) = x
extractSourceText _ = ""

locatedRdrNameToId :: Located RdrName -> Ext.Id
locatedRdrNameToId (L srcSpan (Unqual occname)) = 
  Ext.Id (srcSpanToSrcInfo srcSpan) (occNameString occname) (occNameString occname)
locatedRdrNameToId (L srcSpan (Qual mdlname occname)) = 
  Ext.Id (srcSpanToSrcInfo srcSpan) (occNameString occname) (occNameString occname)
locatedRdrNameToId (L _ _) = error "Unknown reader name"

rdrNameToId :: RdrName -> SrcSpan -> Ext.Id
rdrNameToId (Unqual occname) _ = Ext.Id anonSrcInfo (occNameString occname) (occNameString occname)
rdrNameToId (Qual mdlname occname) _ = Ext.Id anonSrcInfo (occNameString occname) (occNameString occname)
rdrNameToId _ _ = error "Unknown reader name"

srcSpanToSrcInfo :: SrcSpan -> SrcInfo
srcSpanToSrcInfo r@(RealSrcSpan srcSpan) = 
  let file = srcSpanFile srcSpan
      (RealSrcLoc start) = srcSpanStart r
      (RealSrcLoc end) = srcSpanEnd r
      startL = srcLocLine start
      startC = srcLocCol start
      endL = srcLocLine end
      endC = srcLocCol end
  in SrcInfo 0 (unpackFS file) startL startC 1

main :: IO ()
main = do
  prog <- runGhc (Just libdir) (testParser "Example.hs")
  print prog
  return ()
