module AST.ChParser.Test where

import AST.ChParser.Parser
import AST.Desugarer
import AST.External as E
import AST.Internal as I
-- import AST.RETrans
import Config.Global
import Control.Monad
import System.IO
import Misc
import Misc.Result
import System.Environment
import System.Exit
import AST.ChParser.Tokens
import AST.Token
--import System.Control

main = do
  as <- getArgs
  case as of
    [] -> putStrLn "no filename specified!" >> exitFailure
    (f : _) -> do
      h <- openFile f ReadMode
      str <- hGetContents h
      let eres = parseProg f 0 str
      putStrLn "\n *** EXTERNAL AST *** "
      print eres
      case eres of
        Failure es b -> exitFailure
        -- don't care, it should be (causeFailure es ... )
        Success es (_rest, scs, n, ast, toks) ->
          ( do
              mapM_ (\tok -> do
                  putStrLn $ "Token: " ++  tokenString tok
                  putStrLn $ "Length: " ++  show (tokenLength tok)
                  putStrLn "----"
                ) toks
              -- let e@(E.Prog ((E.Module _ _ _ edecs) : _)) = ast
              -- let ped = pretty edecs
              -- putStrLn ped
              -- putStrLn "\n *** RE trans *** \n"
              -- let ast'@(E.Prog ((E.Module _ _ _ tdecs) : _)) = (transProg ast)
              -- putStrLn (pretty tdecs)

              -- let int_ast = desugarProg opts ast
              -- let (prg, jst) = int_ast
              -- let (I.Prog ((I.Module _ _ _ decs) : _)) = prg
              -- let pd = pretty decs
              -- putStrLn "\n *** INTERNAL AST *** "
              -- putStrLn pd
              -- print int_ast
          )
  where
    opts =
      Options
        { colourText = True,
          installPath = "",
          verbosity = VerbLevel 0,
          inputFiles = [],
          outputFile = defaultOutputFilename,
          modulePaths = [".", ""],
          backend = defaultBackEnd,
          importImplicit = True,
          checkKinds = True,
          checkIntSyntax = False,
          xhaskellExt = True,
          lastStage = AllDone,
          mimicGHC = False,
          jsonOutput = False
        }
