--------------------------------------------------------------------------------
--
-- Copyright (C) 2004 The Chameleon Team
--
-- This program is free software; you can redistribute it and/or modify it
-- under the terms of the GNU General Public License as published by the Free
-- Software Foundation; either version 2 of the License, or (at your option)
-- any later version. This program is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU General Public License for more details.
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
--
--------------------------------------------------------------------------------
--

--------------------------------------------------------------------------------

-- | Tokens for the Chameleon parser.
module AST.ChParser.Tokens where

import qualified AST.Token as AST

--------------------------------------------------------------------------------
-- simple position information

-- module name, row and column of source
type SrcPos = (String, Int, Int)

-- | Refers to no specific source position. (Note though that this is not a
-- unique reference. All of these `noSrcPos' things are equivalent.)
noSrcPos :: SrcPos
noSrcPos = AST.noSrcPos

-- | Something paired with a source position.
type Src a = (a, SrcPos)

-- | Formats a `SrcPos' as a fairly verbose string.
formatSrcPosVerbose :: SrcPos -> String
formatSrcPosVerbose (mod, x, y) = "line " ++ show x ++ ", col. " ++ show y

----------------------------------------

-- | Creates a token without any `Src'. (Actually it's _|_.)
mkNoSrc :: (Src a -> Token) -> Token
mkNoSrc tk = tk (error "AST.ChParser.Tokens: token has an undefined Src field")

-- | Gets the value (non-source info.) part of a `Src' pairing.
srcVal :: Src a -> a
srcVal = fst

-- | Source tokens.
data Token
  = -- | variable id
    TId (Src String)
  | -- | constructor id.
    TConId (Src String)
  | -- | infix id.
    TIId (Src String)
  | -- | int lit.
    TInt (Src (Integer, String))
  | -- | float lit.
    TFloat (Src (Double, String, (Integer, Integer)))
  | -- | char. lit.
    TChar (Src Char)
  | -- | string lit.
    TStr (Src String)
  | -- | `-' (minus sign\\dash)
    TMinus (Src String)
  | -- | rule
    TRule (Src String)
  | -- | overload
    TOverload (Src String)
  | -- | class
    TClass (Src String)
  | -- | instance
    TInstance (Src String)
  | -- | deriving
    TDeriving (Src String)
  | -- | infix
    TInfix (Src String)
  | -- | infixl
    TInfixl (Src String)
  | -- | infixr
    TInfixr (Src String)
  | -- | hiding
    THiding (Src String)
  | -- | hconstraint
    THConstraint (Src String)
  | -- | extern
    TExtern (Src String)
  | -- | module
    TModule (Src String)
  | -- | import
    TImport (Src String)
  | -- | as
    TAs (Src String)
  | -- | qualified
    TQualified (Src String)
  | -- | primitive
    TPrimitive (Src String)
  | -- | data
    TData (Src String)
  | -- | type
    TType (Src String)
  | -- | forall
    TForall (Src String)
  | -- | `;'
    TSemicolon
  | -- | `{'
    TLBrace
  | -- | '}'
    TRBrace
  | -- | `='
    TEquals (Src String)
  | -- | `\\'
    TBackslash (Src String)
  | -- | `->'
    TArrow (Src String)
  | -- | if
    TIf (Src String)
  | -- | then
    TThen (Src String)
  | -- | else
    TElse (Src String)
  | -- | let
    TLet (Src String)
  | -- | in
    TIn (Src String)
  | -- | do
    TDo (Src String)
  | -- | case
    TCase (Src String)
  | -- | of
    TOf (Src String)
  | -- | where
    TWhere (Src String)
  | -- | `('
    TLParen (Src String)
  | -- | `)'
    TRParen (Src String)
  | -- | `['
    TLBracket (Src String)
  | -- | `]'
    TRBracket (Src String)
  | -- | `..'
    TCont (Src String)
  | -- | `_'
    TUnderscore (Src String)
  | -- | `=>'
    TImpl (Src String)
  | -- | `==>'
    TPropArrow (Src String)
  | -- | `<=>'
    TSimpArrow (Src String)
  | -- | `,'
    TComma
  | -- | `.'
    TDot
  | -- | `@'
    TAt (Src String)
  | -- | `|'
    TBar (Src String)
  | -- | `<-'
    TLArrow (Src String)
  | -- | `::'
    TAnn (Src String)
  | -- | `:::'
    TEAnn (Src String)
  | -- | `:*:'
    TRAnn (Src String)
  | -- | True
    TTrue (Src String)
  | -- | False
    TFalse (Src String)
  | -- | `::?'
    TQuery (Src String)
  | -- | '?'
    TQuestion (Src String)
  | -- | Indicates start of a block. (inserted
    --   after let, where, do, of.)
    TLayoutBlock Int
  | -- | First text column in a new line.
    TLayoutFirst Int
  | -- | The EOF that Happy sees
    Teof
  | -- | The EOF that records the last text position
    Tend (Src ())
  deriving (Eq)

----------------------------------------

instance Show Token where
  show tok = case tok of
    TId (str, _) -> str -- ++ "-id"
    TConId (str, _) -> str
    TIId (str, _) -> str -- ++ "-infix"
    TInt ((_, str), _) -> str
    TFloat ((_, str, _), _) -> str
    TChar (char, _) -> show char
    TStr (str, _) -> str
    TMinus _ -> "-" -- ++ "minus"
    TRule _ -> "rule"
    TOverload _ -> "overload"
    TClass _ -> "class"
    TInstance _ -> "instance"
    TDeriving _ -> "deriving"
    TInfix _ -> "infix"
    TInfixl _ -> "infixl"
    TInfixr _ -> "infixr"
    THiding _ -> "hiding"
    THConstraint _ -> "hconstraint"
    TExtern _ -> "extern"
    TModule _ -> "module"
    TImport _ -> "import"
    TAs _ -> "as"
    TQualified _ -> "qualified"
    TPrimitive _ -> "primitive"
    TData _ -> "data"
    TForall _ -> "forall"
    TSemicolon -> ";"
    TLBrace -> "{"
    TRBrace -> "}"
    TEquals _ -> "="
    TBackslash _ -> "\\"
    TArrow _ -> "->"
    TIf _ -> "if"
    TThen _ -> "then"
    TElse _ -> "else"
    TLet _ -> "let"
    TIn _ -> "in"
    TDo _ -> "do"
    TCase _ -> "case"
    TOf _ -> "of"
    TWhere _ -> "where"
    TLParen _ -> "("
    TRParen _ -> ")"
    TLBracket _ -> "["
    TRBracket _ -> "]"
    TUnderscore _ -> "_"
    TImpl _ -> "=>"
    TPropArrow _ -> "==>"
    TSimpArrow _ -> "<==>"
    TComma -> ","
    TDot -> "."
    TBar _ -> "|"
    TLArrow _ -> "<-"
    TAnn _ -> "::"
    TEAnn _ -> ":::"
    TRAnn _ -> ":*:"
    TTrue _ -> "True!"
    TFalse _ -> "False!"
    TQuery _ -> "::?"
    TQuestion _ -> "?"
    TCont _ -> ".."
    TType _ -> "type"
    TAt _ -> "@"
    TLayoutBlock int -> "LAYOUT{" ++ show int ++ "}"
    TLayoutFirst int -> "LAYOUT<" ++ show int ++ ">"
    Teof -> "EOF"
    Tend _ -> "THE_END!"
    -- _ -> "<BUG: some token>"

----------------------------------------

instance AST.StdToken Token where
  tokenString = show

  tokenSrcPos t = case t of
    TId x -> snd x
    TConId x -> snd x
    TIId x -> snd x
    TInt x -> snd x
    TFloat x -> snd x
    TChar x -> snd x
    TStr x -> snd x
    TMinus x -> snd x
    TRule x -> snd x
    TOverload x -> snd x
    TClass x -> snd x
    TInstance x -> snd x
    TDeriving x -> snd x
    THiding x -> snd x
    THConstraint x -> snd x
    TExtern x -> snd x
    TModule x -> snd x
    TImport x -> snd x
    TData x -> snd x
    TType x -> snd x
    TForall x -> snd x
    TEquals x -> snd x
    TBackslash x -> snd x
    TArrow x -> snd x
    TIf x -> snd x
    TThen x -> snd x
    TElse x -> snd x
    TLet x -> snd x
    TIn x -> snd x
    TDo x -> snd x
    TCase x -> snd x
    TOf x -> snd x
    TWhere x -> snd x
    TLParen x -> snd x
    TRParen x -> snd x
    TLBracket x -> snd x
    TRBracket x -> snd x
    TCont x -> snd x
    TUnderscore x -> snd x
    TImpl x -> snd x
    TPropArrow x -> snd x
    TSimpArrow x -> snd x
    TBar x -> snd x
    TLArrow x -> snd x
    TAnn x -> snd x
    TEAnn x -> snd x
    TRAnn x -> snd x
    TTrue x -> snd x
    TFalse x -> snd x
    TQuery x -> snd x
    TQuestion x -> snd x
    Tend x -> snd x
    _ -> noSrcPos

  tokenLength t = case t of
    TId x -> length (fst x)
    TConId x -> length (fst x)
    TIId x -> length (fst x)
    TInt ((_, x), _) -> length x
    TFloat x -> let (_, strPresentation, _) = fst x in length strPresentation
    TChar x -> 3
    TStr x -> length (fst x) + 2
    TMinus x -> 1
    TRule x -> length (fst x)
    TOverload x -> length (fst x)
    TClass x -> length (fst x)
    TInstance x -> length (fst x)
    TDeriving x -> length (fst x)
    THiding x -> length (fst x)
    THConstraint x -> length (fst x)
    TExtern x -> length (fst x)
    TModule x -> length (fst x)
    TImport x -> length (fst x)
    TData x -> length (fst x)
    TType x -> length (fst x)
    TForall x -> length (fst x)
    TEquals x -> length (fst x)
    TBackslash x -> length (fst x)
    TArrow x -> length (fst x)
    TIf x -> 2
    TThen x -> 4
    TElse x -> 4
    TLet x -> 3
    TIn x -> 2
    TDo x -> 2
    TCase x -> 4
    TOf x -> 2
    TWhere x -> 5
    TLParen x -> 1
    TRParen x -> 1
    TLBracket x -> 1
    TRBracket x -> 1
    TCont x -> length (fst x)
    TUnderscore x -> length (fst x)
    TImpl x -> length (fst x)
    TPropArrow x -> length (fst x)
    TSimpArrow x -> length (fst x)
    TBar x -> length (fst x)
    TLArrow x -> length (fst x)
    TAnn x -> 2
    TEAnn x -> length (fst x)
    TRAnn x -> length (fst x)
    TTrue x -> 4
    TFalse x -> 5
    TComma -> 1
    TQuery x -> length (fst x)
    TQuestion x -> length (fst x)
    _ -> 0
