{-# LANGUAGE QuasiQuotes #-}

module AST.Test where
import Text.RawString.QQ
import AST.ChParser.Parser
import Misc.Result
import AST.ChParser.Parser ( parseProg )

src = [r|

le = \ x -> x + 1
ple = (\x -> x + 1)
ls = let x = 3 in x + 2
|]
main = 
  let fn = "Eg.hs"
      n = 1
      eres = parseProg fn n src
  in case eres of
    Failure es b -> do
        print "Failure"
    Success es (rest, scs, n, ast, toks) -> do
    --   stSet extProg' ast
    --   stSet srcCommands' scs
    --   stSet srcTokens' toks
        putStrLn $ show ast
        -- print scs
        -- print toks


-- Prog [
--     Module (Id -1 "Main") 
--     ExAll [] 
--     [
--         ValDec 3 [Def (Id 1 "a") [] (RHS1 (LitExp (IntegerLit 2 1 "1"))) []],
--         ValDec 6 [Def (Id 4 "b") [] (RHS1 (LitExp (IntegerLit 5 2 "2"))) []]
--     ]
-- ]

-- Prog [
--     Module (Id 1 "Example") 
--     ExAll [] 
--     [
--         ValDec 0 [Def (Id -1 "a") [] (RHS1 (LitExp (IntegerLit -1 1 "1"))) []],
--         ValDec 0 [Def (Id -1 "b") [] (RHS1 (LitExp (IntegerLit -1 3 "3"))) []]
--     ]
-- ]

-- Prog [
--     Module (Id 1 "Example") 
--     ExAll [] 
--     [
--         ValDec 0 [Def (Id -1 "a") [] (RHS1 (LitExp (IntegerLit -1 1 "1"))) []],
--         ValDec 0 [Def (Id -1 "b") [
--             VarPat (Id -1 "c")] (
--                 RHSG [
--                     GdExp -1 (LitExp (IntegerLit -1 1 "1")) (LitExp (StrLit -1 "hello"))
--                     ]) []
--                 ]
--         ]
--     ]
-- Prog [
--     Module 
--     (Id 1 "Example") 
--     ExAll [] 
--     [
--         ValDec 0 [Def (Id -1 "a") [] (RHS1 (LitExp (IntegerLit -1 1 "1"))) []],
--         ValDec 0 [Def (Id -1 "b") [
--             VarPat (Id -1 "c")] 
--             (RHS1 (VarExp (Id -1 "c"))) []
--         ]
--     ]
-- ]
