{-# LANGUAGE TypeSynonymInstances #-}
--------------------------------------------------------------------------------
--
-- Copyright (C) 2004 The Chameleon Team
--
-- This program is free software; you can redistribute it and/or modify it
-- under the terms of the GNU General Public License as published by the Free
-- Software Foundation; either version 2 of the License, or (at your option)
-- any later version. This program is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU General Public License for more details.
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
--
--------------------------------------------------------------------------------
--
-- This module defines the AST data structure for Chameleon's external
-- language. See doc/external_AST.txt for details.
--
-- NOTE: Think about moving some of the common parts of the AST out of here and
--         Internal, and put them in a separate module that both import.
--
--------------------------------------------------------------------------------

module AST.External
  ( module AST.External,
    module AST.Common,
  )
where

import AST.Common
import AST.SrcInfo
import Data.Maybe
import Misc
import Misc.Pretty

--------------------------------------------------------------------------------

data Prog = Prog [Module]
  deriving (Eq, Show)

-- a module with a name, export list and local declarations
data Module = Module Id Exports [Import] [Dec]
  deriving (Eq, Show)

----------------------------------------

data Dec
  = RuleDec SrcInfo Rule
  | DataDec SrcInfo Data -- data type
  | TypeDec SrcInfo TSyn -- type synonym
  | AnnDec SrcInfo Sig -- a type signature/annotation
  | ExtDec SrcInfo Extern -- external declaration
  | ValDec SrcInfo [Def] -- function binding
  | PatDec SrcInfo PatBnd -- pattern binding
  | OverDec SrcInfo Over -- overload declaration
  | ClassDec SrcInfo Class -- class declaration
  | InstDec SrcInfo Inst -- instance declaration
  | FixDec SrcInfo Fixity -- fixity declaration
  | PrimDec SrcInfo PrimVal -- primitive declaration
  deriving (Eq, Show)

type IFace = Id

data Fixity = Fix OpAssoc OpPrec [Id]
  deriving (Eq, Show)

type OpPrec = Int

data OpAssoc
  = NonAssoc
  | LeftAssoc
  | RightAssoc
  deriving (Eq, Show)

-- a primitive identifier (with explicit external-name string)
data PrimVal = Prim Id String TSchm
  deriving (Eq, Show)

data Over
  = Over0 Id
  | OverDef Sig [Def]
  deriving (Eq, Show)

data Class = Class Ctxt Pred [FunDep] Where
  deriving (Eq, Show)

data FunDep = FunDep SrcInfo [Id] [Id]
  deriving (Eq, Show)

data Inst = Inst Ctxt Pred Where
  deriving (Eq, Show)

data Extern
  = VarExt Sig -- external var./constructor
  | TConExt Id -- external type constructor
  deriving (Eq, Show)

data Def = Def Id [Pat] RHS Where
  deriving (Eq, Show)

data PatBnd = PatBnd Pat RHS Where
  deriving (Eq, Show)

data RHS
  = RHS1 Exp -- single expression
  | RHSG [GdExp] -- guarded expression(s)
  deriving (Eq, Show)

type Guard = Exp

data GdExp = GdExp SrcInfo Guard Exp
  deriving (Eq, Show)

type Where = [Dec]

data Exp
  = VarExp Id
  | ConExp Id
  | LitExp Lit
  | NegExp SrcInfo Exp
  | AbsExp SrcInfo [Pat] Exp
  | AppExp SrcInfo Exp Exp
  | IfxAppExp SrcInfo Exp Exp Exp -- e1 `e2` e3
  | PIfxAppExp SrcInfo Exp Exp Exp -- parens
  | LetExp SrcInfo [Dec] Exp
  | CaseExp SrcInfo Exp [CaseAlt]
  | DoExp SrcInfo [Stmt]
  | IfExp SrcInfo Exp SrcInfo Exp Exp -- cond and branches have separate locations
  | AnnExp SrcInfo Exp AnnType TSchm
  | TupExp SrcInfo [Exp]
  | ListExp SrcInfo (Many Exp) -- extra info is for site of elem unifier
  | ListCompExp SrcInfo Exp [Stmt]
  | RecExp Id [FieldBind] -- record construct
  | UpdExp Exp [FieldBind] -- record update
  | SecLeftExp SrcInfo Exp Exp -- (e op) operator sectioning
  | SecRightExp SrcInfo Exp Exp -- (op e) operator sectioning
  | EnumFromExp SrcInfo Exp -- [n..]
  | EnumFromToExp SrcInfo Exp Exp -- [n..l]
  | EnumFromThenExp SrcInfo Exp Exp -- [n,m..]
  | EnumFromThenToExp SrcInfo Exp Exp Exp -- [n,m..l]
  deriving (Eq, Show)

data CaseAlt = CaseAlt SrcInfo Pat RHS Where
  deriving (Eq, Show)

data Stmt
  = GenStmt SrcInfo Pat Exp
  | QualStmt SrcInfo Exp
  | LetStmt SrcInfo [Dec]
  deriving (Eq, Show)

-- NOTE: we store both value and string representations for floats and ints
data Lit
  = IntegerLit SrcInfo Integer String
  | FloatLit SrcInfo Double (Integer, Integer) String
  | CharLit SrcInfo Char
  | StrLit SrcInfo String
  deriving (Eq, Show)

data TSyn = TSyn Id [Id] Type -- lhs cons and args -> type
  deriving (Eq, Show)

data Sig = Sig Id AnnType TSchm -- type sig
  deriving (Eq, Show)

data TSchm = TSchm Ctxt Type
  deriving (Eq, Show)

data AnnType
  = Univ -- ::
  | Exist -- :::
  | Reg -- :*:
  deriving (Eq, Show)

data Data = Data Id [Id] [Con] Deriv
  deriving (Eq, Show)

data Deriv = Deriv [Id] -- deriving
  deriving (Eq, Show)

-- qualified data constructor: name, exist. variables, context, args
data Con
  = Con Id [Type]
  | QCon [Id] Cnst Id [Type]
  | RecCon Id [FieldDef] -- record-style
  | RecQCon [Id] Cnst Id [FieldDef]
  deriving (Eq, Show)

data FieldDef = FieldDef Id Type
  deriving (Eq, Show)

data FieldBind = FieldBind SrcInfo Id Exp
  deriving (Eq, Show)

data Rule
  = PropRule Ctxt Cnst
  | SimpRule Ctxt Cnst
  deriving (Eq, Show)

data Ctxt = Ctxt [Pred]
  deriving (Eq, Show)

data Cnst = Cnst [Prim]
  deriving (Eq, Show)

data Pred = Pred SrcInfo Id [Type]
  deriving (Eq, Show)

data Prim
  = PredPrim Pred
  | EqPrim SrcInfo Type Type
  deriving (Eq, Show)

data Type
  = VarType Id
  | ConType Id
  | ArrType SrcInfo Type Type
  | AppType SrcInfo Type Type
  | TupType SrcInfo [Type]
  | ListType SrcInfo Type
  | UnionType SrcInfo [Type]
  | -- | type  {- regexp type -}
    StarType SrcInfo Type
  | -- | type  {- regexp type -}
    OptionType SrcInfo Type -- ? type  {- regexp type -}
  deriving (Eq, Show)

data Pat
  = VarPat Id
  | LitPat Lit
  | AsPat SrcInfo Id Pat
  | ConPat SrcInfo Id [Pat]
  | TupPat SrcInfo [Pat]
  | ListPat SrcInfo [Pat]
  | LabPat Id [FieldPat] -- labeled pattern
  | AnnPat SrcInfo Id Type -- type annotated pattern {- regexp pat -}
  deriving (Eq, Show)

data FieldPat = FieldPat SrcInfo Id Pat
  deriving (Eq, Show)

--------------------------------------------------------------------------------

instance HasSrcInfo a => HasSrcInfo [a] where
  getSrcInfo [] = anonSrcInfo
  getSrcInfo (x : _) = getSrcInfo x

instance HasSrcInfo Dec where
  getSrcInfo (RuleDec s _) = s
  getSrcInfo (DataDec s _) = s
  getSrcInfo (TypeDec s _) = s
  getSrcInfo (AnnDec s _) = s
  getSrcInfo (ExtDec s _) = s
  getSrcInfo (ValDec s _) = s
  getSrcInfo (PatDec s _) = s
  getSrcInfo (OverDec s _) = s
  getSrcInfo (ClassDec s _) = s
  getSrcInfo (InstDec s _) = s
  getSrcInfo (FixDec s _) = s
  getSrcInfo (PrimDec s _) = s

instance HasSrcInfo Def where
  getSrcInfo (Def id _ _ _) = getSrcInfo id

instance HasSrcInfo PatBnd where
  getSrcInfo (PatBnd pat _ _) = getSrcInfo pat

instance HasSrcInfo Pred where
  getSrcInfo (Pred s _ _) = s

instance HasSrcInfo Prim where
  getSrcInfo (PredPrim p) = getSrcInfo p
  getSrcInfo (EqPrim s _ _) = s

instance HasSrcInfo Type where
  getSrcInfo (VarType id) = getSrcInfo id
  getSrcInfo (ConType id) = getSrcInfo id
  getSrcInfo (ArrType s _ _) = s
  getSrcInfo (AppType s _ _) = s
  getSrcInfo (TupType s _) = s
  getSrcInfo (ListType s _) = s

instance HasSrcInfo Pat where
  getSrcInfo (VarPat id) = getSrcInfo id
  getSrcInfo (LitPat lit) = getSrcInfo lit
  getSrcInfo (ConPat s _ _) = s
  getSrcInfo (TupPat s _) = s
  getSrcInfo (ListPat s _) = s
  getSrcInfo (LabPat id _) = getSrcInfo id

instance HasSrcInfo Exp where
  getSrcInfo exp = case exp of
    VarExp id -> getSrcInfo id
    ConExp id -> getSrcInfo id
    LitExp l -> getSrcInfo l
    NegExp s _ -> s
    AbsExp s _ _ -> s
    AppExp s _ _ -> s
    IfxAppExp s _ _ _ -> s
    PIfxAppExp s _ _ _ -> s
    LetExp s _ _ -> s
    CaseExp s _ _ -> s
    DoExp s _ -> s
    IfExp s _ s' _ _ -> s -- WARNING: 2nd SrcInfo not returned!
    AnnExp s _ _ _ -> s
    TupExp s _ -> s
    ListExp s _ -> s
    ListCompExp s _ _ -> s
    SecLeftExp s _ _ -> s
    SecRightExp s _ _ -> s
    EnumFromExp s _ -> s
    EnumFromToExp s _ _ -> s
    EnumFromThenExp s _ _ -> s
    EnumFromThenToExp s _ _ _ -> s
    _ -> anonSrcInfo

instance HasSrcInfo CaseAlt where
  getSrcInfo (CaseAlt s _ _ _) = s

instance HasSrcInfo GdExp where
  getSrcInfo (GdExp s _ _) = s

instance HasSrcInfo Lit where
  getSrcInfo (IntegerLit s _ _) = s
  getSrcInfo (FloatLit s _ _ _) = s
  getSrcInfo (CharLit s _) = s
  getSrcInfo (StrLit s _) = s

instance HasSrcInfo Sig where
  getSrcInfo (Sig id _ _) = getSrcInfo id

--------------------------------------------------------------------------------

emptyCtxt :: Ctxt
emptyCtxt = Ctxt []

emptyCnst :: Cnst
emptyCnst = Cnst []

addCtxt :: Ctxt -> Ctxt -> Ctxt
addCtxt (Ctxt ps1) (Ctxt ps2) = Ctxt (ps1 ++ ps2)

addCnst :: Cnst -> Cnst -> Cnst
addCnst (Cnst cs1) (Cnst cs2) = Cnst (cs1 ++ cs2)

-- adds existential variables to a Con
addExistVarsCon :: [Id] -> Con -> Con
addExistVarsCon vs (Con id ts) = QCon vs emptyCnst id ts
addExistVarsCon vs (QCon vs' cnst id ts) = QCon (vs ++ vs') cnst id ts
addExistVarsCon vs (RecCon id fs) = RecQCon vs emptyCnst id fs
addExistVarsCon vs (RecQCon vs' cnst id fs) = RecQCon (vs ++ vs') cnst id fs

-- adds to the context of the given Con
addCnstCon :: Cnst -> Con -> Con
addCnstCon cnst (Con id ts) = QCon [] cnst id ts
addCnstCon cnst (QCon vs cnst' id ts) = QCon vs (cnst `addCnst` cnst') id ts
addCnstCon cnst (RecCon id fs) = RecQCon [] cnst id fs
addCnstCon cnst (RecQCon vs cnst' id fs) = RecQCon vs (cnst `addCnst` cnst') id fs

-- true for record-style constructors
isRecCon :: Con -> Bool
isRecCon (RecCon _ _) = True
isRecCon (RecQCon _ _ _ _) = True
isRecCon _ = False

-- merges neighbouring Defs with the same name into a single Def.
-- returns all other declarations untouched
mergeDefs :: [Dec] -> [Dec]
mergeDefs ds = merge ds
  where
    merge [] = []
    merge (d : ds)
      | isValDec d =
        let id = valDecIdStr d
            (ds1, ds2) =
              span
                ( isValDec
                    &&& ((== id) . valDecIdStr)
                )
                ds
         in addDefs d (concatMap defs ds1) : merge ds2
      | otherwise = d : merge ds

    isValDec (ValDec _ _) = True
    isValDec _ = False

    valDecIdStr (ValDec _ (Def id _ _ _ : _)) = idStr id
    defs (ValDec _ fs) = fs
    addDefs (ValDec s fs') fs = ValDec s (fs' ++ fs)

dropAnnDecs :: [Dec] -> [Dec]
dropAnnDecs = filter (not . isAnnDec)

isAnnDec :: Dec -> Bool
isAnnDec (AnnDec _ _) = True
isAnnDec _ = False

-- looks for an Ann Dec with matching id
findAnn :: Id -> [Dec] -> Maybe Dec
findAnn id ds = listToMaybe [d | d@(AnnDec s (Sig sid _ _)) <- ds, sid == id]

defsId :: [Def] -> Id
defsId (Def id _ _ _ : _) = id

--------------------------------------------------------------------------------

-- Applies an expression transforming function to each sub-expression in the
-- program in a top-down function. (i.e. transforms an expression, then all
-- children of that new expression.)

appExpProg :: (Exp -> Exp) -> Prog -> Prog
appExpProg f (Prog ms) = Prog (map (appExpModule f) ms)

appExpModule :: (Exp -> Exp) -> Module -> Module
appExpModule f (Module id ims exs ds) =
  Module id ims exs (map (appExpDec f) ds)

appExpDec :: (Exp -> Exp) -> Dec -> Dec
appExpDec f dec = case dec of
  ValDec s ds -> ValDec s (map (appExpDef f) ds)
  PatDec s pb -> PatDec s (appExpPatBnd f pb)
  ClassDec s c -> ClassDec s (appExpClass f c)
  InstDec s i -> InstDec s (appExpInst f i)
  d -> d

appExpClass :: (Exp -> Exp) -> Class -> Class
appExpClass f (Class c p fds w) = Class c p fds (chDecs f w)

appExpInst :: (Exp -> Exp) -> Inst -> Inst
appExpInst f (Inst c p w) = Inst c p (chDecs f w)

appExpPatBnd :: (Exp -> Exp) -> PatBnd -> PatBnd
appExpPatBnd f (PatBnd p rhs w) = PatBnd p (chRHS f rhs) (chDecs f w)

-- apply the given function to the top-level expression in the def
appExpDef :: (Exp -> Exp) -> Def -> Def
appExpDef f def = chDef f def

chDefs :: (Exp -> Exp) -> [Def] -> [Def]
chDefs f ds = map (chDef f) ds

chDef :: (Exp -> Exp) -> Def -> Def
chDef f (Def id ps rhs w) = Def id ps (chRHS f rhs) (chDecs f w)

chRHS :: (Exp -> Exp) -> RHS -> RHS
chRHS f (RHS1 ae) = RHS1 (chExp f ae)
chRHS f (RHSG ges) = RHSG (map (chGdExp f) ges)

chDecs :: (Exp -> Exp) -> [Dec] -> [Dec]
chDecs f = map (chDec f)

chDec :: (Exp -> Exp) -> Dec -> Dec
chDec = appExpDec

chStmt :: (Exp -> Exp) -> Stmt -> Stmt
chStmt f stmt = case stmt of
  GenStmt s p e -> GenStmt s p (chExp f e)
  QualStmt s e -> QualStmt s (chExp f e)
  LetStmt s ds -> LetStmt s (chDecs f ds)

chAlt :: (Exp -> Exp) -> CaseAlt -> CaseAlt
chAlt f (CaseAlt s p e w) = CaseAlt s p (chRHS f e) (chDecs f w)

chGdExp :: (Exp -> Exp) -> GdExp -> GdExp
chGdExp f (GdExp s g e) = GdExp s (chExp f g) (chExp f e)

chExp :: (Exp -> Exp) -> Exp -> Exp
chExp f exp = case f exp of
  NegExp s e -> NegExp s (chx e)
  AbsExp s id e1 -> AbsExp s id (chx e1)
  AppExp s e1 e2 -> AppExp s (chx e1) (chx e2)
  IfxAppExp s e1 e2 e3 -> IfxAppExp s (chx e1) (chx e2) (chx e3)
  PIfxAppExp s e1 e2 e3 -> PIfxAppExp s (chx e1) (chx e2) (chx e3)
  IfExp s1 e1 s2 e2 e3 -> IfExp s1 (chx e1) s2 (chx e2) (chx e3)
  AnnExp s e at ts -> AnnExp s (chx e) at ts
  TupExp s es -> TupExp s (map chx es)
  LetExp s ds e -> LetExp s (chDecs f ds) (chx e)
  DoExp s ss -> DoExp s (map (chStmt f) ss)
  CaseExp s e as -> CaseExp s (chx e) (map (chAlt f) as)
  ListExp s1 (Many s2 es) -> ListExp s1 (Many s2 (map chx es))
  ListCompExp s e ss -> ListCompExp s (chx e) (map (chStmt f) ss)
  SecLeftExp s e1 e2 -> SecLeftExp s (chx e1) (chx e2)
  SecRightExp s e1 e2 -> SecRightExp s (chx e1) (chx e2)
  EnumFromExp s e1 -> EnumFromExp s (chx e1)
  EnumFromToExp s e1 e2 -> EnumFromToExp s (chx e1) (chx e2)
  EnumFromThenExp s e1 e2 -> EnumFromThenExp s (chx e1) (chx e2)
  EnumFromThenToExp s e1 e2 e3 ->
    EnumFromThenToExp
      s
      (chx e1)
      (chx e2)
      (chx e3)
  e -> e -- no subexpressions
  where
    chx = chExp f

instance Pretty Dec where
  pretty (RuleDec _ rule) = pretty rule
  pretty (DataDec _ data_) = pretty data_
  pretty (TypeDec _ tsyn) = pretty tsyn
  pretty (AnnDec _ sig) = pretty sig
  pretty (ExtDec _ extern) = pretty extern
  pretty (ValDec _ defs) = pretty defs
  pretty (PatDec _ patbnd) = pretty patbnd
  pretty (OverDec _ over) = pretty over
  pretty (ClassDec _ class_) = pretty class_
  pretty (InstDec _ inst) = pretty inst
  pretty (FixDec _ fixity) = pretty fixity
  pretty (PrimDec _ primval) = pretty primval
  prettySep _ = "\n"

instance Pretty Rule where
  pretty (PropRule ctxt cnst) = "rule (" ++ (pretty ctxt) ++ ")" ++ "==>" ++ (pretty cnst)
  pretty (SimpRule ctxt cnst) = "rule (" ++ (pretty ctxt) ++ ")" ++ "<==>" ++ (pretty cnst)

instance Pretty Data where
  pretty (Data id ids cons deriv) = "data " ++ (pretty id) ++ (pretty ids) ++ " = " ++ (pretty cons) ++ (pretty deriv)

instance Pretty Con where
  pretty (Con id types) = "(" ++ (pretty id) ++ " " ++ (pretty types) ++ ")"
  pretty (QCon ids cnst id types) = "(Forall " ++ (pretty ids) ++ (pretty cnst) ++ (pretty id) ++ (pretty types) ++ ")"
  pretty (RecCon id fds) = (pretty id) ++ " { " ++ (pretty fds) ++ " } "
  pretty (RecQCon ids cnst id fds) = "(Forall " ++ (pretty ids) ++ (pretty cnst) ++ (pretty id) ++ " { " ++ (pretty fds) ++ ")"
  prettySep _ = " | "

instance Pretty Type where
  pretty (VarType id) = pretty id
  pretty (ConType id) = pretty id
  pretty (ArrType _ ty1 ty2) = "(" ++ (pretty ty1) ++ "->" ++ (pretty ty2) ++ ")"
  pretty (AppType _ ty1 ty2) = "(" ++ (pretty ty1) ++ " " ++ (pretty ty2) ++ ")"
  pretty (TupType _ types) = "(" ++ (pretty types) ++ ",)"
  pretty (ListType _ t) = "[" ++ (pretty t) ++ "]"
  pretty (UnionType _ types) = "(" ++ (pretty types) ++ "|)"
  pretty (StarType _ t) = "(" ++ (pretty t) ++ "*)"
  pretty (OptionType _ t) = "(" ++ (pretty t) ++ "?)"
  prettySep _ = " "

instance Pretty FieldDef where
  pretty (FieldDef id ty) = (pretty id) ++ " :: " ++ (pretty ty)
  prettySep _ = " "

instance Pretty Deriv where
  pretty (Deriv ids) = " deriving (" ++ (pretty ids) ++ ")"

instance Pretty TSyn where
  pretty (TSyn id ids ty) = "type " ++ (pretty id) ++ (pretty ids) ++ " = " ++ (pretty ty)

instance Pretty Sig where
  pretty (Sig id at ts) = (pretty id) ++ (pretty at) ++ (pretty ts)

instance Pretty AnnType where
  pretty Univ = " :: "
  pretty Exist = " ::: "
  pretty Reg = " :*: "

instance Pretty TSchm where
  pretty (TSchm ctxt ty) = " ( " ++ (pretty ctxt) ++ " ) => " ++ (pretty ty)

instance Pretty Extern where
  pretty (VarExt sig) = "extern " ++ (pretty sig)
  pretty (TConExt id) = "extern " ++ (pretty id)

instance Pretty Def where
  pretty (Def id pats rhs wh) = (pretty id) ++ "(" ++ (pretty pats) ++ ")" ++ (pretty rhs) ++ "\n where \n" ++ (pretty wh)

instance Pretty Pat where
  pretty (VarPat id) = (pretty id)
  pretty (LitPat lit) = "Literal"
  pretty (AsPat _ id pat) = "(" ++ (pretty id) ++ "@" ++ (pretty pat) ++ ")"
  pretty (ConPat _ id pats) = "(" ++ (pretty id) ++ " " ++ (pretty pats) ++ ")"
  pretty (TupPat _ pats) = "(" ++ (pretty pats) ++ ",)"
  pretty (ListPat _ pats) = "[" ++ (pretty pats) ++ ",]"
  pretty (LabPat id fpats) = "(" ++ (pretty id) ++ "{" ++ (pretty fpats) ++ "})"
  pretty (AnnPat _ id ty) = "(" ++ (pretty id) ++ " :: " ++ (pretty ty) ++ ")"

instance Pretty FieldPat where
  pretty (FieldPat _ id pat) = (pretty id) ++ " = " ++ (pretty pat)
  prettySep _ = " , "

instance Pretty RHS where
  pretty (RHS1 exp) = " = " ++ (pretty exp)
  pretty (RHSG gexps) = " | " ++ (pretty gexps)

instance Pretty Exp where
  pretty (VarExp id) = pretty id
  pretty (ConExp id) = pretty id
  pretty (LitExp _) = "Literal"
  pretty (NegExp _ exp) = "(-" ++ (pretty exp) ++ ")"
  pretty (AbsExp _ pats exp) = "\\" ++ (pretty pats) ++ "->" ++ (pretty exp)
  pretty (AppExp _ e1 e2) = "(" ++ (pretty e1) ++ " " ++ (pretty e2) ++ ")"
  pretty (IfxAppExp _ e1 e2 e3) = "(" ++ (pretty e1) ++ " `" ++ (pretty e2) ++ "` " ++ (pretty e3) ++ ")"
  pretty (PIfxAppExp _ e1 e2 e3) = "(" ++ (pretty e1) ++ " `" ++ (pretty e2) ++ "` " ++ (pretty e3) ++ ")"
  pretty (LetExp _ decs exp) = "( let" ++ (pretty decs) ++ "\n in" ++ (pretty exp) ++ ")"
  pretty (CaseExp _ exp calts) = "( case " ++ (pretty exp) ++ "of \n" ++ (pretty calts) ++ ")"
  pretty (DoExp _ stmts) = "(do {\n" ++ (pretty stmts) ++ "}"
  pretty (IfExp _ e1 _ e2 e3) = "( if " ++ (pretty e1) ++ " then " ++ (pretty e2) ++ " else " ++ (pretty e3) ++ ")"
  pretty (AnnExp _ e annty tschm) = "(" ++ (pretty e) ++ (pretty annty) ++ (pretty tschm) ++ ")"
  pretty (TupExp _ exps) = "(" ++ (pretty exps) ++ ",)"
  pretty (ListExp _ (Many _ exps)) = "[" ++ (pretty exps) ++ "]"
  pretty (ListCompExp _ exp stmts) = "[" ++ (pretty exp) ++ "|" ++ (pretty stmts) ++ "]"
  pretty (RecExp id fbs) = "(" ++ (idStr id) ++ "{" ++ (pretty fbs) ++ "})"
  pretty (UpdExp exp fbs) = "(" ++ (pretty exp) ++ "{" ++ (pretty fbs) ++ "})"
  pretty (SecLeftExp _ e1 e2) = "(" ++ (pretty e1) ++ " " ++ (pretty e2) ++ ")"
  pretty (SecRightExp _ e1 e2) = "(" ++ (pretty e1) ++ " " ++ (pretty e2) ++ ")"
  pretty (EnumFromExp _ e) = "[" ++ (pretty e) ++ "..]"
  pretty (EnumFromThenExp _ e1 e2) = "[" ++ (pretty e1) ++ "," ++ (pretty e2) ++ "..]"
  pretty (EnumFromThenToExp _ e1 e2 e3) = "[" ++ (pretty e1) ++ "," ++ (pretty e2) ++ ".." ++ (pretty e3) ++ "]"

instance Pretty FieldBind where
  pretty (FieldBind _ id exp) = (idStr id) ++ "=" ++ (pretty exp)
  prettySep _ = " , "

instance Pretty CaseAlt where
  pretty (CaseAlt _ pat rhs wh) = (pretty pat) ++ "->" ++ (pretty rhs) ++ "\n where \n " ++ (pretty wh)
  prettySep _ = " | "

instance Pretty GdExp where
  pretty (GdExp _ guard exp) = (pretty guard) ++ " = " ++ (pretty exp)
  prettySep _ = " | "

instance Pretty Stmt where
  pretty (GenStmt _ pat exp) = (pretty pat) ++ " <- " ++ (pretty exp)
  pretty (QualStmt _ exp) = (pretty exp)
  pretty (LetStmt _ decs) = "let " ++ (pretty decs)
  prettySep _ = " \n "

instance Pretty PatBnd where
  pretty (PatBnd pat rhs wh) = (pretty pat) ++ (pretty rhs) ++ "\n where \n" ++ (pretty wh)

instance Pretty Over where
  pretty (Over0 id) = "overload " ++ (idStr id)
  pretty (OverDef sig defs) = "overload " ++ (pretty sig) ++ (pretty defs)

instance Pretty Class where
  pretty (Class ctxt pred fds wh) = "class (" ++ (pretty ctxt) ++ ") => " ++ (pretty pred) ++ " | " ++ (pretty fds) ++ " where \n " ++ (pretty wh)
  prettySep _ = "\n"

instance Pretty FunDep where
  pretty (FunDep _ ids ids') = (pretty ids) ++ " -> " ++ (pretty ids')
  prettySep _ = ","

instance Pretty Pred where
  pretty (Pred _ id tys) = (idStr id) ++ (pretty tys)
  prettySep _ = ","

instance Pretty Inst where
  pretty (Inst ctxt pred wh) = "instance (" ++ (pretty ctxt) ++ ")  => " ++ (pretty pred) ++ " where \n" ++ (pretty wh)

instance Pretty Fixity where
  pretty (Fix oa op ids) = "infix" ++ (pretty oa) ++ " " ++ (pretty op) ++ " " ++ (pretty ids)

instance Pretty OpAssoc where
  pretty NonAssoc = ""
  pretty LeftAssoc = "l"
  pretty RightAssoc = "r"

instance Pretty OpPrec where
  pretty i = (show i)

instance Pretty PrimVal where
  pretty (Prim id str tschm) = "primitive " ++ (idStr id) ++ " " ++ str ++ " " ++ (pretty tschm)

instance Pretty Ctxt where
  pretty (Ctxt preds) = pretty preds

instance Pretty Cnst where
  pretty (Cnst prims) = pretty prims

instance Pretty Prim where
  pretty (PredPrim pred) = pretty pred
  pretty (EqPrim _ ty1 ty2) = (pretty ty1) ++ " = " ++ (pretty ty2)
  prettySep _ = ","