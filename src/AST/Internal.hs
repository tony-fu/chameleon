--------------------------------------------------------------------------------
--
-- Copyright (C) 2004 The Chameleon Team
--
-- This program is free software; you can redistribute it and/or modify it
-- under the terms of the GNU General Public License as published by the Free
-- Software Foundation; either version 2 of the License, or (at your option)
-- any later version. This program is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU General Public License for more details.
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
--
--------------------------------------------------------------------------------
--
-- This module defines the AST data structure for Chameleon's internal
-- language. See doc/internal_AST.txt for details.
--
-- NOTE: Think about moving some of the common parts of the AST out of here and
--	 Internal, and put them in a separate module that both import.
--
--------------------------------------------------------------------------------

module AST.Internal
  ( module AST.Internal,
    module AST.Common,
  )
where

import AST.Common
import AST.SrcInfo
import Misc
import Misc.Pretty

--------------------------------------------------------------------------------

-- A `Prog' really corresponds to the contents of a single file, even though
-- the entire program may be spread across multiple files.
-- NOTE: Currently we only ever process the first module. Haskell only allows
--	 one module per file anyway.
data Prog = Prog [Module]
  deriving (Eq, Show)

-- a module with a name, export list and local declarations
-- syntax: "module" <module name :: Id> <export_list :: Exports>
--		    <import declarations :: [Import]>
--		    <other declarations :: [Dec]>
data Module = Module Id Exports [Import] [Dec]
  deriving (Eq, Show)

----------------------------------------

-- all non-import declarations
-- syntax: see below for specific cases
data Dec
  = ValDec SrcInfo LetBnd
  | DataDec SrcInfo DataType [Cons]
  | RuleDec SrcInfo Rule
  | ClassDec SrcInfo Class
  | InstDec SrcInfo Inst
  | PrimDec SrcInfo PrimVal
  deriving (Eq, Show)

-- a nested block of declarations (usually under a `where')
type Where = [Dec]

-- a primitive identifier (with explicit external-name string)
-- syntax: "primitive" <primitive id :: Id>
--		       <external name :: String> <type :: TSchm>
data PrimVal = Prim Id String TSchm
  deriving (Eq, Show)

-- class declaration
-- syntax: "class" <context/super classes :: Ctxt> <class predicate :: Pred> "|"
--		   <functional dependencies :: [FunDep]> "where"
--		   <class methods :: [Method]
data Class = Class Ctxt Pred [FunDep] [Method]
  deriving (Eq, Show)

-- class method
-- syntax: <method name :: Id> "::" <type scheme :: TSchm>
data Method = Method Id TSchm
  deriving (Eq, Show)

-- functional dependency
-- syntax: <domain variables :: [Id]> "->" <range variables :: [Id]>
data FunDep = FunDep SrcInfo [Id] [Id]
  deriving (Eq, Show)

-- instance
-- syntax: "instance" <context :: Ctxt> <instance predicate :: Pred> "where"
--		      <instance method declarations :: Where>
data Inst = Inst Ctxt Pred Where
  deriving (Eq, Show)

-- data type
-- syntax: <constructor name :: Id> <type arguments :: [Type]>
data DataType = DataType Id [Type]
  deriving (Eq, Show)

-- data constructor
-- syntax: "forall" <existential vars :: [Id]> "." <context :: Cnst> "=>"
--		    <constructor :: DataType>
data Cons = Cons SrcInfo [Id] Cnst DataType
  deriving (Eq, Show)

-- CHR rules
-- syntax: <head :: Ctxt> "<=>" <body :: Cnst>
--	 | <head :: Ctxt> "==>" <body :: Cnst>
data Rule
  = SimpRule Ctxt Cnst
  | PropRule Ctxt Cnst
  deriving (Eq, Show)

-- context; a list of predicates
-- syntax: <predicate :: Pred>
--	 | "(" <predicate :: Pred> "," ... "," ... <predicate :: Pred> ")"
data Ctxt = Ctxt [Pred]
  deriving (Eq, Show)

-- constraint; a list of primitives
data Cnst = Cnst [Prim]
  deriving (Eq, Show)

-- predicate
-- syntax: <predicate symbol :: Id> <type arguments :: [Type]>
data Pred = Pred SrcInfo Id [Type]
  deriving (Eq, Show)

-- primitive; either a predicate or an equation
-- syntax: <predicate :: Pred>
--	 | <left type :: Type> "=" <right type :: Type>
data Prim
  = PredPrim Pred
  | EqPrim SrcInfo Type Type
  deriving (Eq, Show)

-- expressions
-- syntax: <variable :: Id>
--	 | <constructor :: Id>
--	 | <literal :: Lit>
--	 | <function :: Exp> <argument :: Exp>
--	 | "\" <pattern variable :: Id> "->" <result :: Exp>
--	 | "let" <let binding group :: [LetBnd]> "in" <result :: Exp>
--	 | "case" <scrutinees :: [Exp]> <alternatives :: [Match]>
data Exp
  = VarExp Id
  | ConExp Id -- do we need these?!
  | LitExp Lit
  | AppExp SrcInfo Exp Exp
  | AbsExp SrcInfo Id Exp
  | LetExp SrcInfo [LetBnd] Exp
  | CaseExp SrcInfo [Exp] [Match]
  deriving (Eq, Show)

-- case alternative/match
-- syntax: <patterns :: [Pat]> "->" <result :: Exp>
data Match = Match [Pat] Exp
  deriving (Eq, Show)

-- patterns
-- syntax: <variable :: Pat>
--	 | <literal :: Pat>
--	 | <constructor name :: Id> <constructor args :: [Pat]>
data Pat
  = VarPat Id
  | LitPat Lit
  | ConPat SrcInfo Id [Pat]
  deriving (Eq, Show)

-- literals
--    note: All integer literals from the source program become IntegerLits.
--	    IntLits are generate internally only
--	    FloatLits contain a rational representation of the Float.
-- syntax: <integer :: Integer>
--	 | <int :: Int>
--	 | <floating point :: Double>
--	 | <character string :: String>
--	 | <character :: Char>
data Lit
  = IntegerLit SrcInfo Integer String
  | IntLit SrcInfo Int
  | FloatLit SrcInfo Double (Integer, Integer) String
  | StrLit SrcInfo String
  | CharLit SrcInfo Char
  deriving (Eq, Show)

-- let bindings
-- syntax: "let" <variable name :: Id> "=" <body :: Exp>
--	 | "let" <variable name :: Id> <annotation type :: AnnType>
--		 <typescheme :: TSchm> "=" <body :: Exp>
data LetBnd
  = LetBnd SrcInfo Id Exp
  | LetBndAnn SrcInfo Id AnnType TSchm Exp
  deriving (Eq, Show)

-- annotation type
-- syntax: "::"
--	 | ":::"
data AnnType
  = Univ
  | Exist
  deriving (Eq, Show)

-- type scheme
-- syntax: <type context :: Ctxt> "=>" <type :: Type>
data TSchm = TSchm Ctxt Type
  deriving (Eq, Show)

-- types
-- syntax: <type variable :: Id>
--	 | <type constructor :: Id>
--	 | <type :: Type> <type :: Type>
data Type
  = VarType Id
  | ConType Id
  | AppType SrcInfo Type Type
  deriving (Eq, Show)

--------------------------------------------------------------------------------

-- Miscellaneous helper functions, instances etc.

emptyCtxt = Ctxt []

emptyCnst = Cnst []

letBndUAnn = \s id ts e -> LetBndAnn s id Univ ts e

letBndEAnn = \s id ts e -> LetBndAnn s id Exist ts e

-- True if the given let-binding is annotated
isAnnLetBnd :: LetBnd -> Bool
isAnnLetBnd (LetBnd {}) = False
isAnnLetBnd (LetBndAnn {}) = True

isValDec d = case d of ValDec {} -> True; _ -> False

moduleDecs (Module _ _ _ ds) = ds

addToAllModules :: [Dec] -> Prog -> Prog
addToAllModules ds1 (Prog ms) = Prog (map add ms)
  where
    add :: Module -> Module
    add (Module id exps ims ds2) = Module id exps ims (ds1 ++ ds2)

----------------------------------------

instance HasSrcInfo Dec where
  getSrcInfo (ValDec s _) = s
  getSrcInfo (DataDec s _ _) = s
  getSrcInfo (RuleDec s _) = s
  getSrcInfo (InstDec s _) = s
  getSrcInfo (ClassDec s _) = s

  newSrcInfo s (ValDec _ lb) = ValDec s lb
  newSrcInfo s (DataDec _ d cs) = DataDec s d cs
  newSrcInfo s (RuleDec _ r) = RuleDec s r
  newSrcInfo s (InstDec _ i) = InstDec s i
  newSrcInfo s (ClassDec _ c) = ClassDec s c

instance HasSrcInfo LetBnd where
  getSrcInfo (LetBnd s _ _) = s
  getSrcInfo (LetBndAnn s _ _ _ _) = s

  newSrcInfo s (LetBnd _ id exp) = LetBnd s id exp
  newSrcInfo s (LetBndAnn _ id at ts exp) = LetBndAnn s id at ts exp

instance HasSrcInfo Lit where
  getSrcInfo (IntegerLit s _ _) = s
  getSrcInfo (IntLit s _) = s
  getSrcInfo (FloatLit s _ _ _) = s
  getSrcInfo (StrLit s _) = s
  getSrcInfo (CharLit s _) = s

  newSrcInfo s (IntegerLit _ i str) = IntegerLit s i str
  newSrcInfo s (IntLit _ i) = IntLit s i
  newSrcInfo s (FloatLit _ f r str) = FloatLit s f r str
  newSrcInfo s (StrLit _ str) = StrLit s str
  newSrcInfo s (CharLit _ c) = CharLit s c

instance HasSrcInfo Exp where
  getSrcInfo (VarExp id) = getSrcInfo id
  getSrcInfo (ConExp id) = getSrcInfo id
  getSrcInfo (LitExp lit) = getSrcInfo lit
  getSrcInfo (AppExp s _ _) = s
  getSrcInfo (AbsExp s _ _) = s
  getSrcInfo (LetExp s _ _) = s
  getSrcInfo (CaseExp s _ _) = s

  newSrcInfo s (VarExp id) = VarExp (newSrcInfo s id)
  newSrcInfo s (ConExp id) = ConExp (newSrcInfo s id)
  newSrcInfo s (LitExp lit) = LitExp (newSrcInfo s lit)
  newSrcInfo s (AppExp _ e1 e2) = AppExp s e1 e2
  newSrcInfo s (AbsExp _ id e) = AbsExp s id e
  newSrcInfo s (LetExp _ lb e) = LetExp s lb e
  newSrcInfo s (CaseExp _ e m) = CaseExp s e m

instance HasSrcInfo Pat where
  getSrcInfo (VarPat id) = getSrcInfo id
  getSrcInfo (LitPat l) = getSrcInfo l
  getSrcInfo (ConPat s _ _) = s

  newSrcInfo s (VarPat id) = VarPat (newSrcInfo s id)
  newSrcInfo s (LitPat l) = LitPat (newSrcInfo s l)
  newSrcInfo s (ConPat _ id ps) = ConPat s id ps

instance HasSrcInfo Type where
  getSrcInfo (VarType id) = getSrcInfo id
  getSrcInfo (ConType id) = getSrcInfo id
  getSrcInfo (AppType s _ _) = s

  newSrcInfo s (VarType id) = VarType (newSrcInfo s id)
  newSrcInfo s (ConType id) = ConType (newSrcInfo s id)
  newSrcInfo s (AppType _ t1 t2) = AppType s t1 t2

instance HasSrcInfo Pred where
  getSrcInfo (Pred s _ _) = s
  newSrcInfo s (Pred _ id t) = Pred s id t

instance HasSrcInfo Prim where
  getSrcInfo (PredPrim p) = getSrcInfo p
  getSrcInfo (EqPrim s _ _) = s

  newSrcInfo s (PredPrim p) = PredPrim (newSrcInfo s p)
  newSrcInfo s (EqPrim _ t1 t2) = EqPrim s t1 t2

--------------------------------------------
-- Pretty Print Instances For Internal AST

instance Pretty Type where
  pretty (VarType id) = idStr id
  pretty (ConType id) = idStr id
  pretty (AppType _ t1 t2) = "(" ++ (pretty t1) ++ " " ++ (pretty t2) ++ ")"
  prettySep _ = " "

instance Pretty DataType where
  pretty (DataType id xs) = "(" ++ (idStr id) ++ " " ++ (pretty xs) ++ ")"

instance Pretty Pred where
  pretty (Pred _ id ts) = "(" ++ (idStr id) ++ " " ++ (pretty ts) ++ ")"

instance Pretty Prim where
  pretty (PredPrim p) = pretty p
  pretty (EqPrim _ t1 t2) = "(" ++ (pretty t1) ++ " = " ++ (pretty t2) ++ ")"

instance Pretty Ctxt where
  pretty (Ctxt []) = ""
  pretty (Ctxt ps) = (pretty ps) ++ " => "

instance Pretty Cnst where
  pretty (Cnst []) = ""
  pretty (Cnst ps) = (pretty ps) ++ " => "

instance Pretty Cons where
  pretty (Cons _ [] c d) = "(" ++ (pretty c) ++ (pretty d) ++ ")"
  pretty (Cons _ ids c d) = "(Forall " ++ (pretty ids) ++ " . " ++ (pretty c) ++ (pretty d) ++ ")"
  prettySep _ = " | "

instance Pretty TSchm where
  pretty (TSchm c t) = (pretty c) ++ (pretty t)
  prettySep _ = " "

instance Pretty Method where
  pretty (Method id ts) = (idStr id) ++ " :: " ++ (pretty ts)
  prettySep _ = "\n"

instance Pretty Class where
  pretty (Class c p _ m) = (pretty c) ++ (pretty p) ++ " where" ++ "\n" ++ (pretty m)
  prettySep _ = "\n"

instance Pretty Inst where
  pretty (Inst c p w) = (pretty c) ++ (pretty p) ++ " where" ++ "\n" ++ (pretty w)
  prettySep _ = "\n"

instance Pretty Dec where
  pretty (DataDec _ d c) = "dataType " ++ (pretty d) ++ " = " ++ (pretty c)
  pretty (ClassDec _ c) = "class " ++ (pretty c)
  pretty (ValDec _ l) = (pretty l)
  pretty (InstDec _ i) = "instance " ++ (pretty i)
  pretty (RuleDec _ r) = "rule " ++ (pretty r)
  pretty (PrimDec _ p) = (pretty p)
  prettySep _ = "\n"

instance Pretty PrimVal where
  pretty (Prim id ex tschm) = "primitive " ++ (idStr id) ++ " " ++ ex ++ " " ++ (pretty tschm)

instance Pretty LetBnd where
  pretty (LetBnd _ id exp) = (idStr id) ++ " = " ++ (pretty exp)
  pretty (LetBndAnn _ id ann schm exp) =
    (idStr id) ++ " " ++ (pretty ann)
      ++ " "
      ++ (pretty schm)
      ++ " = "
      ++ (pretty exp)
  prettySep _ = "\n"

instance Pretty AnnType where
  pretty (Univ) = "::"
  pretty (Exist) = ":::"

instance Pretty Exp where
  pretty (VarExp id) = (idStr id)
  pretty (ConExp id) = (idStr id)
  pretty (LitExp lit) = "Literal"
  pretty (AppExp _ e1 e2) = (pretty e1) ++ " " ++ (pretty e2)
  pretty (AbsExp _ id e) = "/" ++ (idStr id) ++ "->" ++ (pretty e)
  pretty (LetExp _ ls e) = "let " ++ (pretty ls) ++ "\nin " ++ (pretty e)
  pretty (CaseExp _ es m) = "(Case " ++ (pretty es) ++ " of " ++ (pretty m) ++ ")"
  prettySep _ = " "

instance Pretty Match where
  pretty (Match ps e) = (pretty ps) ++ "->" ++ (pretty e)

instance Pretty Pat where
  pretty (VarPat id) = (idStr id)
  pretty (LitPat lit) = "Literal"
  pretty (ConPat _ id ps) = (idStr id) ++ " " ++ (pretty ps)

instance Pretty Rule where
  pretty (SimpRule h t) = "(" ++ (pretty h) ++ " <==> " ++ (pretty t) ++ ")"
  pretty (PropRule h t) = "(" ++ (pretty h) ++ " ==> " ++ (pretty t) ++ ")"
