--------------------------------------------------------------------------------
--
-- Copyright (C) 2004 The Chameleon Team
--
-- This program is free software; you can redistribute it and/or modify it
-- under the terms of the GNU General Public License as published by the Free
-- Software Foundation; either version 2 of the License, or (at your option)
-- any later version. This program is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU General Public License for more details.
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
--
--------------------------------------------------------------------------------
--
-- Operations on (Herbrand) constraints.
--
-- Includes:
--  - sat test
--  - finding a min. unsat. subset
--  - finding a min. grounding subset
--  - finding a min. instantiating subset
--
--------------------------------------------------------------------------------

module Core.ConsOps
  ( sat,
    unsat,
    minUnsatSubset,
    maxSatSubset,
    minGroundingSubset,
    minGroundingSubsetTotal,
    minInstantiatingSubset,
    findInstantiated,
    dropSkol,
  )
where

import Control.Monad
import Core.BuiltIn
import Core.Constraint
import Core.Justify
import Core.Name
import Core.Types
import Data.List
import Misc
import Solvers.Convert
import Solvers.HerbrandNew
import Debug.Trace

--------------------------------------------------------------------------------

-- Returns True if the constraints are satisfiable
sat :: [BCons] -> IO Bool
sat bcs = do
  st <- newStore
  ok <-
    runHerbrand
      st
      ( do
          (_, tjs) <- thread intBCons emptyVarMap bcs
          let ts = map fst tjs
          solveAll ts
      )
  deleteStore st
  return ok

unsat :: [BCons] -> IO Bool
unsat bcs = do
  ok <- sat bcs
  return (not ok)

--------------------------------------------------------------------------------

minUnsatSubsetTotal :: [BCons] -> IO (Either String [BCons])
minUnsatSubsetTotal bcs = do
  st <- newStore
  min <-
    runHerbrand
      st
      ( do
          -- first internalise all the BCons
          (_, tjs) <- thread intBCons emptyVarMap bcs
          let ts = map fst tjs
              cs = zip bcs ts
          accum cs []
      )
  deleteStore st
  return min
  where
    -- The hs are constraints definitely in the min. unsat. subset (they are
    -- present in the store) - cs need further processing.
    accum :: Herb s c => [(BCons, c)] -> [BCons] -> Herbrand s (Either String [BCons])
    accum cs hs = do
      cp <- createCP
      findRes <- find cs []
      rewind cp
      case findRes of
        (Right ((h, d), cs')) -> do 
          -- re-add d to the store, if it becomes unsat., we've found the min.
          -- unsat. subset (d:ds)
          ok <- solveEq d
          if ok -- not unsatisfiable yet
            then accum cs' (h : hs)
            else -- it's unsatisfiable, we're done
              return $ Right (h : hs)
        (Left er) -> return $ Left er
      where
        -- Finds the next constraint to add to the min. unsat. subset.
        -- Go through cs until the store becomes unsat.
        -- Returns both the constraint definitely in the min. unsat. subset, as
        -- well as the rest of the constraints to consider.
        find ::
          Herb s c =>
          [(BCons, c)] ->
          [(BCons, c)] ->
          Herbrand s (Either String ((BCons, c), [(BCons, c)]))
        find [] _ = return $ Left "minUnsatSubset: constraint not unsat!"
        find (hc@(_, c) : cs) as = do
          ok <- solveEq c
          if ok
            then -- not unsatisfiable yet
              find cs (hc : as)
            else -- unsatisfiable with the addition of this constraint
              return $ Right (hc, as)

-- Given an unsatisfiable set of constraints, finds one minimal unsatisfiable
-- subset.
minUnsatSubset :: [BCons] -> IO [BCons]
minUnsatSubset bcs = do
  st <- newStore
  min <-
    runHerbrand
      st
      ( do
          -- first internalise all the BCons
          (_, tjs) <- thread intBCons emptyVarMap bcs
          let ts = map fst tjs
              cs = zip bcs ts
          accum cs []
      )
  deleteStore st
  return min
  where
    -- The hs are constraints definitely in the min. unsat. subset (they are
    -- present in the store) - cs need further processing.
    accum :: Herb s c => [(BCons, c)] -> [BCons] -> Herbrand s [BCons]
    accum cs hs = do
      cp <- createCP
      ((h, d), cs') <- find cs []
      rewind cp
      -- re-add d to the store, if it becomes unsat., we've found the min.
      -- unsat. subset (d:ds)
      ok <- solveEq d
      if ok -- not unsatisfiable yet
        then accum cs' (h : hs)
        else -- it's unsatisfiable, we're done
          return (h : hs)
      where
        -- Finds the next constraint to add to the min. unsat. subset.
        -- Go through cs until the store becomes unsat.
        -- Returns both the constraint definitely in the min. unsat. subset, as
        -- well as the rest of the constraints to consider.
        find ::
          Herb s c =>
          [(BCons, c)] ->
          [(BCons, c)] ->
          Herbrand s ((BCons, c), [(BCons, c)])
        find [] _ = bug "minUnsatSubset: constraint not unsat!"
        find (hc@(_, c) : cs) as = do
          ok <- solveEq c
          if ok
            then -- not unsatisfiable yet
              find cs (hc : as)
            else -- unsatisfiable with the addition of this constraint
              return (hc, as)

-- Solves all given equations, as represented by terms, returns True if
-- successful, else False (note: no binding is undone)
solveAll :: Herb s c => [c] -> Herbrand s Bool
solveAll [] = return True
solveAll (t : ts) = do
  ok <- solveEq t
  if not ok
    then return False
    else solveAll ts

-- Unifies left and right sides of given equation, returns True if
-- successful, else False (note: no binding is undone)
solveEq :: Herb s c => c -> Herbrand s Bool
solveEq tm = do
  t1 <- arg tm 1
  t2 <- arg tm 2
  stat <- unify t1 t2
  return (not (failed stat))

----------------------------------------

-- finds a maximal satisfiable subset of the given constraints
maxSatSubset :: [BCons] -> IO [BCons]
maxSatSubset bcs = do
  st <- newStore
  max <-
    runHerbrand
      st
      ( do
          -- first internalise all the BCons
          (_, tjs) <- thread intBCons emptyVarMap bcs
          let ts = map fst tjs
          bs <- check ts
          return [bc | (ok, bc) <- zip bs bcs, ok]
      )
  deleteStore st
  return max
  where
    -- To Make sure one constraint is always in the maxSatSubset, make sure it is at the start of the list, right?
    check [] = return []
    check (t : ts) = do
      cp <- createCP
      ok <- solveEq t
      rewind cp
      if ok
        then do
          solveEq t
          oks <- check ts
          return (ok : oks)
        else do
          oks <- check ts
          return (ok : oks)

----------------------------------------

-- Finds a minimal subset of equations in bcs which grounds a variable v.
-- This works by first skolemising v, then finding a min. unsat. subset.
minGroundingSubset :: Var -> [BCons] -> IO [BCons]
minGroundingSubset v bcs = do
  let sk_bc = (TVar v =+= TCon (mkFreeName "Sk!")) skolConsJust
  min <- minUnsatSubset (sk_bc : bcs)
  return $ trace (pretty sk_bc) (dropSkol min)

minGroundingSubsetTotal :: Var -> [BCons] -> IO (Either String [BCons])
minGroundingSubsetTotal v bcs = do
  let sk_bc = (TVar v =+= TCon (mkFreeName "Sk!")) skolConsJust
  min <- minUnsatSubsetTotal (sk_bc : bcs)
  return $ fmap dropSkol min

----------------------------------------

-- Finds a minimal subset of equations in bcs which instantiates variable v
-- by either one of the other vs or by some constant.
-- This works by first skolemising vs, then finding a min. unsat. subset.
minInstantiatingSubset :: Var -> [Var] -> [BCons] -> IO [BCons]
minInstantiatingSubset v vs bcs = try sk_vs
  where
    -- skolemises v
    sk_v = (TVar v =+= TCon (mkFreeName "Sk!")) skolConsJust

    -- skolemises vs
    sk_vs = map (\v -> (TVar v =+= TCon (mkFreeName "Sk!!")) skolConsJust) vs

    try :: [BCons] -> IO [BCons]
    -- if v isn't instantiated by any of vs, it must already be affected by bcs
    try [] = do
      min <- minUnsatSubset (sk_v : bcs)
      return (dropSkol min)
    try (bc : bcs) = do
      mres <- try1 bc
      case mres of
        Nothing -> try bcs
        Just res -> return res

    try1 :: BCons -> IO (Maybe [BCons])
    try1 bc = do
      let bcs' = bc : sk_v : bcs
      ok <- sat bcs'
      if ok
        then return Nothing
        else do
          min <- minUnsatSubset bcs'
          return (Just (dropSkol min))

--------------------------------------------------------------------------------

-- Finds out which of the variables is instantiated by the given set of
-- constraints (which must be satisfiable.)
findInstantiated :: [BCons] -> [Var] -> IO Var
findInstantiated bcs vs = do
  st <- newStore
  -- first solve all bcs
  (vm, ok) <-
    runHerbrand
      st
      ( do
          (vm, tjs) <- thread intBCons emptyVarMap bcs
          let ts = map fst tjs
          ok <- solveAll ts
          return (vm, ok)
      )
  unless ok (bug "findInstantiated: bcs not sat.!")
  v <- runHerbrand st (try vm vs)
  deleteStore st
  return v
  where
    skol v = (TVar v =+= TCon (mkFreeName "Sk!")) skolConsJust

    try vm [] = bug "findInstantiated: no variable instantiated!"
    try vm (v : vs) = do
      cp <- createCP
      (_, tj) <- intBCons vm (skol v)
      ok <- solveEq (fst tj)
      rewind cp
      if ok
        then try vm vs -- not instantiated
        else return v -- instantiated

--------------------------------------------------------------------------------

-- The first variable escapes through the second.
-- Find the minimal subset of constraints which allows this to happen.
-- (we find a minimal bcs' subset bcs, such that: v1 in fv(mgu(bcs') v2)
minEscapeSubset :: Var -> Var -> [BCons] -> IO [BCons]
minEscapeSubset v1 v2 bcs = do
  -- solve bcs, find fv v2, gen skol constraints, then find min. unsat subset
  st <- newStore
  vs <-
    runHerbrand
      st
      ( do
          (vm, tjs) <- thread intBCons emptyVarMap bcs
          let ts = map fst tjs
              tm2 = case lookupVarMap v2 vm of
                Nothing -> bug "minEscapeSubset: VarMap must contain v2"
                Just x -> x
          mapM_ solveEq ts
          t2 <- extOrigType tm2
          let vs = map (originalVar vm) (fv t2)
          doIO (putStrLn ("t2: " ++ pretty t2))
          doIO (putStrLn ("vs: " ++ pretty vs))
          return vs
      )
  deleteStore st

  let sk_bc = mkSk2 v1
      sk_bcs = map mkSk1 vs
      bcs' = sk_bc : sk_bcs ++ bcs

  s <- sat bcs'

  putStrLn ("sk_bc : " ++ pretty sk_bc)
  putStrLn ("sk_bcs: " ++ pretty sk_bcs)
  putStrLn ("bcs' sat? " ++ show s)

  min <- minUnsatSubset bcs
  return (dropSkol min)
  where
    mkSk1 x = (TVar x =+= TCon (mkFreeName "Sk!")) skolConsJust
    mkSk2 x = (TVar x =+= TCon (mkFreeName "Sk!!")) skolConsJust

--------------------------------------------------------------------------------

-- remove skolemisation bcs
dropSkol :: [BCons] -> [BCons]
dropSkol = filter ((/= skolConsJust) . getJust)

--------------------------------------------------------------------------------

test m =
  {-
      let a = TVar (mkVar "a")
          b = TVar (mkVar "b")
          c = TVar (mkVar "c")
          d = TVar (mkVar "d")
          e = TVar (mkVar "e")
          f = TVar (mkVar "f")

          eq0 = a =:= boolType
          eq1 = a =:= b
          eq2 = b =:= c
          eq3 = c =:= charType
          eq4 = b =:= d
          eq5 = d =:= e
          eqs = [eq1,eq2,eq3,eq4,eq0]
  -}

  let eqs =
        [TVar (mkVar "V1") =:= boolType]
          ++ [TVar (mkVar ('V' : show (m + 1))) =:= charType]
          ++ [ t1 =:= t2 | n <- [1 .. m], let t1 = TVar (mkVar ('V' : show n))
                                              t2 = TVar (mkVar ('V' : show (n + 1)))
             ]
   in eqs

--------------------------------------------------------------------------------

{-
solve :: [BCons] -> IO ()
solve bcs = do
    st <- newStore
    ok <- runHerbrand st (do
        (vm, tjs) <- thread intBCons emptyVarMap bcs
        doIO $ putStrLn ("vm: " ++ pretty (sort (varMapToList vm)))
        let ts = map fst tjs
        doIO $ putStrLn "\nBEFORE"
        mapM_ printTerm ts
        solveAll ts
        doIO $ putStrLn "\n\nAFTER"
        mapM_ printTerm ts )
    deleteStore st
  where
    solveAll [] = return True
    solveAll (t:ts) = do
        ok <- solveEq t
        if not ok then return False
                  else solveAll ts
-}
