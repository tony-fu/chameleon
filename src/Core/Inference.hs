--------------------------------------------------------------------------------
--
-- Copyright (C) 2005 The Chameleon Team
--
-- This program is free software; you can redistribute it and/or modify it
-- under the terms of the GNU General Public License as published by the Free
-- Software Foundation; either version 2 of the License, or (at your option)
-- any later version. This program is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU General Public License for more details.
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
--
--------------------------------------------------------------------------------
-- Implementor: J. Wazny
--  Maintainer: Tony Fu
--      Status: bugs (see below)
--------------------------------------------------------------------------------
--
-- Invokes the CHR solver, via the interface in Solvers.Interface, in order to
-- solve inference goals (InfGoal).
-- This module contains the `extended' solver, for solving implication
-- constraints, which is defined in terms of the standard CHR solver.
--
-- NOTE: Ideally the Term and Store types should be factored out, as in
--         HsHerbie, but currently we only allow the Herbie C terms and store.
--
--------------------------------------------------------------------------------
--
-- BUGS:
--  - We're letting out constraints from top-level subsumption goals. Obviously
--  such constraints are `unmatched' and should be reported as such.
--  (The only constraints which escape in this way are all ground, e.g. UC Int)
--  Probably the easiest fix is to pass the `nested' flag down to the
--  implication solver. If it's not nested, then the current implication must
--  be a solved form, no partial solution is allowed.
--
--------------------------------------------------------------------------------
--
-- NOTE: Each CHR derivation is run in stages
--
--------------------------------------------------------------------------------

module Core.Inference
  ( runInfGoals,
  )
where

import Control.Monad
--NOTE: changed in GHC 6.4
--NOTE: deprecated in GHC 6.4 (use Data.Map)

import Core.CHR
import Core.Constraint
import Core.InfGoal
import Core.InfResult
import Core.Justify
import Core.Name
import Core.Types hiding (rename)
import Data.List
import qualified Data.Map.Strict as Map
import Data.Maybe
import qualified Data.Set as Set
import Foreign.C
import Misc
import Solvers.CHR
import Solvers.CHRState hiding (BCons)
import qualified Solvers.CHRState
import Solvers.Convert hiding (skolemise)
import Solvers.HerbrandNew hiding (skolemise)
import System.IO
import Debug.Trace
--------------------------------------------------------------------------------

type JTTerm = JTerm Term

-- attempting to solve an implication results in one of the following:
--  - we find we can't solve it
--  - we find it's a solved form (wrt the current store), and can remove it
--  - we can partially solve it, adding new constraints (and already did so,
--    for the Herbrand constraints)
data ImpStatus
  = CantSolve
  | CanRemove
  | UpdatedStore (UStore Term)

--------------------------------------------------------------------------------

cPut :: String -> Solvers.CHRState.CHR Store Term ()
cPut = const (return ())

-- doHerbrand . doIO . putStrLn

cPuts :: String -> Solvers.CHRState.CHR Store Term ()
cPuts = const (return ())

-- doHerbrand . doIO . putStrLn

hPuts :: String -> Herbrand Store ()
hPuts =
  -- doIO . putStrLn
  const (return ())

hPuts2 :: String -> Herbrand Store ()
hPuts2 = doIO . putStrLn

printTerm_ = const (return ())

-- printTerm

--------------------------------------------------------------------------------

-- Runs the given CHR rules on each inference goal, resulting in a list of
-- list of inference results.
-- This works by: a) creating a new store, b) compiling all the CHRs in
-- that store, c) solving all goals in that same store, rewinding after each.
runInfGoals :: [Core.CHR.CHR] -> [InfGoal] -> IO [InfResult]
runInfGoals chrs0 igs = do
  let chrs = propsFirst chrs0
  -- create a new store to do all this in
  st <- newStore
  -- and a new VarMap (maps Haskell-side variables to Herbie terms)
  irs <-
    runHerbrand
      st
      ( runCHRInit
          ( do
              -- we split the CHRs into two groups:
              --  - the HMRules (H/M inference rules)
              --  - the rest
              -- All CHR derivations will be staged so that the H/M rules are
              -- always applied first.
              let (hm_chrs, other_chrs) = partition isHMRule chrs

              ichrss <-
                mapM
                  (mapM (doHerbrand . intCHR emptyVarMap))
                  [hm_chrs, other_chrs]
              progs <- doHerbrand $ mapM compileCHRsNoReorder ichrss

              -- Solve all the goals.
              solveAll progs igs
          )
      )

  -- delete the store - we're done with it
  deleteStore st
  return irs
  where
    -- We keep track of the id's of goals which have failed. If one goal has
    -- failed, we don't run any subsequent goals for that id.
    solveAll ::
      Progs Term ->
      [InfGoal] ->
      Solvers.CHRState.CHR Store Term [InfResult]
    solveAll progs igs = thread Set.empty igs []
      where
        thread _ [] rs = return rs
        thread fs (ig : igs) rs
          | infId ig `Set.member` fs = thread fs igs rs
          | otherwise = do
            r <- solve emptyVarMap progs ig
            -- if it was a failure, add the id to the fail set
            let fs'
                  | isFailure r = Set.insert (infId ig) fs
                  | otherwise = fs
            thread fs' igs (r : rs)

    -- The procedure for solving a single goal.
    solve ::
      VarMap ->
      Progs Term ->
      InfGoal ->
      Solvers.CHRState.CHR Store Term InfResult
    solve vm0 progs ig = do
      cPuts "solving an inference goal"
      -- create a choice point
      cp <- doHerbrand createCP
      -- internalise the goal constraints
      (vm1, itjs) <- doHerbrand $ intConstraint vm0 (infCons ig)

      -- look up the term corresponding to the t variable
      let t0 =
            let (t : _) = infTLV ig
             in case lookupVarMap t vm1 of
                  Nothing -> bug "goal has no t component!"
                  Just x -> x
      -- solve, and then extract the result from the final state
      state <- solver t0 progs itjs
      (ucs0, ics) <- doHerbrand $ getCons state

      -- get rid of the C0Vars! user constraints
      let ucs = filter ((/= "C0Vars!") . nameStr . ucName) ucs0
      -- look up the value of each of the original vars., and
      -- generate a corresponding equation.
      vts <- doHerbrand $ lookupVars vm1
      let id = infId ig
          tlv = infTLV ig
          ttlv = map TVar tlv
          tlv' = [t | v <- tlv, (v', t) <- vts, v == v']
          bcs = map mkEq vts
          introVar = lookupVarMap (head tlv) vm1
          ttlv' = if isJust introVar then (TVar . mkVar . show . fromJust $ introVar) : tail ttlv else ttlv
          cons = ics /\ ucs /\ bcs  
          cons' = cons /\ getBStore state
          res = case state of
            Failed {} -> InfFailure id ttlv' cons'
            FailedBS bcs ->  InfFailure id ttlv' cons'
            FailedUniv _ v vs ->
              let v' = originalVar vm1 v
               in InfFailureUniv id v' vs cons'
            FailedUnivEsc _ t vs ->
              let vs' = map (originalVar vm1) vs
               in InfFailureUnivEsc id ttlv t vs' cons'
            FailedUCUnmatched uc -> InfFailureUConsUnmatched id uc
            State {}
              | isSubGoal ig -> SubSuccess id
              | otherwise -> InfSuccess id tlv' cons

      -- rewind to the choice point
      doHerbrand $ rewind cp
      return res

--------------------------------------------------------------------------------

-- `solver' is the top-level CHR solver, `solverNested' should be used when
-- invoking the solver recursively from within some implication.
solver ::
  Term ->
  Progs Term ->
  [JTTerm] ->
  Solvers.CHRState.CHR Store Term (State Term)
solver = solverConf False

solverNested ::
  Term ->
  Progs Term ->
  Solvers.CHRState.CHR Store Term (State Term)
solverNested t progs = solverConf True t progs []

----------------------------------------

-- Given a program (compiled CHRs), runs a derivation on a list of
-- justified constraints, returning the final solver state.
-- If `nested' is True, we assume the goal has already been set, and ignore
-- any passed-in terms.
-- The lone term argument corresponds to the top-level t variable in the goal,
-- i.e. the type we're trying to infer.
solverConf ::
  Bool ->
  Term ->
  Progs Term ->
  [JTTerm] ->
  Solvers.CHRState.CHR Store Term (State Term)
solverConf nested t0 progs tjs = do
  cPuts "solverConf"

  if nested
    then do
      -- Throw any user constraints from the `ustore' back on the stack.
      state@(State s u d b h id vs) <- getState
      putState (State (ustoreToStack u ++ s) [] d b h id vs)
    else do
      -- Create a new state containing the fresh goal constraints.
      let (ts, js) = unzip tjs
      goal@(State s _ _ _ _ _ _) <- doHerbrand $ createGoal ts js
      -- We also need to add any equations in the initial goal to the BStore.
      let sbcs = getBuiltinCons s
      bcs <-
        doHerbrand $
          mapM
            (uncurry extOrigBCons)
            [(t, j) | Solvers.CHRState.BCons t j <- sbcs]
      let goal' = addToBStore bcs goal
      putState goal'

  -- Run the derivation in Herbie, and get the result out.
  stagedDerivation progs
  state <- getState
  -- If the derivation failed, then return.
  if isFailedState state
    then return state
    else do
      -- Check if there are any implications.
      -- NOTE: Implications appear as user constraints to the CHR solver.
      let State s u d b h id vs = state
      (nucs, nics) <- doHerbrand $ splitUConsICons state
      -- If there are no implications, we're done -- a success!
      if null nics
        then return state
        else do
          -- There are implications in the result.
          -- let ics = map unwrapUStoreCons nics
          let state' = State s nucs d b h id vs

          -- Solve all implications.
          msolved <- solveImps t0 progs state' nics
          case msolved of
            -- Some implication could not be solved (not even partially.)
            Nothing ->
              -- Grab the current state, which contains the
              -- failure, and return that.
              do
                state <- getState
                -- Just making sure the current state has been
                -- updated to reflect the failure lower down.
                if isFailedState state
                  then return state
                  else bug "State is supposed to be a failure!"

            -- All implications were at least partially solved. Herbrand
            -- constraints in the result are already in effect in the
            -- store. User constraints, and remaining implications
            -- in the result have been returned.
            Just (ics, ucs) -> do
              cPuts "passed through all implications"
              let state'' = State s (ics ++ ucs ++ nucs) d b h id vs
              case ics of
                -- No implications left, all done.
                [] -> cPuts "all implications done" >> return state''
                -- Some implications remain, but we added something
                -- from each to the store, so go around again.
                _ -> do
                  cPuts "going back for another pass"
                  putState state''
                  solverNested t0 progs

----------------------------------------

-- Solves a sequence of implications
-- The lone term argument corresponds to the top-level t variable in the goal,
-- i.e. the type we're trying to infer.
-- Returns
--  Nothing: if any could not be solved (even partially), OR
--  Just (ics, ucs): ics are the implications which remain, ucs the new user
--                     constraints that need to be added (The store may have
--                     been silently updated with new herbrand constraints.)
solveImps ::
  Term ->
  Progs Term ->
  State Term ->
  UStore Term ->
  Solvers.CHRState.CHR Store Term (Maybe (UStore Term, UStore Term))
solveImps t0 progs state [] = return (Just ([], []))
solveImps t0 progs state (ic : ics) = do
  putState state
  -- Solve the current implication.

  cPuts "solving an implication"
  s <- solveImp t0 progs state ic
  cPuts "done solving an implication"
  case s of
    -- Can't solve the implication - the whole process fails.
    CantSolve ->
      -- cPuts "CantSolve" >>
      return Nothing
    -- Forget the implication - it was a `solved form' wrt. the store.
    CanRemove -> solveImps t0 progs state ics
    -- Partially solved the implication. Record the partial solution and
    -- move on to the next one.
    UpdatedStore ucs -> do
      -- use the new, updated state for the next implication
      state' <- getState
      mss <- solveImps t0 progs state' ics
      case mss of
        Nothing -> return Nothing
        Just (ics, ucss) -> return (Just (ic : ics, ucs ++ ucss))

----------------------------------------

-- Attempts to solve a justified term representing an implication. We pass in
-- the current program, the state and the justified implication term.
-- The lone term argument corresponds to the top-level t variable in the goal,
-- i.e. the type we're trying to infer.
-- We will either
--  fail: can't solve the implication at all.
--  succeed: the implication can be removed.
--  partially succeed: we found some new constraints to add to the store,
--                       the implication remains
solveImp ::
  Term ->
  Progs Term ->
  State Term ->
  UStoreCons Term ->
  Solvers.CHRState.CHR Store Term ImpStatus
solveImp t0 progs c0_state@(State _ ucs_c0 orig_del _ _ _ orig_vs_c0) jimp = do
  --         jimp@(imp,j)= do

  let (imp, j) = unwrapUStoreCons jimp

  let bs_c0 = getBStore c0_state
  -- cPuts ("bs_c0: " ++ pretty bs_c0)

  -- Create a choice point.
  -- We'll want to return to it before adding a partial result to the store.
  cp <- doHerbrand createCP

  -- Find all the `C0Vars!' constraints. These contain within them all of the
  -- variables in this implication's context (C0, in the description.)
  vs_c0' <- doHerbrand $ findC0Vars ucs_c0
  let vs_c0 = nub (orig_vs_c0 ++ vs_c0')

  cPuts "vs_c0\n"
  mapM_
    ( \t -> do
        cPuts (show t ++ " = ")
        doHerbrand (printTerm_ t)
    )
    vs_c0

  -- Disect the implication term into its components: LHS and RHS user and
  -- herbrand constraints (with justifications), and quantified variables.
  (evs_info, jlucs, jlbcs, jrucs, jrbcs) <- doHerbrand $ disectICons imp
  let (lucs, jlus) = unzip jlucs
      (lbcs, jlbs) = unzip jlbcs
      (rucs, jrus) = unzip jrucs
      (rbcs, jrbs) = unzip jrbcs
  tvs <- doHerbrand $ mapM extVarInfo evs_info
  let (evs, orig_vars) = unzip tvs
  -- cPuts ("orig_vars:\n" ++ showlist orig_vars ++ "\n")

  -- Create a fresh goal for the LHS constraints (namely, D).
  goal_d@(State s u d b h i vs_d) <-
    doHerbrand $
      createGoal
        (lucs ++ lbcs)
        (jlus ++ jlbs)
  -- Add user constraints from C0 to this goal/state.
  bs <- doHerbrand $ mapM (uncurry extOrigBCons) (jlbcs :: [(Term, Just)])
  let goal_d' = State (ustoreToStack ucs_c0 ++ s) u d (bs ++ b ++ bs_c0) h i vs_d
  putState goal_d'

  -- cPuts ("bcs C0, D:" ++ pretty (bs++b++bs_c0))

  -- Run a CHR derivation. (NOTE: no new implications can arise.)
  stagedDerivation progs
  state <- getState

  if isFailedState state
    then do
      -- LHS derivation failed - we can't solve this implication.
      --
      -- ERROR REPORTING NOTE: We keep the unsatisfiable store, and the
      -- error is reported as a typical type conflict.
      doHerbrand $ rewindMaintainHeap cp
      cPuts "LHS Deriv. failed"
      return CantSolve
    else do
      -- LHS derivation succeeded - carry on.
      let State s ucs_d' del_d' bstore _ _ _ = state

      -- cPuts ("bcs D':" ++ pretty bstore)

      -- `Remember' the ucs in D for solved-form checking later.
      let tjs_d' = map unwrapUStoreCons ucs_d'

      -- Terms from D and C0 that are still vars after the first derivation.
      vs_d' <- doHerbrand $ keepVars vs_d
      vs_c0' <- doHerbrand $ keepVarsNoDeref vs_c0

      cPuts ("\nvs_c0 (in D')\n")
      mapM_
        ( \t -> do
            cPuts (show t ++ " = ")
            doHerbrand (printTerm_ t)
        )
        vs_c0

      cPuts ("vs_c0'\n")
      mapM_
        ( \t -> do
            cPuts (show t ++ " = ")
            doHerbrand (printTerm_ t)
        )
        vs_c0'
      -- Find all the existential and D' vars which are uninstantiated (i.e.
      -- are still vars, as above.)
      evs' <- doHerbrand $ keepVars evs

      cPuts "\nevs'\n"
      mapM_
        ( \t -> do
            cPuts (show t ++ " = ")
            doHerbrand (printTerm_ t)
        )
        evs'

      -- NOTE: it is critical that we calculate vs BEFORE the second
      -- derivation
      vs <- doHerbrand $ keepVarsNoDeref (evs' ++ vs_d' ++ vs_c0')

      -- Create a fresh goal for the RHS constraints (namely, C).
      goal_c@(State s u d b h i vs_c) <-
        doHerbrand $
          createGoal (rucs ++ rbcs) (jrus ++ jrbs)
      -- Add user constraints from D' (result of LHS derivation) to this
      -- goal/state.
      bs <- doHerbrand $ mapM (uncurry extOrigBCons) (jrbcs :: [(Term, Just)])
      let goal_c'_ucs = ustoreToStack ucs_d' ++ s
          goal_c' =
            State
              goal_c'_ucs
              u
              (orig_del ++ del_d' ++ d)
              (b ++ bs ++ bstore)
              h
              i
              (nub (vs_c ++ vs_d' ++ vs_c0'))
      putState goal_c'

      -- cPuts ("D', C:" ++ pretty (b++bs++bstore))

      -- Because implications may be nested, we recursively
      -- call `solver' here instead of `derivation'.
      state <- solverNested t0 progs
      if isFailedState state
        then do
          -- RHS derivation failed - we can't solve this implication.
          --
          -- ERROR REPORTING NOTE: We keep the unsatisfiable store, and the
          -- error is reported as a typical type conflict.
          doHerbrand $ rewindMaintainHeap cp
          cPuts "RHS Deriv. failed"
          return CantSolve
        else do
          -- RHS derivation succeeded - press onward.
          let State _ ucs_c'0 _ bstore _ _ _ = state
          ucs_c' <- doHerbrand $ dropC0VarsUCons ucs_c'0
          let tjs_c' = map unwrapUStoreCons ucs_c'

          cPuts "\nvs_c0' (in C')\n"
          mapM_
            ( \t -> do
                cPuts (show t ++ " = ")
                doHerbrand (printTerm_ t)
            )
            vs_c0'

          cPuts "\nts_d'"
          doHerbrand $ mapM_ (\(t, _) -> printTerm_ t) tjs_d'

          cPuts "\nts_c'"
          doHerbrand $ mapM_ (\(t, _) -> printTerm_ t) tjs_c'

          -- Check whether the implication is already a `solved form' wrt the
          -- context constraints (C0).
          -- NOTE: tjs_d' are the justified user constraint terms from the
          --             LHS derivation, and tjs_c' are the same from the RHS.
          solved <- isSolvedForm vs tjs_d' tjs_c'
          if solved
            then do
              -- Solved form - drop the implication.
              -- cPuts "solved form"
              doHerbrand (rewindMaintainHeap cp)
              return CanRemove
            else do
              -- Not a solved form, extract some constraints
              -- (a partial solution.)

              cPuts "not a solved form"

              -- Find the constraints in the partial solution.
              mcs <- newImpCons t0 (imp, j) vs_c0' evs' ucs_d'
              case mcs of
                -- Some universal var has been instantiated - can't solve.
                -- ERROR REPORTING NOTE: At some point we'd like to make
                -- the distinction between a polymorphic type variable, and
                -- an `existential' variable being instantiated.
                UnivInst t -> do
                  cPuts "Some polymorphic variable is instantiated"
                  doHerbrand (rewindMaintainHeap cp)
                  v <- doHerbrand $ extOrigType t
                  -- NOTE: We need to remove t from the list of *other*
                  -- universal vars., or the error reporting may fail.
                  uvs <- doHerbrand $ mapM extOrigType (delete t evs')
                  state <- getState
                  let bs = getBStore state
                      uvs' = map fromTVar uvs
                      state' = FailedUniv bs (fromTVar v) uvs'
                  putState state'
                  return CantSolve

                -- FIXME: we still want to associate the location information
                -- from the original variable names, with the current evs'.
                UnivEscaped t -> do
                  cPuts "Polymorphic variable escaped"
                  doHerbrand $ rewindMaintainHeap cp
                  state <- getState
                  uvs <- doHerbrand $ mapM extOrigType evs'
                  let bs = getBStore state
                      uvs' = map fromTVar uvs
                      state' = FailedUnivEsc bs t uvs'
                  putState state'
                  return CantSolve
                {-
                                  UCUnmatched (t,j) -> do
                                    cPuts "Unmatched user constraint"

                                    uc_temp <- doHerbrand $ extUCons t j
                                    cPuts ("uc_temp: " ++ pretty uc_temp)

                                    doHerbrand $ rewindMaintainHeap cp
                                    uc <- doHerbrand $ extUCons t j
                                    let state' = FailedUCUnmatched uc
                                    putState state'
                                    return CantSolve
                -}
                ImpSoln ucs bcs0 -> do
                  -- We have the new ucs and bcs.
                  -- Undo the C',D' derivations, add the bcs and check if
                  -- anything new was added (if not, and there are no ucs,
                  -- then fail - we can't solve this implication).

                  -- NOTE: It's possible that the user constraints we lift
                  -- out as part of the partial solution contain non-C0
                  -- variables which are ground, or instantiated by C0
                  -- variables. As soon as we rewind (to the point before
                  -- either derivation) we will lose this connection. So, we
                  -- need to build additional equations (term/term pairs) to
                  -- represent these, and add them to the bcs returned above.
                  uc_bcs <- doHerbrand $ uconsBindsToBCons ucs
                  let bcs = uc_bcs ++ bcs0

                  skol <- doHerbrand $ do
                    -- Undo everything. We just need to check if
                    -- the suggested new constraints add anything.
                    rewindMaintainHeap cp

                    -- We only want the variables in C0 which were
                    -- still just variables before we started to
                    -- solve this implication.
                    vs_c0' <- keepVars vs_c0
                    mapM_ (uncurry unify) bcs -- this can't fail
                    skolemise False vs_c0'

                  -- If skolemisation succeeded, then none of the C0
                  -- variables were grounded. Effectively, no new `real'
                  -- herbrand constraints were suggested.
                  if skolOk skol && null ucs
                    then do
                      -- Nothing new added, can't solve this implication.
                      -- FIXME: IMP. ERROR REPORTING
                      cPuts "Can't solve this implication - not sure why"
                      return CantSolve
                    else do
                      -- Partially solved, return the user constraints.
                      --                        cPuts "Partially updated the store"
                      state <- getState
                      new_bcs <- pairsToBCons j bcs -- noJust bcs
                      -- cPuts ("new_bcs: " ++ pretty new_bcs)

                      -- rewind bstore!
                      let state' = setBStore (new_bcs ++ bs_c0) state
                      putState state'
                      {-
                                              cPuts "new ucs: "
                                              doHerbrand $ mapM_ printTerm_ $ map (fst . unwrapUStoreCons) ucs
                      -}
                      return (UpdatedStore ucs)

                -- This is basically the same case as ImpSoln, BUT, if we
                -- find nothing was added by the bcs, then we can report that
                -- the given user constraints are unmatched.
                -- Note also that we don't build any equations from the ucs,
                -- since they're only here for error reporting purposes.
                -- (Look to the ImpSoln case for some comments about what's
                -- going on here.)
                ImpSolnUnmatchedUCs ucs@((t, j) : _) bcs -> do
                  cPut "ImpSolnUnmatchedUCs"
                  skol <- doHerbrand $ do
                    rewindMaintainHeap cp
                    vs_c0' <- keepVars vs_c0
                    mapM_ (uncurry unify) bcs -- this can't fail
                    skolemise False vs_c0'

                  -- If skolemisation succeeded, then none of the C0
                  -- variables were grounded. Effectively, no new `real'
                  -- herbrand constraints were suggested.
                  if skolOk skol
                    then do
                      -- Nothing new added, can't solve this implication.
                      -- BUT, there were some unmatched user constraints in
                      -- the RHS, so complain about one of those now.
                      -- (We just take the first one in the list.)

                      cPuts "Unmatched user constraint"
                      uc <- doHerbrand $ extUCons t j
                      let state' = FailedUCUnmatched uc
                      putState state'
                      return CantSolve
                    else do
                      -- Partially solved, return the user constraints.
                      cPut "Partially updated the store"
                      state <- getState
                      new_bcs <- pairsToBCons j bcs -- noJust bcs
                      --                        cPut ("new_bcs: " ++ pretty new_bcs)

                      -- rewind bstore!
                      let state' = setBStore (new_bcs ++ bs_c0) state
                      putState state'
                      {-
                                              cPuts "new ucs: "
                                              doHerbrand $ mapM_ printTerm_ $ map fst ucs
                      -}
                      -- We return no ucs as part of the partial solution,
                      -- since the only ones we know about at this point are
                      -- the error ones above (and since the equations *DID*
                      -- add something, we can ignore them.)
                      return (UpdatedStore [])

-- Given a list of terms (packaged in a UStore), representing a list of user
-- constraints, returns a list of equations (as Term/Term pairs) mapping the
-- `immediate' type argument of each UC to its instantiated value.
-- e.g. say the store contains: U t1 t2, t1 = t2, t2 = Int,
--        we return (t1 = Int, t2 = Int)
uconsBindsToBCons :: UStore Term -> Herbrand Store [(Term, Term)]
uconsBindsToBCons store = do
  let ucs = map (fst . unwrapUStoreCons) store
  concatMapM binds ucs
  where
    binds :: Term -> Herbrand Store [(Term, Term)]
    binds uc = do
      (_, arr) <- functor uc
      ts <- mapM (arg uc) [1 .. arr]
      ts' <- mapM deref ts
      return (zip ts ts')

-- Converts a list of pairs of terms to justified built-ins. All equations
-- share the same justification.
pairsToBCons :: Just -> [(Term, Term)] -> Solvers.CHRState.CHR Store Term [BCons]
pairsToBCons j = doHerbrand . mapM mkBCons
  where
    mkBCons (t1, t2) = do
      t1' <- extOrigType t1
      t2' <- -- extType t2
        extOrigType t2
      return ((t1' =+= t2') j)

----------------------------------------

-- represents an attempt to solve an implication, cases include:
-- ImpSoln : new user constraints, and equations to add to the store
-- ImpSolnUnmatchedUCs: No user constraints could be lifted out into the
--            partial solution; the ones we return are those which contained
--            `bad' variables, either skolem, or non-C0. If it turns out that the
--            equations add nothing, then we can report that these constraints
--            remain unmatched.
-- UnivInst: a universal variable is already instantiated, can't continue
-- UnivEscaped: a universal variable t1, escapes (a C0 var, t2 is bound to it)
data ImpSoln
  = ImpSoln (UStore Term) [(Term, Term)]
  | ImpSolnUnmatchedUCs [JTTerm] [(Term, Term)]
  | UnivInst Term
  | UnivEscaped Type

-- The given implication is not a solved form (wrt current store), so extract
-- some constraints from it so we can continue.
-- Note: the current store contains C', no variables are skolemised
-- We pass in: the implication, a list of the exist. vars., the user
--               constraints from D'
-- The lone term argument corresponds to the top-level t variable in the goal,
-- i.e. the type we're trying to infer.
-- Returns an ImpSoln thing.
--
-- ERROR REPORTING NOTE: A number of things can go wrong at this point:
--  - Some user constraint on the RHS doesn't appear on the LHS
--    and can't be floated out.
--  - A universal variable is instantiated
--  - The top-level type variable is bound to a sk. constructor (univ. variable)
newImpCons ::
  Term ->
  JTTerm ->
  [Term] ->
  [Term] ->
  UStore Term ->
  Solvers.CHRState.CHR Store Term ImpSoln
newImpCons t0 (imp, j) vs_c0 evs ucs_d'0 = do
  -- Skolemise the exist. vars.
  cp <- doHerbrand $ createCP
  skol <- doHerbrand $ skolemise True evs
  if not (skolOk skol)
    then do
      -- Skolemisation failed, some exist. var has been instantiated
      doHerbrand $ rewindMaintainHeap cp
      return (UnivInst (fromJust skol))
    else do
      state@(State _ ucs_c'0 _ _ _ _ _) <- getState
      -- Drop the C0Vars! constraints before matching.
      ucs_c' <- doHerbrand $ dropC0VarsUCons ucs_c'0
      ucs_d' <- doHerbrand $ dropC0VarsUCons ucs_d'0
      -- ucs1: all user constraints in ucs_c' which are not in ucs_d', do not
      -- contain any skolem constants, and whose variables are all in vs_c0
      -- f_ucs: user constraints in ucs_c' which are not in ucs_d', but
      -- contain `bad' variables, skolemised or non-C0
      (ucs1, f_ucs) <- doHerbrand $ filterUCs ucs_c' ucs_d' [] []

      -- Now find all equations representing the values of variables in
      -- C0. These cannot contain any skolem consts, or non-C0 variables.
      ts <- doHerbrand $ termsToTypes vs_c0
      -- NOTE: This only works because extType also uses show to convert
      -- vars. i.e. the `externalisation' is the same.
      let tmts = zip vs_c0 ts
      vs_c0 <- doHerbrand $ mapM deref vs_c0
      let tvs_c0 = map show vs_c0
          vm = listToVarMap (zip (map mkVar tvs_c0) vs_c0)

      -- ERROR REPORTING NOTE: We fail right away if the type bound to
      -- the top-level t variable contains a skolem constructor. This
      -- represents a polymorphic variable escaping from its scope. Even
      -- if we weren't to fail immediately, we'd end up failing
      -- eventually because the implication constraint will be found to
      -- be unsolvable.
      -- FIXME: Add note about non-confluent programs.
      tt0s <- doHerbrand $ termsToTypes [t0]
      let tt0 = head tt0s
      let esc = any (`hasPrefix` "SK!") (map nameStr (typeTCons tt0))
      case False {- esc -} of
        True -> return (UnivEscaped tt0)
        -- no polymorphic variables escape
        False -> do
          bcs1 <- doHerbrand $ filterBCs vm tmts
          doHerbrand $ rewindMaintainHeap cp
          cPuts "returning a partial solution"

          -- If there are no user constraints in the partial solution (ucs1
          -- above), but there are unmatched constraints (f_ucs from above)
          -- then return an ImpSolnUnmatchedUCs.)
          return $
            if null ucs1 && not (null f_ucs)
              then ImpSolnUnmatchedUCs (map unwrapUStoreCons f_ucs) bcs1
              else ImpSoln ucs1 bcs1
  where
    -- Given a variable mapping (from Haskell vars to terms), and a list of
    -- pairs representing the value of each C0 variable (as a Haskell type),
    -- returns a list of pairs representing hebrand constraints binding each
    -- (C) variable with its result.
    --
    -- The list of terms and types represents `candidate' equations.
    -- We filter out the equations where the C0 var. is mapped to a type
    -- containing either a non-C0 var.
    -- NOTE: We've already checked that none of the C0s is bound to anything
    -- containing a skolem constructor. (and failed, if that was the case)
    -- FIXME: add check for skolem cons!!
    filterBCs :: VarMap -> [(Term, Type)] -> Herbrand Store [(Term, Term)]
    filterBCs _ [] = return []
    filterBCs vm (tmt@(tm, t) : tmts) = do
      let -- all variables must be in C0
          all_c0 = all (`elemVarMap` vm) (fv t)
          any_sk = any (`hasPrefix` "SK!") (map nameStr (typeTCons t))
      if {-not all_c0 || -} any_sk
        then -- dropped a candidate equation
          filterBCs vm tmts
        else do
          -- kept a candidate equation
          (_, t') <- intType vm t
          hPuts "t':"
          printTerm_ t'
          rest <- filterBCs vm tmts
          return ((tm, t') : rest)

    -- Splits the user constraints from cs, which aren't in ds,
    -- into two groups:
    --  a) Those which can be lifted out as part of a partial solution.
    --        b) Those which contain variables which cannot be allowed to escape --
    --           such a constraint represents an error.
    -- We can only lift out constraints that contain variables from C0, and no
    -- polymorphic variables.
    -- Note that if there are any constraints in the `b' group, we will
    -- immediately (upon returning from this function) fail.
    --
    -- ERROR REPORTING NOTE: User constraints already on the RHS are a-okay.
    -- Lifting out constraints which contain only C0 variables is okay too.
    -- Otherwise, we have the following cases:
    --        1) The uc contains a polymorphic variable.
    --  2) The uc contains a fresh variable (We ought to have a
    --           range-restrictedness check that would eliminate this.
    --           But we don't do one yet.)
    -- We could handle these differently, but we don't. In both cases we just
    -- report that `some user constraint is unmatched'.
    --
    -- Some alternatives (corresponding to the cases above), include:
    --  1) Suggest adding a type class constraint to the relevant
    --           type declaration or data type guard.
    --  2) Report this as an ambiguity error. i.e. tell the programmer where he
    --     can add type information to ground these fresh variables.
    filterUCs ::
      UStore Term ->
      UStore Term ->
      UStore Term ->
      UStore Term ->
      Herbrand Store (UStore Term, UStore Term)
    filterUCs [] ds as fs = return (as, fs)
    filterUCs (c : cs) ds as fs = do
      -- Check whether c is already in ds.
      copy <-
        let eq ::
              UStoreCons Term ->
              UStoreCons Term ->
              Herbrand Store Bool
            eq t1 t2 = do
              let (t1', _) = unwrapUStoreCons t1
                  (t2', _) = unwrapUStoreCons t2
              s <- eqeq t1' t2'
              return (s == Success)
         in anyM (eq c) ds

      -- This will be True if c should be eliminated because of its variables.
      -- These conditions are described below.
      badVs <- do
        let (uc, _) = unwrapUStoreCons c

        -- Check if there are variables in uc, not appearing in C0.
        vs_c0_deref <- mapM deref vs_c0
        let vs_c0' = vs_c0_deref ++ vs_c0
        vs <- fvCons uc
        let new = any (`notElem` vs_c0') vs

        -- Check if uc contains any existential vars
        -- (which are skolemised at this point.)
        as <- conArgs uc
        -- let sk = any (`elem`dr_evs) dr_as
        mts <- mapM findSkBound as

        -- FIXME: This condition may be unnecessarily strong. e.g.
        -- this user constraint could be in a nested implication,
        -- but contains polymorphic variables from the outer
        -- implication. (Could this even happen without an error?)
        let sk = any isJust mts

        -- If either condition is True, c/uc is no good.
        return (new || sk)

      let as'
            | copy || badVs = as
            | otherwise = c : as
          fs'
            | not copy && badVs = c : fs
            | otherwise = fs
      filterUCs cs ds as' fs'

----------------------------------------

-- Find all the C0Vars! user constraints, and collect all their variables
-- (These are the free variables in C0.)
findC0Vars :: (Eq c, Herb s c) => UStore c -> Herbrand s [c]
findC0Vars ustore = do
  c0UCs <- hasC0Func ustore []
  vss <- mapM getVars c0UCs
  return (concat vss)
  where
    -- Returns the terms representing the C0Vars! user constraints.
    hasC0Func [] ucs = return ucs
    hasC0Func ((Num (UCons f _ uc _) _) : ss) ucs = do
      isC <- isCnst f
      if not isC
        then bug "findC0Vars: functor is not a constant!"
        else do
          cstr <- cnstName f
          -- str <- doIO $ peekCString cstr
          let ucs'
                | cstr == "C0Vars!" = uc : ucs
                | otherwise = ucs
          hasC0Func ss ucs'

    -- Returns all of the variables in one of these C0Vars! terms
    -- (it's a list of nested tuples in the term.)
    getVars uc = do
      t <- arg uc 1
      vs <- fvType t
      return (nub vs)

-- Removes all the C0Vars! user constraints from the given store.
dropC0VarsUCons :: Herb s c => UStore c -> Herbrand s (UStore c)
dropC0VarsUCons us = do
  let ts = map (fst . unwrapUStoreCons) us
  fas <- mapM functor ts
  let fs = map fst fas
  fs' <-
    mapM
      ( \f -> do
          nm <- cnstName f
          -- doIO $ peekCString nm
          return nm
      )
      fs
  return [u | (f, u) <- zip fs' us, f /= "C0Vars!"]

-- As above, but works directly on justified terms, rather than the UC store.
dropC0VarsJTerms :: Herb s c => [JTerm c] -> Herbrand s [JTerm c]
dropC0VarsJTerms ts = do
  fas <- mapM (functor . fst) ts
  let fs = map fst fas
  fs' <-
    mapM
      ( \f -> do
          nm <- cnstName f
          -- doIO $ peekCString nm
          return nm
      )
      fs
  return [u | (f, u) <- zip fs' ts, f /= "C0Vars!"]

----------------------------------------

-- Keep only the terms which represent vars.
filterVars :: Herb s c => [c] -> Herbrand s [c]
filterVars ts = filt [] ts
  where
    filt vs [] = return vs
    filt vs (t : ts) = do
      isV <- isVar t
      let vs'
            | isV = t : vs
            | otherwise = vs
      filt vs' ts

-- Like the above (filterVars), but also de-references the vars and removes
-- duplicates.
keepVars :: (Eq c, Herb s c) => [c] -> Herbrand s [c]
keepVars vs = do
  vs' <- filterVars vs
  vs'' <- mapM deref vs'
  return (nub vs'')

keepVarsNoDeref :: (Eq c, Herb s c) => [c] -> Herbrand s [c]
keepVarsNoDeref vs = do
  vs' <- filterVars vs
  vs'' <- mapM deref vs'

  let pvs = zip vs' vs''
      vs''' = map fst (nubBy eq pvs)

  return vs'''
  where
    eq (_, v1) (_, v2) = v1 == v2

nubVarsNoDeref :: (Eq c, Herb s c) => [c] -> Herbrand s [c]
nubVarsNoDeref vs = do
  vs' <- mapM deref vs

  let pvs = zip vs vs'
      vs'' = map fst (nubBy eq pvs)

  return vs''
  where
    eq (_, v1) (_, v2) = v1 == v2

----------------------------------------

-- Dereferences each term and converts the result into a Haskell type.
termsToTypes :: [Term] -> Herbrand Store [Type]
termsToTypes [] = return []
termsToTypes (tm : tms) = do
  tm' <- deref tm
  t <- extType tm'
  ts <- termsToTypes tms
  return (t : ts)

-- find a single sub-term which is bound to a skolem. cons
findSkBound :: Term -> Herbrand Store (Maybe Term)
findSkBound t0 = do
  t <- deref t0
  isV <- isVar t
  if isV
    then return Nothing
    else do
      isC <- isCnst t
      if isC
        then do
          cstr <- cnstName t
          -- str <- doIO $ peekCString cstr
          if cstr `hasPrefix` "SK!"
            then return (Just t0)
            else return Nothing
        else do
          -- ``It's an App!''
          t1 <- appArg t 0
          mb1 <- findSkBound t1
          case mb1 of
            Just _ -> return mb1
            Nothing -> do
              t2 <- appArg t 1
              mb2 <- findSkBound t2
              case mb2 of
                Just _ -> return mb2
                Nothing -> return Nothing

----------------------------------------

-- Checks if the implication we just processed is a solved form wrt its context.
-- Takes a list of variables (fv(C_0,D_1,\bar{a})), a list of the
-- user constraints in the original D', and the user constraints in C'.
-- Note that the D' constraints are copies.
-- NOTE: Herbie skolemisation is broken by design - don't use it!
--         It sets all variables to the same skolem constant!
isSolvedForm ::
  [Term] ->
  [JTTerm] ->
  [JTTerm] ->
  Solvers.CHRState.CHR Store Term Bool
isSolvedForm vs ucs_d'0 ucs_c'0 = do
  cPuts "solved form check"
  cp <- doHerbrand createCP
  -- First see if any LHS variables are affected.
  cPuts ("\nvs\n")
  mapM_
    ( \t -> do
        cPuts (show t ++ " = ")
        t' <- doHerbrand $ deref t
        doHerbrand (printTerm_ t')
    )
    vs
  --
  skol <- doHerbrand $ skolemise True vs
  let eqsOkay = skolOk skol
  if not eqsOkay
    then do
      -- They are, so it's not a solved form.
      cPuts "some LHS var. instantiated"
      doHerbrand $ rewindMaintainHeap cp
      return False
    else do
      -- No variables affected, now check for matching ucons.
      -- Filter out the C0Vars! constraints.
      ucs_d' <- doHerbrand $ dropC0VarsJTerms ucs_d'0
      ucs_c' <- doHerbrand $ dropC0VarsJTerms ucs_c'0

      cPuts "comparing LHS and RHS ucons"
      cPuts "ucs_d'"
      doHerbrand $ mapM_ printTerm_ (map fst ucs_d')
      cPuts "ucs_c'"
      doHerbrand $ mapM_ printTerm_ (map fst ucs_c')

      if null ucs_d' && null ucs_c'
        then cPuts "solved form1!" >> return True -- a trivial match (no user constraints present)
        else do
          -- Match `inferred' ucons. against `provided'.
          m <- doHerbrand $ matchUCons ucs_c' ucs_d'
          doHerbrand $ rewindMaintainHeap cp
          case m of
            [] -> cPuts "ucons don't match" >> return False -- No matching, not a solved form.
            _ -> cPuts "solved form!" >> return True -- A match, solved form!

-- Looks up the term associated with each variable in the VarMap.
lookupVars :: VarMap -> Herbrand Store [(Var, Type)]
lookupVars vm = do
  let vml = varMapToList vm
      (vs, tms) = unzip vml
  ts <- mapM extType tms
  return (zip vs ts)

mkEq :: (Var, Type) -> BCons
mkEq (v, t) = (TVar v) =:= t

unwrapUStoreCons :: Herb s c => UStoreCons c -> (c, Just)
unwrapUStoreCons = \(Num (Solvers.CHRState.UCons _ _ t j) _) -> (t, j)

----------------------------------------

-- Attempts to unify each term in the list with a new skolem constant.
-- If `go' is True, then the unification is performed, otherwise it is undone,
-- and the caller doesn't have to clean up anything.
-- NOTE: It's the caller's responsibility to not pass in any duplicates. (Which
--         will involve dereferencing all the terms to check.)
skolemise :: Bool -> [Term] -> Herbrand Store (Maybe Term)
skolemise go ts =
  if go
    then skol ts
    else do
      cp <- createCP
      res <- skol ts
      rewind cp
      return res
  where
    -- Returns Nothing if all terms are skolemised successfully, else Just the
    -- term that it failed on.
    skol [] = return Nothing
    skol (t : ts) = do
      sk <- genSkolemConst
      {-
              hPuts ("new skolem const: " ++ show sk)
              printTerm sk
              hPuts ("about to skolemise term: " ++ show t)
              printTerm t
      -}
      stat <- unify t sk
      case stat of
        Success -> skol ts
        _ -> do
          -- hPuts "skolemisation just failed"
          return (Just t)

-- Tests whether skolemisation was successful.
skolOk :: Maybe a -> Bool
skolOk = isNothing

----------------------------------------

-- Check whether there exists renaming function phi
-- ( dom(phi) cap tvs = emptyset)
-- such that ucs2 = phi ucs1
-- Returns the matching (or an empty list, if there's no match).
--
-- This works by generating all possible pairs of constraints from the
-- two lists, and eliminating those which aren't matchings.
-- Actually its a bit cleverer in that it only generates potential matchings
-- where the predicate symbols are the same.
--
-- Below, ucs1 are declared/provided, ucs2 are demanded/inferred.
matchUCons :: [JTTerm] -> [JTTerm] -> Herbrand Store [(JTTerm, JTTerm)]
matchUCons jucs1 jucs2 = do
  {-
      hPuts2 "matchUCons"
      let ucs1 = map fst jucs1
          ucs2 = map fst jucs2
          pr (t,j) = printTerm t >> hPuts2 (show j)
      hPuts2 "\nucs1:\n"
      mapM_ pr jucs1
      hPuts2 "\nucs2:\n"
      mapM_ pr jucs2
  -}
  -- Build all sensible combinations of pairs.
  jucs2_alts <- altMatch
  let ps = zip jucs2_alts (repeat jucs1)
  -- Find a match.
  ms <- findMatch ps
  let m = case ms of
        [] -> [] -- No match.
        ((ucs1, ucs2) : _) -> zip ucs1 ucs2 -- At least one match.
  return m
  where
    -- Goes through the list of potential matches, until it finds a real match,
    -- which it returns.
    findMatch :: [([JTTerm], [JTTerm])] -> Herbrand Store [([JTTerm], [JTTerm])]
    findMatch [] = return []
    findMatch (p : ps) = do
      m <- isMatch p
      if m
        then return [p]
        else findMatch ps

    -- True if the corresponding elements of each list match.
    isMatch :: ([JTTerm], [JTTerm]) -> Herbrand Store Bool
    isMatch (jucs1, jucs2) = do
      let (ucs1, js1) = unzip jucs1
          (ucs2, js2) = unzip jucs2
      cp <- createCP
      ok <- canUnifyAll (zip ucs1 ucs2)
      rewind cp
      return ok
      where
        -- Attempts to unify all pairs in the list, until it either fails, or
        -- gets through the list (succeeding.) Note: Since the underlying term
        -- representation of user constraints is untyped, we can just unify
        -- them as though they are types.
        canUnifyAll :: Herb s c => [(c, c)] -> Herbrand s Bool
        canUnifyAll [] = return True
        canUnifyAll ((uc1, uc2) : ucs) = do
          stat <- unify uc1 uc2
          case stat of
            Success -> canUnifyAll ucs
            _ -> return False

    -- Generates all the potential matches.
    -- We should avoid generating obviously useless matches (i.e. where the
    -- predicate symbols are different.) So, first group each term in jucs2 by
    -- predicate symbol and get the symbols in jucs1 in order. Then
    -- generate all alternatives by walking down the jucs1 symbol list, only
    -- offerring as candidates those terms in the appropriate jucs2 group.
    altMatch :: Herbrand Store [[JTTerm]]
    altMatch = do
      -- Get all the predicate names.
      str1 <- mapM ucStr jucs1
      str2 <- mapM ucStr jucs2
      let ucs_str2 = zip str2 jucs2
          -- Group all the constraints in jucs2 by name.
          group = foldr addToGroup [] ucs_str2
          alts = map (find group) str1
          -- Generate all the potential matches.
          cs = combos alts
      return cs
      where
        -- Returns the predicate name.
        ucStr (uc, j) = do
          (f, _) <- functor uc
          cstr <- cnstName f
          -- doIO (peekCString cstr)
          return cstr

        -- Given the list of jucs2 groups matched against jucs1 predicate
        -- symbols, generates all potential matches.
        combos :: [[JTTerm]] -> [[JTTerm]]
        combos [] = [[]]
        combos (ts : tss) =
          let cs = combos tss
           in [t : c | t <- ts, c <- cs]

        -- NOTE: If we hit the Nothing case below, the whole matchUCons
        --         procedure will end up failing. We should fail asap.
        find :: [(String, [JTTerm])] -> String -> [JTTerm]
        find g str = case lookup str g of
          Nothing -> []
          Just ps -> ps

        -- Builds up the grouping of constraints by predicate symbol.
        addToGroup ::
          (String, JTTerm) ->
          [(String, [JTTerm])] ->
          [(String, [JTTerm])]
        addToGroup (str, t) g =
          let (a, b) = span ((/= str) . fst) g
           in case b of
                [] -> (str, [t]) : a
                ((s', ts) : xs) -> (s', t : ts) : xs ++ a

----------------------------------------

printUStore ucs = do
  let ts = map (fst . unwrapUStoreCons) ucs
  doHerbrand $ mapM_ printTerm_ ts

printVars :: [Term] -> Herbrand Store ()
printVars = mapM_ print
  where
    print t = do
      -- printTerm_ t
      doIO $ putStrLn (show t ++ " = ")
      printTerm_ t
      doIO $ putStr "\n"

-- Like `rewind', but does not rewind heap usage.
rewindMaintainHeap :: Herb s c => s -> Herbrand s ()
rewindMaintainHeap cp = do
  setFlag NoRewindHeap On
  rewind cp
  setFlag NoRewindHeap Off

--------------------------------------------------------------------------------
