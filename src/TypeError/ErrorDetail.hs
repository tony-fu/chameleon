{-# LANGUAGE DeriveGeneric #-}

module TypeError.ErrorDetail where

import AST.External
import AST.SrcInfo
import Control.Monad
import Core.BCons
import Core.ConsOps
import Core.Constraint
import Core.InfGoal
import Core.InfResult
import Core.Inference
import Core.Justify
import Core.Types as T
import Data.Aeson
import Data.Aeson.Text (encodeToLazyText)
import Data.List (elemIndex, group, groupBy, intercalate, intersperse, maximumBy, nub, sort, sortBy)
import Data.Maybe
import Data.Ord (comparing)
import Data.Text.Lazy (unpack)
import GHC.Generics
import Misc
import Misc.Document
import Misc.MinUnsatSet
import Misc.Print
import Misc.Utils
import qualified Data.Set as Set
typeToString :: T.Var -> [BCons] -> IO (Maybe String)
typeToString var cons = do
  results <- runInfGoals [] [InfGoal (anonId "Exp") True [var] (toConstraint cons)]
  return (fromInfResults results)

fromInfResults :: [InfResult] -> Maybe String
fromInfResults [] = Nothing
fromInfResults (x : xs) =
  case x of
    (InfSuccess id (t : ts) c) ->
      let ucs = cUCons c
          (typ', ucs') = T.prettyRename (t, ucs)
          ucs_str
            | null ucs' = ""
            | singleton ucs' = pretty ucs' ++ " => "
            | otherwise = "(" ++ pretty ucs' ++ ") => "
          typ_str = pretty typ'
       in Just (ucs_str ++ typ_str)
    _ -> fromInfResults xs

getChameleonResult :: [InfResult] -> SrcTextData -> Filename -> IO ChameleonResult
getChameleonResult rs srcData filename = do
  errs <- getChameleonErrors rs
  return $
    ChameleonResult
      { filepath = filename,
        placetable = fromSrcTextData srcData,
        decls = errs
      }

getChameleonErrors :: [InfResult] -> IO [ChameleonError]
getChameleonErrors = mapM getChameleonError

getChameleonError :: InfResult -> IO ChameleonError
getChameleonError (InfFailure id vs c) = do
  let allConstrains = cBCons c
  min <- minUnsatSubset allConstrains
  -- let reordered = reorder min
  -- let highestRanks = highestRank (varList reordered) reordered
  -- let highestRankVar = head highestRanks

  let tlv = fromTVar . head $ vs

  let flanks = filter (\c -> isFlank c (filter (/= c) min)) min
  let reducedConstraints = map (\c -> filter (/= c) min) flanks

  original <- mapM
    ( \c -> do
        let removedFlank = filter (/= c) min
        groundingset <- maxSatSubset (removedFlank ++ allConstrains)
        sig <- typeToString tlv groundingset
        return (fromJust sig, Set.fromList groundingset)
    ) flanks
    
  let grouped = groupBy (\a b -> fst a == fst b) original
  let merged = map (\vs -> (fst (head vs), Set.unions (map snd vs))) grouped 
  let intersection = intersectionAll (map snd merged)
  let trimed = map (\v ->  (fst v, snd v `Set.difference` intersection)) merged
  let variants = map (\(v, cons) -> TypeVariant v (concatMap getJustLocs cons)) trimed

  return $
    ChameleonError
      { declInfo = getDeclInfo id,
        typeConflict = Nothing,
        mismatch =
          Just $
            Mismatch
              { allConstraints = min,
                typeVariants = variants
              }
      }

getChameleonError (SubSuccess id) = return $ ChameleonError (getDeclInfo id) Nothing Nothing
getChameleonError (InfSuccess id tlv c) = return $ ChameleonError (getDeclInfo id) Nothing Nothing
getChameleonError (InfFailureUniv id v vs C {cBCons = bcs}) = return $ ChameleonError (getDeclInfo id) Nothing Nothing
getChameleonError (InfFailureUnivEsc id _ _ _ _) = return $ ChameleonError (getDeclInfo id) Nothing Nothing
getChameleonError (InfFailureUConsUnmatched id rUcons) = return $ ChameleonError (getDeclInfo id) Nothing Nothing


intersectionAll :: Ord a => [Set.Set a] -> Set.Set a
intersectionAll [] = Set.empty
intersectionAll [x] = x
intersectionAll [x, y] = x `Set.intersection` y
intersectionAll (x:y:zs) = (x `Set.intersection` y) `Set.intersection` intersectionAll zs


data DeclInfo = DeclInfo
  { nameOriginal :: String,
    nameQualified :: String,
    declaredLoc :: Int,
    declaredFile :: String
  }
  deriving (Show, Generic)

instance ToJSON DeclInfo

getDeclInfo :: Id -> DeclInfo
getDeclInfo (Id idSrcInfo idStr idOrigStr) =
  DeclInfo
    { nameOriginal = idOrigStr,
      nameQualified = idStr,
      declaredLoc = srcLoc idSrcInfo,
      declaredFile = srcFile idSrcInfo
    }


data TypeConflict = TypeConflict
  { constraints :: [BCons],
    conflictTerm :: [Int],
    typeLeftMinimal :: String,
    typeLeftFull :: Maybe String,
    typeRightMinimal :: String,
    typeRightFull :: Maybe String
  }
  deriving (Show, Generic)

instance ToJSON TypeConflict

data TypeVariant = TypeVariant {
  signature :: String, 
  locs:: [Int]
  } deriving (Show, Generic)

instance ToJSON TypeVariant

data Mismatch = Mismatch
  { allConstraints :: [BCons],
    typeVariants :: [TypeVariant]
  }
  deriving (Show, Generic)

instance ToJSON Mismatch

data ChameleonError = ChameleonError
  { declInfo :: DeclInfo,
    typeConflict :: Maybe TypeConflict,
    mismatch :: Maybe Mismatch
  }
  deriving (Show, Generic)

instance ToJSON ChameleonError

data ChameleonResult = ChameleonResult
  { filepath :: String,
    placetable :: [LocDocument],
    decls :: [ChameleonError]
  }
  deriving (Show, Generic)

instance ToJSON ChameleonResult
