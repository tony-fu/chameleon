--------------------------------------------------------------------------------
--
-- Copyright (C) 2004 The Chameleon Team
--
-- This program is free software; you can redistribute it and/or modify it
-- under the terms of the GNU General Public License as published by the Free
-- Software Foundation; either version 2 of the License, or (at your option)
-- any later version. This program is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU General Public License for more details.
-- You should have received a copy of the GNU General Public License along
-- with this program; if not, write to the Free Software Foundation, Inc.,
-- 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
--
--------------------------------------------------------------------------------
-- Implementor: J.Wazny
--  Maintainer: J.Wazny
--      Status: - always defaulting to an ANSI target for printing source code
--                - otherwise okay
--------------------------------------------------------------------------------
--
-- Contains routines for generating error messages for reporting type errors.
--
--------------------------------------------------------------------------------

module TypeError.ErrorMsg
  ( reportAllTypeErrors,
    generateTypeErrorMsg,
    generateKindErrorMsg,
    generateSelectiveHL,
  )
where



import AST.SrcInfo
import Core.ConsOps
import Core.Constraint
import Core.InfResult
import Core.Justify
import Core.Name
import Core.Types
import Data.List
import Misc
import Misc.Error
import Misc.ErrorMsg
import qualified Misc.Output.ANSI as ANSI
import qualified Misc.Output.Plain as Plain
import Misc.Print
import Misc.Result
import Data.Aeson.Text
import Data.Aeson
import Data.Text.Lazy (unpack)
--------------------------------------------------------------------------------

reportAllTypeErrors :: Bool -> SrcTextData -> [InfResult] -> IO ()
reportAllTypeErrors json sd rs = do
  let rs' = filter isInfFailure rs
      reps = intersperse (putStrLn "") (map (reportTypeError json sd) rs')
  sequence_ reps

----------------------------------------

-- Reports the type error as a text message.
reportTypeError :: Bool -> SrcTextData -> InfResult -> IO ()
reportTypeError _ _ (InfSuccess _ _ _) = bug "reportTypeError: not a type error"
reportTypeError json sd (InfFailure id vs c) = do
  let hdr = errorMsg id ["Type error"]
      bcs = cBCons c
  min <- minUnsatSubset bcs
  let ls =
        let j = concatJusts min
            js = map getJust min
         in nub (justLocs j)

      s = printHLLocs sd ls
      src = ANSI.printSpec s
  if json
    then do
      putStrLn $ unpack $ encodeToLazyText s
    else do
      putStrLn (hdr ++ "\n" ++ src)


--------------------------------------------------------------------------------

-- Generates a standard error message from the source text and an inference
-- result.
generateTypeErrorMsg :: Bool -> SrcTextData -> InfResult -> IO Error
generateTypeErrorMsg _ _ (InfSuccess {}) = return notAnError
-- type conflict
generateTypeErrorMsg json sd (InfFailure id vs (C {cBCons = bcs})) = do
  --  putStrLn ("unsat. bcs: " ++ pretty bcs)
  min <- minUnsatSubset bcs
  let hdr = errorMsg id ["Type error"]
      ls =
        let j = concatJusts min
            js = map getJust min
         in nub (justLocs j)
      s = printHLLocs sd ls
      src = ANSI.printSpec s
      -- Plain.printSpec s
      -- msg = hdr ++ "\n" ++ src
      msg =
        errorMsg
          id
          ("Type error; conflicting sites:" : lines src)
      err = mkError id msg
  if json
    then do
      putStrLn $ unpack $ encodeToLazyText s
      return err
    else do 
      putStrLn "Min Uns Set: " 
      mapM_ (putStrLn . pretty) min 
      putStrLn ("Error Locations: " ++ show ls)
      return err

-- universal variable instantiated
generateTypeErrorMsg json sd (InfFailureUniv id v vs (C {cBCons = bcs})) = do
  -- putStrLn ("\nv  : " ++ pretty v)
  -- putStrLn ("\nvs : " ++ pretty vs)
  -- putStrLn ("\nbcs: " ++ pretty bcs)
  min <- minInstantiatingSubset v vs bcs
  -- putStrLn ("\nmin: " ++ pretty min)

  let nm = varName v
      ls0 =
        let j = concatJusts min
            js = map getJust min
         in nub (justLocs j)
  let ls = filter (> 0) ls0

  -- putStrLn ("ls: " ++ show (sort ls))

  if nameHasRef nm
    then
      let ref = nameRef nm
          info = refInfo ref
          v_str = refStr ref

          rc = (srcRow info, srcCol info)
          s = printHLLocsPos sd ls [rc]
          src = ANSI.printSpec s
          -- Plain.printSpec s
          -- Put in spaces, since errorMsg will remove empty strings.
          src' = map (\s -> if null s then " " else s) (lines src)
          hdr =
            split80
              ( "Polymorphic type variable `" ++ v_str
                  ++ "' (from line "
                  ++ show (srcRow info)
                  ++ ", col. "
                  ++ show (srcCol info)
                  ++ ") "
                  ++ "instantiated by"
              )
          msg = errorMsg id (hdr ++ src')
       in if json 
          then do
            putStrLn $ unpack $ encodeToLazyText s
            return (mkError id msg)
          else return (mkError id msg)

    else
      let s = printHLLocsPos sd ls []
          hdr = ["Polymorphic type variable instantiated by"]
          src = ANSI.printSpec s
          src' = map (\s -> if null s then " " else s) (lines src)
          msg = errorMsg id (hdr ++ src')
       in if json 
          then do
            putStrLn $ unpack $ encodeToLazyText s
            return (mkError id msg)
          else return (mkError id msg)

-- universal variable escaped from its scope
generateTypeErrorMsg json sd (InfFailureUnivEsc id (tt : _) t vs (C {cBCons = bcs})) = do
  {-
      putStrLn ("t  : " ++ pretty t)
      putStrLn ("vs : " ++ pretty vs)
      putStrLn ("bcs: " ++ pretty bcs)
  -}
  let bcs' = (t =+= tt) skolConsJust : bcs
  v <- findInstantiated bcs' vs
  --  putStrLn ("v: " ++ show v)
  min0 <- minInstantiatingSubset v [] bcs'
  let min = dropSkol min0
  --  putStrLn ("min: " ++ pretty min)

  --  bug "generateTypeErrorMsg InfFailureUnivEsc"

  let nm = varName v
      ls0 =
        let j = concatJusts min
            js = map getJust min
         in nub (justLocs j)
  let ls = filter (> 0) ls0

  if nameHasRef nm
    then
      let ref = nameRef nm
          info = refInfo ref
          v_str = refStr ref
          rc = (srcRow info, srcCol info)
          s = printHLLocsPos sd ls [rc]
          src = ANSI.printSpec s
          -- Plain.printSpec s
          -- Put in spaces, since errorMsg will remove empty strings.
          src' = map (\s -> if null s then " " else s) (lines src)
          hdr =
            split80
              ( "Polymorphic type variable `" ++ v_str
                  ++ "' (from line "
                  ++ show (srcRow info)
                  ++ ", col. "
                  ++ show (srcCol info)
                  ++ ") "
                  ++ "escapes through locations:"
              )
          msg = errorMsg id (hdr ++ src')
       in if json 
          then do
            putStrLn $ unpack $ encodeToLazyText s
            return (mkError id msg)
          else return (mkError id msg)
    else
      let s = printHLLocsPos sd ls []
          hdr = ["Polymorphic type variable escapes through locations:"]
          src = ANSI.printSpec s
          src' = map (\s -> if null s then " " else s) (lines src)
          msg = errorMsg id (hdr ++ src')
       in if json 
          then do
            putStrLn $ unpack $ encodeToLazyText s
            return (mkError id msg)
          else return (mkError id msg)
generateTypeErrorMsg json sd (InfFailureUConsUnmatched id uc) = do
  let hdr =
        [ "Unmatched user constraint " ++ pretty (prettyRename uc)
            ++ " from:"
        ]
      ls = filter (> 0) (justLocs (getJust uc))
      s = printHLLocs sd ls
      src = ANSI.printSpec s
      src' = map (\s -> if null s then " " else s) (lines src)
      msg = errorMsg id (hdr ++ src')
  if json 
          then do
            putStrLn $ unpack $ encodeToLazyText s
            return (mkError id msg)
          else do
            putStrLn ("ls: " ++ show ls)
            return (mkError id msg)

-- Kind Error Reporting
generateKindErrorMsg :: String -> SrcTextData -> InfResult -> IO Error
generateKindErrorMsg _ _ (InfSuccess _ _ _) = return notAnError
generateKindErrorMsg isDefault sd (InfFailure id vs (C {cBCons = bcs})) = do
  min <- minUnsatSubset bcs
  let hdr = errorMsg id ["Kind error"]
      ls =
        let j = concatJusts min
            js = map getJust min
         in nub (justLocs j)
      s = printHLLocs sd ls
      src = ANSI.printSpec s
      -- Plain.printSpec s
      msg =
        let msgHead = case isDefault of
              (s : str) -> "Kind Error Due to Defaulting of Polymorphic Kind\n" ++ isDefault
              [] -> "Kind Error"
         in errorMsg id ([msgHead ++ "Conflicting sites:"] ++ lines src)
      err = mkError id msg
  return err

-- Interface for selective location highlight printing of a program
-- srcT, selected by input list of program locations ls.
generateSelectiveHL :: SrcTextData -> [Loc] -> String
generateSelectiveHL srcT ls =
  let pSrc = printHLLocs srcT ls
   in ANSI.printSpec pSrc
