module TypeError.Format where

import Misc.Print
import Misc
import Core.InfResult
import Core.Constraint
import Misc.Document
import Core.ConsOps

formatError :: SrcTextData -> Filename -> InfResult -> IO ()
formatError sd fn (InfFailure id vs c) = do
  let locDocument = fromSrcTextData sd
  let bcons = cBCons c
  min <- minUnsatSubset bcons
  return ()
formatError sd fn (InfSuccess id tlv c) = undefined
formatError sd fn (SubSuccess id) = undefined 
formatError sd fn (InfFailureUniv id v vs C {cBCons = bcs}) = undefined 
formatError sd fn InfFailureUnivEsc {} = undefined 
formatError sd fn (InfFailureUConsUnmatched rId rUcons) = undefined
