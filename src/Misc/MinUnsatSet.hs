module Misc.MinUnsatSet where

import Core.BCons
import Core.Types
import Data.List


freeVarInCons :: BCons -> [Var]
freeVarInCons (Eq left right _) = fv left ++ fv right

varIsInCons :: Var -> BCons -> Bool
varIsInCons v bcon = v `elem` freeVarInCons bcon

varIsInAnyConstrains :: Var -> [BCons] -> Bool
varIsInAnyConstrains v bcons = any (\c -> varIsInCons v c) bcons

varList :: [BCons] -> [Var]
varList bcons = nub $ go bcons
    where
        go :: [BCons] -> [Var]
        go [] = []
        go (bc:bcs) = freeVarInCons bc ++ go bcs


-- Get two edges from a node
connectors :: [BCons] -> Var -> [BCons]
connectors [] _ = []
connectors (Eq left right j : bcs) var =
    if var `elem` fv left  || var `elem` fv right
        then Eq left right j:connectors bcs var
        else connectors bcs var

-- -o-
consToWedges :: [BCons] -> [(Var, [BCons])]
consToWedges bcons =
   filter ((>=2) . length . snd) $
    map (\v -> (v, connectors bcons v)) (varList bcons)

commonVars :: BCons -> BCons -> [Var]
commonVars (Eq left right _) (Eq left' right' _)  =
    filter (`elem` (fv left' ++ fv right')) (fv left ++ fv right)

conInWedge :: BCons -> (Var, [BCons]) -> Bool
conInWedge bcon (_, bcons) = bcon `elem` bcons

reorderWedges :: [(Var, [BCons])] -> [BCons]
reorderWedges ((var, bcon1:bcon2:_): cs) =
    reorderLeft var bcon1 cs ++ reorderRight var bcon2 cs

reorderLeft :: Var -> BCons -> [(Var, [BCons])] -> [BCons]
reorderLeft v bcon wedges =
    let connectedWedge = filter (conInWedge bcon) wedges
        possibleNextVar =
            let (Eq left right _) = bcon
            in if v `elem` fv left
                then fv right
                else fv left
        connectedWedge' = filter (\w -> fst w `elem` possibleNextVar) connectedWedge
        restWedges = filter (`notElem` connectedWedge) wedges
    in case connectedWedge' of
        [] -> [bcon]
        (w:_) ->
            let wedgeCons = snd w
                nextMatchCon =
                    if head wedgeCons == bcon
                        then head $ tail wedgeCons
                        else head wedgeCons
            in reorderLeft (fst w) nextMatchCon restWedges  ++  [bcon]

reorderRight :: Var -> BCons -> [(Var, [BCons])] -> [BCons]
reorderRight v bcon wedges =
    let possibleNextVar =
            let (Eq left right _) = bcon
            in if v `elem` fv left
                then fv right
                else fv left
        connectedWedge = filter (conInWedge bcon) wedges
        connectedWedge' = filter (\w -> fst w `elem` possibleNextVar) connectedWedge
        restWedges = filter (`notElem` connectedWedge) wedges
    in case connectedWedge' of
        [] -> [bcon]
        (w:_) ->
            let wedgeCons = snd w
                nextMatchCon =
                    if head wedgeCons == bcon
                        then head $ tail wedgeCons
                        else head wedgeCons
            in bcon: reorderRight (fst w) nextMatchCon restWedges

reorder :: [BCons] -> [BCons]
reorder = reorderWedges . consToWedges

isVarEqVar :: BCons -> Bool
isVarEqVar (Eq left right _) = isTVar left && isTVar right

varInApp :: Var -> BCons -> Bool
-- varInApp v (Eq app1@(TApp _ _) app2@(TApp _ _) _) = (v `elem` fv app1) || (v `elem` fv app2)
varInApp v (Eq app1@(TApp _ _) _ _) = v `elem` fv app1
varInApp v (Eq _ app2@(TApp _ _) _) = v `elem` fv app2
varInApp _ _ = False

varInAnyApp :: Var -> [BCons] -> Bool
varInAnyApp v bcons = any (varInApp v) bcons

highestRank :: [Var] -> [BCons] -> [Var]
highestRank vars bcons = filter onlyInVEVs vars
    where
        vevs = filter isVarEqVar bcons
        onlyInVEVs :: Var -> Bool
        onlyInVEVs v =
            let peers = nub $ findPeers vevs [v]
            in all (\p -> not (varInAnyApp p bcons)) peers


findPeers :: [BCons] -> [Var] -> [Var]
findPeers [] vs = vs
findPeers (Eq (TVar left) (TVar right) _:cs) vs
  | left `elem` vs = findPeers cs (right:vs)
  | right `elem` vs = findPeers cs (left:vs)
  | otherwise = findPeers cs vs
findPeers (Eq {} : cs) vs = findPeers cs vs


isFlank :: BCons -> [BCons] -> Bool
isFlank (Eq tl tr _) cs
    | isConstant tl && not (isConstant tr) = True
    | not (isConstant tl) && isConstant tr = True
    | all (\v -> not (varIsInAnyConstrains v cs)) (allVars tl) = True
    | all (\v -> not (varIsInAnyConstrains v cs)) (allVars tr) = True
    | otherwise = False 

isConstant :: Type -> Bool 
isConstant (TCon _) = True
isConstant (TVar _) = False
isConstant (TApp a b) = isConstant a && isConstant b

allVars :: Type -> [Var]
allVars (TCon _) = []
allVars (TVar v) = [v]
allVars (TApp a b) = allVars a ++ allVars b
    -- let vEqV = filter isVarEqVar bcons

-- v671=Bool->Bool_[12,9,4] x
-- v671=v654_[12,9,4,-5]    = 
-- v654=v650->v653_[12,9,6] -1
-- v650=v651_[12,9,5]       =
-- v655=v651_[12,9,3]       =
-- v656=v655->v652_[12,9]   +1
-- v657->v658=v656_[12,9,-1]-1
-- v657=v659_[12,9,-1]       =
-- v660=v659_[12,9,-1]       =
-- v661=v660->v658_[12,9,7]  +1
-- v640=v661_[12,9,7]        =
-- v640=v635_[12,9,-5]       =
-- v635=v632->v634_[12,11]   -1
-- v632=[Char]_[12,10]
