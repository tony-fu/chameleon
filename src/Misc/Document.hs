{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
module Misc.Document where
import Misc.Print
import AST.SrcInfo
import AST.Token
import Data.Aeson
import GHC.Generics

import qualified Data.Map.Strict as Map
data Range = 
    Range { fromRow :: Int, toRow::Int, fromCol::Int, toCol::Int} 
    | EmptyRange
    deriving (Eq, Show, Generic)

instance ToJSON Range

data LocDocument = 
    LocDocument { getLoc:: Int, getRange:: Range} 
    deriving (Eq, Show, Generic)

instance ToJSON LocDocument

fromSrcInfo :: [Token] -> SrcInfo -> Range
fromSrcInfo (t:ts)  (SrcInfo loc srcfile row col offset) = 
    if tokenSrcPos t == noSrcPos
        then fromSrcInfo ts (SrcInfo loc srcfile row col offset)
        else let (a, r', c') = tokenSrcPos t
                in if r' == row && c' == col 
                then Range (row - 1) (row - 1) (col - 1) (col + tokenLength t - 1)
                else fromSrcInfo ts (SrcInfo loc srcfile row col offset)
fromSrcInfo [] _ = EmptyRange

fromSrcTextData :: SrcTextData -> [LocDocument]
fromSrcTextData (SrcTextData text (SrcInfoTable srcInfos locToSrcInfo) tokens) = 
    filter ((/= EmptyRange) . getRange) (map go (Map.assocs locToSrcInfo))
    where 
        go ::  (Int, SrcInfo) -> LocDocument
        go (loc, srcInfo) = LocDocument loc (fromSrcInfo tokens srcInfo) 