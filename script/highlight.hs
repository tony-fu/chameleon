#!/usr/bin/env stack
{- stack
  script
  --resolver lts-16.19
  --package aeson
  --package bytestring
  --package ansi-terminal
-}

{-# LANGUAGE DeriveGeneric #-}

import Data.Aeson ( FromJSON, eitherDecode )
import GHC.Generics ( Generic )
import qualified Data.ByteString.Lazy as BL
import System.Console.ANSI
    ( setSGR,
      Color(Red, Cyan, Magenta, Green, Yellow, Blue),
      ColorIntensity(Dull),
      ConsoleLayer(Background),
      SGR(Reset, SetColor) )
import Data.Char ( isSpace )
import Data.Bifunctor ( Bifunctor(first) )

data ErrorDetail = ErrorDetail {
    fileName :: String,
    description :: String,
    lineNumber :: Int,
    columnNumber  :: Int,
    conflicts :: [Conflict]
    } deriving (Show, Generic)

instance FromJSON ErrorDetail

data Conflict = Conflict {
    colour :: String, 
    label :: String,
    instanciatedType :: String,
    expectedType :: String,
    locations :: [Location] 
    }deriving (Show, Generic)

instance FromJSON Conflict


data Location = Location {
    lineFrom :: Int,
    lineTo :: Int,
    columnFrom :: Int,
    columnTo :: Int
    } deriving(Show, Generic)

instance FromJSON Location


colors = [
     Red
    ,Blue
    ,Cyan
    ,Magenta
    ,Yellow
    ,Green
    ]


outputChar :: [(Conflict, Int)] -> (Char, Int, Int) -> IO ()
outputChar [] (ch, _, _)  = putStr [ch]
outputChar ((conf, index):cs) (ch,li,co) = do
    -- putStrLn $ show li ++ ", " ++ show co
    let locs = locations conf
    -- print locs
    let charIsSpace = isSpace ch
    let isPositionInLoc = (\li co Location {lineFrom=lf, lineTo=lt, columnFrom=cf, columnTo=ct} -> 
                                if lf /= lt
                                    then li == lf && co >= cf
                                    else li >= lf && li <= lt && co >= cf && co <= ct)

    let shoudSetStyle = not charIsSpace &&  any (isPositionInLoc li co) locs
    if shoudSetStyle
        then do
            setSGR [SetColor Background  Dull  (colors !! index)]
            putStr [ch]
            setSGR [Reset]
        else outputChar cs (ch, li, co)
                

main = do
    content <- BL.getContents
    -- BL.putStrLn content
    let maybeErrors = eitherDecode content :: Either String [ErrorDetail]
    case maybeErrors of
        Left decodeError -> do
            putStrLn decodeError
        Right [] -> putStrLn "No type error"
        Right typeErrors -> do
            let fn = fileName (head typeErrors)
                errorName = description (head typeErrors)
                confs = conflicts (head typeErrors)
            -- print confs
            fileContent <- readFile fn
            putStrLn errorName
            mapM_ (\(conf, index) -> do
                    let text = instanciatedType conf
                    putStr $ (label conf) ++ " :: "
                    setSGR [SetColor Background  Dull  (colors !! index)]
                    putStr text
                    setSGR [Reset]
                    putStrLn ""
                ) (confs `zip` [0..])
            let fileLines = lines fileContent `zip` [0..]
                reinsertEol = map (first ('\n' :)) fileLines
                fileChars = concatMap (\(line, lineNum) -> zip3 line (repeat lineNum) [0..]) reinsertEol
            putStrLn $  "-----------------------------" ++ fn ++ "-----------------------------"
            mapM_ (outputChar (confs `zip` [0..])) fileChars

            putStrLn $  "\n\n-----------------------------" ++ replicate (length fn) '-' ++ "-----------------------------"
-- Sometimes the chameleon say line 4 - 5 column 6 - 0. Which is bad.