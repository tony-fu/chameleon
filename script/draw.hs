#!/usr/bin/env stack
{- stack
  script
  --resolver lts-16.19
  --package aeson
  --package bytestring
  --package diagrams
  --package diagrams-lib
  --package diagrams-svg
-}

{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE OverloadedStrings         #-}

import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine
import Data.Aeson ( (.:), FromJSON(parseJSON), Value(Object), eitherDecode )
import qualified Data.ByteString.Lazy as BL

data BCons = Eq {
    left :: Type,
    right :: Type,
    just :: [Int]
  }
  deriving (Eq, Show)

instance FromJSON BCons where
  parseJSON (Object v) = do
    left <- v .: "left"
    right <- v .: "right"
    just <- v .: "just"
    return $ Eq left right just


data Type = Var String | Con String | App Type Type deriving (Eq, Show)
instance FromJSON Type where
  parseJSON (Object v) = do
    cla <- v .: "class"
    case cla of
      ['c', 'o','n'] -> do
        name <- v .: "name"
        return $ Con name
      ['v', 'a','r'] -> do
        name <- v .: "name"
        return $ Var name
      ['a', 'p','p'] -> do
        type1 <- v .: "type1"
        type2 <- v .: "type2"
        return $ App type1 type2


myCircle :: [BCons] -> Diagram B
myCircle [] = mempty
myCircle (Eq left right just : bcons) = 
  vsep 0.4
    [hsep 0.2 
      [ circle 1, circle 1],  myCircle bcons]

-- drawBcons :: BCons -> Diagram B
-- drawBcons (Eq left right just) = 

drawType :: Type -> Diagram B
drawType (Var name) = 
  text name # fontSizeL 0.2 # fc black <> circle 0.2 # named name
drawType (Con name) = 
  text name # fontSizeL 0.2 # fc black <> circle 0.2 # named name
drawType (App t1 t2) = 
  hsep 0.2 
    [ drawType t1
    , drawType t2
    ]
-- main :: IO ()
-- main = do
--   txt <- BL.readFile  "./intermediate/mus.Main.app.txt" 
--   let ebcons = eitherDecode txt:: Either String [BCons]
--   case ebcons of
--     Left err -> putStrLn err
--     Right bcons -> mainWith $ myCircle bcons
main = mainWith $ 
  drawType $ App (Var "a") (Var "b")