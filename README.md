Chameleon Type Debugging System
---------------------

## Status
Chameleon is currently under a major overhaul. 
This is to let chameleon work with recent haskell releases (8.0.0+), modern computers and OSs.

I will update this section, with updates when chameleon passes all tests.

## Requirements:
- Stack 
https://docs.haskellstack.org/en/stable/README/

## Compile project:
```
stack build
```

## Run this project:
```
stack run -- --help
stack run -- --lib="." Example.hs
```

## Old chameleon repository
https://github.com/sulzmann/Chameleon

## New repository 
https://gitlab.com/tony-fu/chameleon

## Folder structure of this project
```bash
Chemeleon
├── lib # Contains Libraries (e.g. Prelude) for ".ch" files
├── intermediate # Auto-generated intermediate file formats
├── src # Source code for chameleon
│   ├── AST # ".ch" file syntax parser
│   │   └── ChParser
│   ├── Config # Parsing command line arguments
│   ├── Core # Transform everything from "./AST" into constraints, and deliver to "./Solvers"
│   │   └── Kinds
│   ├── Misc # Utility functions. (Output formating, Data structures, Error handling, etc.)
│   │   └── Output
│   ├── Solvers # CHR implementation
│   ├── System # Orchastrate execution, handle file IO, etc.
│   └── TypeError # Output formatting of type error
└── example # Examples files that showcase chameleon type checking
```

## Haskell Modules
We currently use a few modules. The purposes are listed below
- [base](https://hackage.haskell.org/package/base): No comment necessary
- [containers](https://hackage.haskell.org/package/containers):  For data structures like Set, Map and IntMap
- [unordered-containers](https://hackage.haskell.org/package/unordered-containers):  For data structures
- [array](https://hackage.haskell.org/package/array):  For data structures
- [pretty](https://hackage.haskell.org/package/pretty): For pretty print in terminal
- [directory](https://hackage.haskell.org/package/directory): Handle directory lookup, listing, creation and removal
- [filepath](https://hackage.haskell.org/package/filepath): Handle filepath in unix-based system ("/a/b/c/d") and windows system (c:\\a\\b\\c\\d)
- [mtl](https://hackage.haskell.org/package/mtl): For monad trasformation
- [ghc](https://hackage.haskell.org/package/ghc):  For parsing Haskell source file
- [ghc-paths](https://hackage.haskell.org/package/ghc-paths):  Locate the ghc path in the system in portable way
- [raw-strings-qq](https://hackage.haskell.org/package/raw-strings-qq): Enable writing test cases for parsing with quasi quotes.
- [aeson](https://hackage.haskell.org/package/aeson): For json serializing/deserializing
- [text](https://hackage.haskell.org/package/text): For handling lazy text data structure
## TODOS
```
[x] If branch variable should be y instead of x -> y
[x] Highlighted areas should include expressions and patterns?
[x] Deal with unknown case
[ ] Use base case for error reporting
[ ] Compile image for mus graph
[ ] SrcPos need to include an length (or offset) option so we know where the token end 
```

## Terminology
### Acronym
These are not quite terminology but just acronyms that being used consistently in this project.
- tlv : top level variable
- trans: Some place use trans to mean translate e.g. transUnknownTypeCons
- Inf: Mostly short hand for inference
- Cons: More likely it is short for constraints, instead of cons operator
- Prim: Short for primitive, a chameleon special syntax. Like haskell prim module.
- Just: More likely it means justification, not the maybe constructor