-- https://stackoverflow.com/questions/65953613/move-one-element-from-one-list-to-another-in-haskell

test ::[a] -> [b] -> ([a],[b])
test [] [] = ([], [])
test [] [x] = ([x], [])
test [x] [] = ([x], [])
test [y] [x] = ([x:y], [])

-- TODO: why it will be considered correct if we swap the line 5 and 6