-- https://stackoverflow.com/questions/19217646/haskell-type-error-with-where
type NI = Integer

type Age = Integer

type Balance = Integer

type Person = (NI, Age, Balance)

type Bank = [Person]

sumAllAccounts :: NI -> Bank -> Integer
sumAllAccounts n l = filter niMatch l
  where
    niMatch n (a, _, _)
      | n == a = True
      | otherwise = False
