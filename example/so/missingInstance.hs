-- https://stackoverflow.com/questions/65953742/haskell-cant-understand-error-no-instance-for-ord-a-arising-from-a-use-of
testReturn :: [[Char]] -> ([a], [b]) -> Bool
testReturn [] b = orderedCheck (fst b)
testReturn (_:rest) b = testReturn rest (saFunc (fst b), snd b)

orderedCheck :: (Ord a) => [a] -> Bool
orderedCheck []  = True
orderedCheck [x] = True
orderedCheck (x:y:xs) = x <= y && orderedCheck (y:xs)

saFunc :: [a] -> [a]
saFunc [] = []
saFunc [x] = [x]
saFunc (x:y:xs) = y:x:xs