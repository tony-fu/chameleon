-- https://stackoverflow.com/questions/43926822/maybe-int-type-error

type Prog = [Cmd]

data Cmd = LD Int
    | ADD
    | MULT
    | DUP
    | INC
    | SWAP
    | POP Int

type Stack = [Int]
-- type D = Stack -> Maybe Stack
type Rank = Int
type CmdRank = (Int,Int)


rankC :: Cmd -> CmdRank
rankC (LD _) = (0,1)
rankC ADD = (2,1)
rankC MULT = (2,1)
rankC DUP = (1,2)
rankC INC = (1,1)
rankC SWAP = (2,2)
rankC (POP x) = (x,0)


rankP :: Prog -> Maybe Rank
rankP [] = Nothing
rankP [x] = Just (snd (rankC x) - fst (rankC x))
rankP (x:xs) = if (Just (snd (rankC x) - fst (rankC x)) + rankP xs) < 0
                    then Nothing
                    else Just (snd (rankC x) - fst(rankC x)) + rankP xs

-- The problem is the Just constructor appears first in constraints. But the real relevent one is (+) application
-- The order of expressions must matter