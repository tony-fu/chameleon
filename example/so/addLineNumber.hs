main :: IO ()
main = do
    fileContent <- readFile "./users.txt"
    let fileLines = lines fileContent `zip` [0..]
        withNumTuple = fileLines 
        withNumStr = map (\(l, num) -> (num : l)) withNumTuple
        newContent = unlines withNumStr
    putStrLn newContent
