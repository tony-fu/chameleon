withNumTuple = lines "hello" `zip` [0..]
withNumStr = map (\(line, num) -> num : line) withNumTuple