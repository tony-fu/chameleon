
-- class Num a where
--     (+) :: Num a => a -> a -> a
--     fromInteger :: Num a => Integer -> a  


-- instance Num Integer where
--     (+) = bot
--     fromInteger = bot 

-- instance Num Int where
--     (+) = bot
--     fromInteger = bot 

class Bird a where
    fly :: Bird b => b -> Bool 

primitive bot :: a

data Pigeon = Pigeon

instance Bird Pigeon where
    fly = bot

b = fly "foo"