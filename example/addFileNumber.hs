main :: IO ()
main = do
    fileContent <- readFile "./users.txt"
    let fileLines = lines fileContent
    let withNumTuple = fileLines `zip` [0..]
    let withNumStr = map (\(line, num) -> num : line) withNumTuple
    mapM_ putStrLn withNumStr
    return () 



-- ./example/addFileNumber.hs:(3,17):
--     ERROR: Undefined variable `>>=`

-- ./example/addFileNumber.hs:(3,20):
--     ERROR: Undefined variable `readFile`

-- ./example/addFileNumber.hs:(7,5):
--     ERROR: Undefined variable `mapM_`

-- ./example/addFileNumber.hs:(7,11):
--     ERROR: Undefined variable `putStrLn`

-- ./example/addFileNumber.hs:(4,21):
--     ERROR: Undefined variable `lines`

-- ./example/addFileNumber.hs:(5,34):
--     ERROR: Undefined variable `zip`

-- ./example/addFileNumber.hs:(5,40):
--     ERROR: Undefined variable `enumFrom`
