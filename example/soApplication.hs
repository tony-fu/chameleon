-- https://stackoverflow.com/questions/21140334/haskell-type-error-in-application

test :: Integer -> [Integer] -> [Integer]
test _ [] = []
test offset xs = move offset [] xs
  where
    move :: Integer -> [Integer] -> [Integer] -> [Integer]
    move offset subList (x : xs)
      | offset == 0 = subList ++ rotate (last (x : xs)) (x : xs)
      | otherwise = move (offset -1) (saveList subList (x : xs)) xs
    saveList :: [Integer] -> [Integer] -> [Integer]
    saveList save (x : xs) = save ++ [x]
    rotate :: Integer -> [Integer] -> [Integer]
    rotate _ [] = []
    rotate item (x : xs) = [item] ++ move x xs
    last :: [Integer] -> Integer
    last [x] = x
    last (x : xs) = last xs

-- TODO: Why is this passing?