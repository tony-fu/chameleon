isPrime :: Integer -> Bool
isPrime x 
  | x <= 1 = False
  | otherwise = ld x == x

ld :: Integer -> Integer
ld = ldf 2

ldf :: Integer -> Integer -> Integer
ldf a b 
  | divides a b = a
  | (a * a) > b = b
  | otherwise = ldf (a + 1) b

remainder :: Integer -> Integer -> Integer
remainder a b 
  | a < b = a
  | otherwise = remainder (a - b) b


divides :: Integer -> Integer -> Bool
divides a b = remainder b a
