data Val c i = C c | I i

divCon :: [Val Char Integer] -> Integer
divCon [] = 0
divCon ((C c):vs) = (divCon vs) - c
divCon ((I i):vs) = (divCon vs) + i
