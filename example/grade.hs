getGrade :: Double -> Double -> Double -> Char
getGrade x y z = grade (average x y z) 

average :: Fractional a => a -> a -> a -> a
average a b c = (a + b + c) / 3

grade :: a -> Char
grade x
  | x >= 90 && x <= 100 = 'A'
  | x >= 80 && x <= 90 = 'B'
  | x >= 70 && x <= 80 = "C"
  | x >= 60 && x <= 70 = 'D'
  | otherwise = 'F'