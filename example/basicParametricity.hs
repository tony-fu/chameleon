swap :: a -> b -> (a, b)
swap a b = (b, a)