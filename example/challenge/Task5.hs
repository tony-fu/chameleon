module Task5 where

-- Takes the last element of a list
last' :: [a] -> a
last' [x] = x
last' (x:xs) = xs

-- Takes the first n elements from a list
take' :: Integer -> [a] -> [a] 
take' n [] = []
take' n (x:xs) = x ++ (take' (n - 1) x)