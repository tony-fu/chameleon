module Task8 where

x :: Maybe Integer -> Integer
x n = let fromMaybe ma a = case ma of
                            (Nothing) -> a
                            (Just b) -> b
      in fromMaybe n Nothing