module Task where


data Parser x = PD x

fmapParser :: (a -> b) -> Parser a -> Parser b 
fmapParser f (PD a) = PD (f a)

instance Functor Parser where
  fmap = fmapParser

