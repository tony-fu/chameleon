module Task6 where

-- Compute the size of a list
length' :: [a] -> Integer
length' [] = 0
length' (x:xs) = (length' xs) + 1

-- Check if an item appears in a list. 
-- This function should be identical to 
-- the standard elem function in prelude.
elem' :: (Eq a) => a -> [a] -> Bool  
elem' a = False  
elem' a (x:xs)  
    | a == x    = True  
    | otherwise = a `elem'` xs


-- Check if an item doesn't appear in a list. 
-- This function should be identical to 
-- the standard elem function in prelude.
notElem' :: (Eq a) => a -> [a] -> Bool  
notElem' a xs = not (elem' a) 