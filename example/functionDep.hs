class C a b | a -> b where 
    c :: a -> b -> a

instance C a a

e = c (\x -> x) 'a'