module Example where

-- x = 1
-- le = \ x -> x + 1
-- ple = (\x -> x + 1)
-- ls = let x = 3 in x + 2
-- wc = x where x = 3

-- parenInfix = 5 + (3 + 2)
-- split xs = case xs of
--     [] -> ([],[])
--     [x] -> ([], x)
--     (x:y:zs) -> let (xs, ys) = split zs 
--                 in (x:xs, y:ys)
-- primitive length :: [a] -> Int
 
main = putStrLn "hello"

-- f :: a -> (a, b)
-- f x = (x, x)

-- f c = if c 
--     then \ g x -> g x
--     else \ e y -> y e

-- g a b = a == a b

-- x xs =  length xs
